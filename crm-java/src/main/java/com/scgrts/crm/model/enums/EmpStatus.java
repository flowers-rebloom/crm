package com.scgrts.crm.model.enums;

import java.util.HashMap;

public enum EmpStatus {
    INACTIVE(0, "未激活"), NORMAL(1, "正常"), DISABLED(2, "禁用");

    private final int value;
    private final String description;

    private EmpStatus(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    private static HashMap<Integer, EmpStatus> mapping = new HashMap<Integer, EmpStatus>();

    static {
        mapping.put(0, INACTIVE);
        mapping.put(1, NORMAL);
        mapping.put(2, DISABLED);
    }

    public static EmpStatus fromInt(int k) {
        return mapping.get(k);
    }

}

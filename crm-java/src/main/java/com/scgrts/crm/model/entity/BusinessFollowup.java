package com.scgrts.crm.model.entity;

import java.util.Date;

import com.scgrts.crm.model.enums.FollowupMethod;

public class BusinessFollowup {
    private Integer businessFollowupId;
    private Integer businessId;
    private Date followupTime;
    private String businessFollowupPresentation;
    private FollowupMethod followupMethod;
    private Date nextFollowupTime;
    private Date createTime;
    private Date updateTime;
    private Integer createEmpId;
    private Integer contactId;

    public Integer getBusinessFollowupId() {
        return businessFollowupId;
    }

    public void setBusinessFollowupId(Integer businessFollowupId) {
        this.businessFollowupId = businessFollowupId;
    }

    public Integer getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public Date getFollowupTime() {
        return followupTime;
    }

    public void setFollowupTime(Date followupTime) {
        this.followupTime = followupTime;
    }

    public String getBusinessFollowupPresentation() {
        return businessFollowupPresentation;
    }

    public void setBusinessFollowupPresentation(String businessFollowupPresentation) {
        this.businessFollowupPresentation = businessFollowupPresentation;
    }

    public FollowupMethod getFollowupMethod() {
        return followupMethod;
    }

    public void setFollowupMethod(FollowupMethod followupMethod) {
        this.followupMethod = followupMethod;
    }

    public Date getNextFollowupTime() {
        return nextFollowupTime;
    }

    public void setNextFollowupTime(Date nextFollowupTime) {
        this.nextFollowupTime = nextFollowupTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getCreateEmpId() {
        return createEmpId;
    }

    public void setCreateEmpId(Integer createEmpId) {
        this.createEmpId = createEmpId;
    }

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

}

package com.scgrts.crm.model.dto;

import com.scgrts.crm.model.entity.BusinessFollowup;

public class BusinessFollowupDTO extends BusinessFollowup {
    private String contactName;

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

}

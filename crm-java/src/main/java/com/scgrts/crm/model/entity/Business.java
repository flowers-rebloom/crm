package com.scgrts.crm.model.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.scgrts.crm.model.enums.BusinessStep;

public class Business {
    private Integer businessId;
    private Integer custId;
    private String businessName;
    private BigDecimal businessMoney;
    private String businessRemark;
    private BusinessStep currentStep;
    private Integer endStatus;
    private Integer createEmpId;
    private Date createTime;
    private Date updateTime;
    private Integer ownerEmpId;
    private Boolean followupStatus;
    private Date lastFollowupTime;
    private String lastFollowupPresentation;
    private Date nextFollowupTime;
    private Boolean deleteStatus;
    private String custName;

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public Boolean getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(Boolean deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    public Integer getBusinessId() {
        return businessId;
    }

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public Integer getCustId() {
        return custId;
    }

    public void setCustId(Integer custId) {
        this.custId = custId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public BigDecimal getBusinessMoney() {
        return businessMoney;
    }

    public void setBusinessMoney(BigDecimal businessMoney) {
        this.businessMoney = businessMoney;
    }

    public String getBusinessRemark() {
        return businessRemark;
    }

    public void setBusinessRemark(String businessRemark) {
        this.businessRemark = businessRemark;
    }

    public BusinessStep getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(BusinessStep currentStep) {
        this.currentStep = currentStep;
    }

    public Integer getEndStatus() {
        return endStatus;
    }

    public void setEndStatus(Integer endStatus) {
        this.endStatus = endStatus;
    }

    public Integer getCreateEmpId() {
        return createEmpId;
    }

    public void setCreateEmpId(Integer createEmpId) {
        this.createEmpId = createEmpId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getOwnerEmpId() {
        return ownerEmpId;
    }

    public void setOwnerEmpId(Integer ownerEmpId) {
        this.ownerEmpId = ownerEmpId;
    }

    public Boolean getFollowupStatus() {
        return followupStatus;
    }

    public void setFollowupStatus(Boolean followupStatus) {
        this.followupStatus = followupStatus;
    }

    public Date getLastFollowupTime() {
        return lastFollowupTime;
    }

    public void setLastFollowupTime(Date lastFollowupTime) {
        this.lastFollowupTime = lastFollowupTime;
    }

    public String getLastFollowupPresentation() {
        return lastFollowupPresentation;
    }

    public void setLastFollowupPresentation(String lastFollowupPresentation) {
        this.lastFollowupPresentation = lastFollowupPresentation;
    }

    public Date getNextFollowupTime() {
        return nextFollowupTime;
    }

    public void setNextFollowupTime(Date nextFollowupTime) {
        this.nextFollowupTime = nextFollowupTime;
    }

    @Override
    public String toString() {
        return "Business [businessId=" + businessId + ", custId=" + custId + ", businessName=" + businessName
                + ", businessMoney=" + businessMoney + ", businessRemark=" + businessRemark + ", currentStep="
                + currentStep + ", endStatus=" + endStatus + ", createEmpId=" + createEmpId + ", createTime="
                + createTime + ", updateTime=" + updateTime + ", ownerEmpId=" + ownerEmpId + ", followupStatus="
                + followupStatus + ", lastFollowupTime=" + lastFollowupTime + ", lastFollowupPresentation="
                + lastFollowupPresentation + ", nextFollowupTime=" + nextFollowupTime + ", deleteStatus=" + deleteStatus
                + "]";
    }

}

package com.scgrts.crm.model.dto;

import java.util.Date;

import com.scgrts.crm.model.enums.FollowupMethod;

/**
 * 本来联系人没有跟进记录，但是跟进一个客户或商机都需要记录联系人信息，所以这个DTO类用于封装与特定的联系人相关的客户跟进/商机跟进。
 *
 * @author root
 */
public class ContactFollowupDTO {

    private Date followupTime;
    private String FollowupPresentation;
    private FollowupMethod followupMethod;

    public Date getFollowupTime() {
        return followupTime;
    }

    public void setFollowupTime(Date followupTime) {
        this.followupTime = followupTime;
    }

    public String getFollowupPresentation() {
        return FollowupPresentation;
    }

    public void setFollowupPresentation(String followupPresentation) {
        FollowupPresentation = followupPresentation;
    }

    public FollowupMethod getFollowupMethod() {
        return followupMethod;
    }

    public void setFollowupMethod(FollowupMethod followupMethod) {
        this.followupMethod = followupMethod;
    }

}

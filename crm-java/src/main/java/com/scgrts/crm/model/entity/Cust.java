package com.scgrts.crm.model.entity;

import java.util.Date;

import com.scgrts.crm.model.enums.Source;

public class Cust {
    private Integer custId;
    private String custName;
    private Source custSource;
    private String custPhone;
    private String custWebsite;
    private Integer custProvince;
    private Integer custCity;
    private Integer custDistrict;
    private String custAddress;
    private String longitude;
    private String latitude;
    private String custRemark;
    private Integer createEmpId;
    private Date createTime;
    private Date updateTime;
    private Integer ownerEmpId;
    private Boolean followupStatus;
    private Date lastFollowupTime;
    private String lastFollowupPresentation;
    private Date nextFollowupTime;
    private Boolean lockStatus;
    private Boolean dealStatus;
    private Boolean deleteStatus;


    public Integer getCustId() {
        return custId;
    }

    public void setCustId(Integer custId) {
        this.custId = custId;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public Source getCustSource() {
        return custSource;
    }

    public void setCustSource(Source custSource) {
        this.custSource = custSource;
    }

    public String getCustPhone() {
        return custPhone;
    }

    public void setCustPhone(String custPhone) {
        this.custPhone = custPhone;
    }

    public String getCustWebsite() {
        return custWebsite;
    }

    public void setCustWebsite(String custWebsite) {
        this.custWebsite = custWebsite;
    }

    public Integer getCustProvince() {
        return custProvince;
    }

    public void setCustProvince(Integer custProvince) {
        this.custProvince = custProvince;
    }

    public Integer getCustCity() {
        return custCity;
    }

    public void setCustCity(Integer custCity) {
        this.custCity = custCity;
    }

    public Integer getCustDistrict() {
        return custDistrict;
    }

    public void setCustDistrict(Integer custDistrict) {
        this.custDistrict = custDistrict;
    }

    public String getCustAddress() {
        return custAddress;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getCustRemark() {
        return custRemark;
    }

    public void setCustRemark(String custRemark) {
        this.custRemark = custRemark;
    }

    public Integer getCreateEmpId() {
        return createEmpId;
    }

    public void setCreateEmpId(Integer createEmpId) {
        this.createEmpId = createEmpId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getOwnerEmpId() {
        return ownerEmpId;
    }

    public void setOwnerEmpId(Integer ownerEmpId) {
        this.ownerEmpId = ownerEmpId;
    }

    public Boolean getFollowupStatus() {
        return followupStatus;
    }

    public void setFollowupStatus(Boolean followupStatus) {
        this.followupStatus = followupStatus;
    }

    public Date getLastFollowupTime() {
        return lastFollowupTime;
    }

    public void setLastFollowupTime(Date lastFollowupTime) {
        this.lastFollowupTime = lastFollowupTime;
    }

    public String getLastFollowupPresentation() {
        return lastFollowupPresentation;
    }

    public void setLastFollowupPresentation(String lastFollowupPresentation) {
        this.lastFollowupPresentation = lastFollowupPresentation;
    }

    public Date getNextFollowupTime() {
        return nextFollowupTime;
    }

    public void setNextFollowupTime(Date nextFollowupTime) {
        this.nextFollowupTime = nextFollowupTime;
    }

    public Boolean getLockStatus() {
        return lockStatus;
    }

    public void setLockStatus(Boolean lockStatus) {
        this.lockStatus = lockStatus;
    }

    public Boolean getDealStatus() {
        return dealStatus;
    }

    public void setDealStatus(Boolean dealStatus) {
        this.dealStatus = dealStatus;
    }

    public Boolean getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(Boolean deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    @Override
    public String toString() {
        return "Cust{" +
                "custId=" + custId +
                ", custName='" + custName + '\'' +
                ", custSource=" + custSource +
                ", custPhone='" + custPhone + '\'' +
                ", custWebsite='" + custWebsite + '\'' +
                ", custProvince=" + custProvince +
                ", custCity=" + custCity +
                ", custDistrict=" + custDistrict +
                ", custAddress='" + custAddress + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", custRemark='" + custRemark + '\'' +
                ", createEmpId=" + createEmpId +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", ownerEmpId=" + ownerEmpId +
                ", followupStatus=" + followupStatus +
                ", lastFollowupTime=" + lastFollowupTime +
                ", lastFollowupPresentation='" + lastFollowupPresentation + '\'' +
                ", nextFollowupTime=" + nextFollowupTime +
                ", lockStatus=" + lockStatus +
                ", dealStatus=" + dealStatus +
                ", deleteStatus=" + deleteStatus +
                '}';
    }
}

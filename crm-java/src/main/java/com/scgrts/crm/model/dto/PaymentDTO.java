package com.scgrts.crm.model.dto;

import com.scgrts.crm.model.entity.Payment;

public class PaymentDTO extends Payment {

    private String custName;
    private String contractName;
    private String ownerEmpName;

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;

    }

    public String getOwnerEmpName() {
        return ownerEmpName;
    }

    public void setOwnerEmpName(String ownerEmpName) {
        this.ownerEmpName = ownerEmpName;
    }

}

package com.scgrts.crm.model.dto;

import java.math.BigDecimal;

public class AreaCustItem {
    private Integer areaId;
    private String areaName;
    private BigDecimal areaNums;

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public BigDecimal getAreaNums() {
        return areaNums;
    }

    public void setAreaNums(BigDecimal areaNums) {
        this.areaNums = areaNums;
    }

}

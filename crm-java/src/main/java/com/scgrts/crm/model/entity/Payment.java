package com.scgrts.crm.model.entity;

import java.util.Date;
/**
 * 和数据库的payment表对应的实体类
 * @author Administrator
 *
 */

import java.math.BigDecimal;

public class Payment {
    private Integer paymentId;
    private Integer custId;
    private Integer contractId;
    private Date paymentTime;
    private String paymentType;
    private BigDecimal paymentMoney;
    private Integer createEmpId;
    private Integer ownerEmpId;
    private Date createTime;
    private Date updateTime;
    private String paymentRemark;
    private boolean deleteStatus;

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public Integer getCustId() {
        return custId;
    }

    public void setCustId(Integer custId) {
        this.custId = custId;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public Date getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(Date paymentTime) {
        this.paymentTime = paymentTime;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public BigDecimal getPaymentMoney() {
        return paymentMoney;
    }

    public void setPaymentMoney(BigDecimal paymentMoney) {

        this.paymentMoney = paymentMoney;
    }

    public Integer getCreateEmpId() {
        return createEmpId;
    }

    public void setCreateEmpId(Integer createEmpId) {
        this.createEmpId = createEmpId;
    }

    public Integer getOwnerEmpId() {
        return ownerEmpId;
    }

    public void setOwnerEmpId(Integer ownerEmpId) {
        this.ownerEmpId = ownerEmpId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getPaymentRemark() {
        return paymentRemark;
    }

    public void setPaymentRemark(String paymentRemark) {
        this.paymentRemark = paymentRemark;
    }

    public boolean getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(boolean deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

}

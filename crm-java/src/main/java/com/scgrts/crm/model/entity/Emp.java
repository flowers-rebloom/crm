package com.scgrts.crm.model.entity;

import java.util.Date;

import com.scgrts.crm.model.enums.EmpStatus;
import com.scgrts.crm.model.enums.Gender;

/**
 * Employee（员工）类，员工也是CRM系统的用户。
 *
 * @author root
 */
public class Emp {

    private Integer empId;
    private Integer deptId;
    private String empCode;
    private String empName;
    private String empPhone;
    private String empPwd;
    private String empSalt;
    private String empEmail;
    private String empPost;
    private Gender empSex;
    private String headPhoto;
    private EmpStatus status;
    private Integer superior;
    private Date createTime;
    private Date updateTime;
    private Date lastLoginTime;
    private String lastLoginIp;

    public Integer getEmpId() {
        return empId;
    }

    public void setEmpId(Integer empId) {
        this.empId = empId;
    }

    public Integer getDeptId() {
        return deptId;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    public String getEmpCode() {
        return empCode;
    }

    public void setEmpCode(String empCode) {
        this.empCode = empCode;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEmpPhone() {
        return empPhone;
    }

    public void setEmpPhone(String empPhone) {
        this.empPhone = empPhone;
    }

    public String getEmpPwd() {
        return empPwd;
    }

    public void setEmpPwd(String empPwd) {
        this.empPwd = empPwd;
    }

    public String getEmpSalt() {
        return empSalt;
    }

    public void setEmpSalt(String empSalt) {
        this.empSalt = empSalt;
    }

    public String getEmpEmail() {
        return empEmail;
    }

    public void setEmpEmail(String empEmail) {
        this.empEmail = empEmail;
    }

    public String getEmpPost() {
        return empPost;
    }

    public void setEmpPost(String empPost) {
        this.empPost = empPost;
    }

    public Gender getEmpSex() {
        return empSex;
    }

    public void setEmpSex(Gender empSex) {
        this.empSex = empSex;
    }

    public String getHeadPhoto() {
        return headPhoto;
    }

    public void setHeadPhoto(String headPhoto) {
        this.headPhoto = headPhoto;
    }

    public EmpStatus getStatus() {
        return status;
    }

    public void setStatus(EmpStatus status) {
        this.status = status;
    }

    public Integer getSuperior() {
        return superior;
    }

    public void setSuperior(Integer superior) {
        this.superior = superior;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    @Override
    public String toString() {
        return "Emp{" +
                "empId=" + empId +
                ", deptId=" + deptId +
                ", empCode='" + empCode + '\'' +
                ", empName='" + empName + '\'' +
                ", empPhone='" + empPhone + '\'' +
                ", empPwd='" + empPwd + '\'' +
                ", empSalt='" + empSalt + '\'' +
                ", empEmail='" + empEmail + '\'' +
                ", empPost='" + empPost + '\'' +
                ", empSex=" + empSex +
                ", headPhoto='" + headPhoto + '\'' +
                ", status=" + status +
                ", superior=" + superior +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", lastLoginTime=" + lastLoginTime +
                ", lastLoginIp='" + lastLoginIp + '\'' +
                '}';
    }
}

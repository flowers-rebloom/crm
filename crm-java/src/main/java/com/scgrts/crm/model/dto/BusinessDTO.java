package com.scgrts.crm.model.dto;

import com.scgrts.crm.model.entity.Business;

public class BusinessDTO extends Business {
    private String createEmpName;
    private String ownerEmpName;
    private String custName;

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCreateEmpName() {
        return createEmpName;
    }

    public void setCreateEmpName(String createEmpName) {
        this.createEmpName = createEmpName;
    }

    public String getOwnerEmpName() {
        return ownerEmpName;
    }

    public void setOwnerEmpName(String ownerEmpName) {
        this.ownerEmpName = ownerEmpName;
    }

}

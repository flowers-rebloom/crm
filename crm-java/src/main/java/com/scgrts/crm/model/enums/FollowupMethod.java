package com.scgrts.crm.model.enums;

import java.util.HashMap;

/**
 * 性别枚举
 *
 * @author root
 */
public enum FollowupMethod {
    PHONE("打电话"), MESSAGE("发信息"), MAIL("发邮件"), FACETOFACE("见面拜访"), OTHERWAY("其它");

    private static HashMap<String, FollowupMethod> mapping = new HashMap<String, FollowupMethod>(); // 映射表

    static {
        mapping.put("打电话", PHONE);
        mapping.put("发信息", MESSAGE);
        mapping.put("发邮件", MAIL);
        mapping.put("见面拜访", FACETOFACE);
        mapping.put("其它", OTHERWAY);
    }

    /**
     * 从int值转换成Gender值
     *
     * @param v
     * @return
     */
    public static FollowupMethod fromString(String k) {
        return mapping.get(k);
    }

    private String description; // 中文的性别描述

    private FollowupMethod(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}

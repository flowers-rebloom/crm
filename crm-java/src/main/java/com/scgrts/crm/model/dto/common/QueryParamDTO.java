package com.scgrts.crm.model.dto.common;

/**
 * 封装通用的查询参数
 *
 * @author root
 */
public class QueryParamDTO {
    /**
     * 搜索关键字
     */
    private String q;
    /**
     * 每页行数
     */
    private Integer pageSize = 10;
    /**
     * 页码
     */
    private Integer pageNo = 1;

    public String getQ() {
        return q;
    }

    public void setQ(String q) {
        this.q = q;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

}

package com.scgrts.crm.model.dto;

import com.scgrts.crm.model.entity.Contract;

public class ContractDTO extends Contract {

    private String custName;
    private String businessName;
    private String ownerEmpName;
    private String companyEmpName;
    private String contactName;

    @Override

    public Integer getContactId() {
        // TODO Auto-generated method stub
        return super.getContactId();
    }

    @Override
    public void setContactId(Integer contactId) {
        // TODO Auto-generated method stub
        super.setContactId(contactId);
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getOwnerEmpName() {
        return ownerEmpName;
    }

    public void setOwnerEmpName(String ownerEmpName) {
        this.ownerEmpName = ownerEmpName;
    }

    public String getCompanyEmpName() {
        return companyEmpName;
    }

    public void setCompanyEmpName(String companyEmpName) {
        this.companyEmpName = companyEmpName;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

}

package com.scgrts.crm.model.enums;

/**
 * 员工的操作日志的对象类型枚举
 *
 * @author root
 */
public enum EmpLogObjectType {

    SYSTEM(0, "系统"), CLUE(1, "线索"), CUST(2, "客户"), BUSINESS(3, "商机"), CONTRACT(4, "合同"), PAYMENT(5, "回款"),
    CONTACT(6, "联系人");

    private int value;
    private String description;

    private EmpLogObjectType(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

}

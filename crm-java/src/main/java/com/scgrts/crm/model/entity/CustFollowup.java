package com.scgrts.crm.model.entity;

import java.util.Date;

import com.scgrts.crm.model.enums.FollowupMethod;

public class CustFollowup {
    private Integer custFollowupId;
    private Integer custId;
    private Date followupTime;
    private String custFollowupPresentation;
    private FollowupMethod followupMethod;
    private Date nextFollowupTime;
    private Date createTime;
    private Date updateTime;
    private Integer createEmpId;
    private Integer contactId;

    public Integer getCustFollowupId() {
        return custFollowupId;
    }

    public void setCustFollowupId(Integer custFollowupId) {
        this.custFollowupId = custFollowupId;
    }

    public Integer getCustId() {
        return custId;
    }

    public void setCustId(Integer custId) {
        this.custId = custId;
    }

    public Date getFollowupTime() {
        return followupTime;
    }

    public void setFollowupTime(Date followupTime) {
        this.followupTime = followupTime;
    }

    public String getCustFollowupPresentation() {
        return custFollowupPresentation;
    }

    public void setCustFollowupPresentation(String custFollowupPresentation) {
        this.custFollowupPresentation = custFollowupPresentation;
    }

    public FollowupMethod getFollowupMethod() {
        return followupMethod;
    }

    public void setFollowupMethod(FollowupMethod followupMethod) {
        this.followupMethod = followupMethod;
    }

    public Date getNextFollowupTime() {
        return nextFollowupTime;
    }

    public void setNextFollowupTime(Date nextFollowupTime) {
        this.nextFollowupTime = nextFollowupTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getCreateEmpId() {
        return createEmpId;
    }

    public void setCreateEmpId(Integer createEmpId) {
        this.createEmpId = createEmpId;
    }

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

}

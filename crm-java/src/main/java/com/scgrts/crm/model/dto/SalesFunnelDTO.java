package com.scgrts.crm.model.dto;

public class SalesFunnelDTO {
    private String name;
    private Integer valuename;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return valuename;
    }

    public void setValue(Integer valuename) {
        this.valuename = valuename;
    }

    public SalesFunnelDTO(String name, Integer valuename) {
        this.name = name;
        this.valuename = valuename;
    }

    @Override
    public String toString() {
        return "SalesFunnelDTO{" +
                "name='" + name + '\'' +
                ", valuename=" + valuename +
                '}';
    }
}

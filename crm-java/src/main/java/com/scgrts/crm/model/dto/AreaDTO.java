package com.scgrts.crm.model.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AreaDTO {
    private List<Map<String, Object>> provinceCustNums = new ArrayList<Map<String, Object>>();

    public List<Map<String, Object>> getProvinceCustNums() {
        return provinceCustNums;
    }

    public void setProvinceCustNums(List<Map<String, Object>> provinceCustNums) {
        this.provinceCustNums = provinceCustNums;
    }

    @Override
    public String toString() {
        return "AreaDTO{" +
                "provinceCustNums=" + provinceCustNums +
                '}';
    }
}

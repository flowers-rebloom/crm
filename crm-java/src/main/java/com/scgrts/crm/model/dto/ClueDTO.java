package com.scgrts.crm.model.dto;

import com.scgrts.crm.model.entity.Clue;

public class ClueDTO extends Clue {
    private String createEmpName;
    private String ownerEmpName;

    public String getCreateEmpName() {
        return createEmpName;
    }

    public void setCreateEmpName(String createEmpName) {
        this.createEmpName = createEmpName;
    }

    public String getOwnerEmpName() {
        return ownerEmpName;
    }

    public void setOwnerEmpName(String ownerEmpName) {
        this.ownerEmpName = ownerEmpName;
    }

}

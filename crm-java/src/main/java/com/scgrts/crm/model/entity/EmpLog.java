package com.scgrts.crm.model.entity;

import java.util.Date;

public class EmpLog {
    private Long empLogId;
    private String empLogMessage;
    private Date createTime;
    private Integer createEmpId;
    private Integer objectType;
    private Integer objectId;

    public Long getEmpLogId() {
        return empLogId;
    }

    public void setEmpLogId(Long empLogId) {
        this.empLogId = empLogId;
    }

    public String getEmpLogMessage() {
        return empLogMessage;
    }

    public void setEmpLogMessage(String empLogMessage) {
        this.empLogMessage = empLogMessage;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getCreateEmpId() {
        return createEmpId;
    }

    public void setCreateEmpId(Integer createEmpId) {
        this.createEmpId = createEmpId;
    }

    public Integer getObjectType() {
        return objectType;
    }

    public void setObjectType(Integer objectType) {
        this.objectType = objectType;
    }

    public Integer getObjectId() {
        return objectId;
    }

    public void setObjectId(Integer objectId) {
        this.objectId = objectId;
    }

}

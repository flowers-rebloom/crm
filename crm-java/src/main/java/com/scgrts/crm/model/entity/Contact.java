package com.scgrts.crm.model.entity;

import java.util.Date;

import com.scgrts.crm.model.enums.Gender;

public class Contact {
    private Integer contactId;
    private Integer custId;
    private String contactName;
    private String contactPost;
    private String contactPhone;
    private String contactEmail;
    private String contactAddress;
    private Gender contactGender;
    private String contactRemark;
    private Boolean isDecisionMaker;
    private Integer createEmpId;
    private Date createTime;
    private Date updateTime;
    private Integer ownerEmpId;

    public Integer getOwnerEmpId() {
        return ownerEmpId;
    }

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    public Integer getCustId() {
        return custId;
    }

    public void setCustId(Integer custId) {
        this.custId = custId;
    }

    public Boolean getIsDecisionMaker() {
        return isDecisionMaker;
    }

    public void setIsDecisionMaker(Boolean isDecisionMaker) {
        this.isDecisionMaker = isDecisionMaker;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPost() {
        return contactPost;
    }

    public void setContactPost(String contactPost) {
        this.contactPost = contactPost;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(String contactAddress) {
        this.contactAddress = contactAddress;
    }

    public Gender getContactGender() {
        return contactGender;
    }

    public void setContactGender(Gender contactGender) {
        this.contactGender = contactGender;
    }

    public String getContactRemark() {
        return contactRemark;
    }

    public void setContactRemark(String contactRemark) {
        this.contactRemark = contactRemark;
    }

    public Integer getCreateEmpId() {
        return createEmpId;
    }

    public void setCreateEmpId(Integer createEmpId) {
        this.createEmpId = createEmpId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public void setOwnerEmpId(Integer createEmpId2) {
        // TODO Auto-generated method stub

    }

}
package com.scgrts.crm.model.entity;

public class MonthEnum {

    private Integer month;
    private String description;

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}

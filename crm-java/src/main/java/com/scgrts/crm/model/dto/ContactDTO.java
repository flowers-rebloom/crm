package com.scgrts.crm.model.dto;

import com.scgrts.crm.model.entity.Contact;

public class ContactDTO extends Contact {
    private String custName;
    private String createEmpName;

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCreateEmpName() {
        return createEmpName;
    }

    public void setCreateEmpName(String createEmpName) {
        this.createEmpName = createEmpName;
    }
}

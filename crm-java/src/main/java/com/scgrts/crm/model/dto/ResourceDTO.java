package com.scgrts.crm.model.dto;

import java.util.Date;

public class ResourceDTO {
    private Integer resourceId;
    private String resourceName;
    private String phone;
    private String address;
    private Date lastFollowupTime;
    private String lastFollowupPresentation;

    public Integer getResourceId() {
        return resourceId;
    }

    public void setResourceId(Integer resourceId) {
        this.resourceId = resourceId;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getLastFollowupTime() {
        return lastFollowupTime;
    }

    public void setLastFollowupTime(Date lastFollowupTime) {
        this.lastFollowupTime = lastFollowupTime;
    }

    public String getLastFollowupPresentation() {
        return lastFollowupPresentation;
    }

    public void setLastFollowupPresentation(String lastFollowupPresentation) {
        this.lastFollowupPresentation = lastFollowupPresentation;
    }

}

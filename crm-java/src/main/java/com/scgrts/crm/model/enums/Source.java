package com.scgrts.crm.model.enums;

import java.util.HashMap;

public enum Source {

    // 0->不明确 1->促销 2->搜索引擎 3->广告 4->转介绍 5->线上注册 6->线上询价 7->预约上门 8->陌生拜访 9->电话咨询
    // 10->邮件咨询
    UNKOWN(0, "不明确"), SALES(1, "促销"), SEARCHENGINE(2, "搜索引擎"), ADV(3, "广告"), OLDCUST(4, "转介绍"), REGONLINE(5, "线上注册"),
    ASKONLINE(6, "线上询价"), YYSM(7, "预约上门"), MSBF(8, "陌生拜访"), DHZX(9, "电话咨询"), MAIL(10, "邮件咨询");

    // 初始化 int -> Source 的映射表
    private static HashMap<Integer, Source> mapping = new HashMap<Integer, Source>();

    static {
        Source[] sources = Source.values();
        for (Source item : sources) {
            mapping.put(item.getValue(), item);
        }
    }

    public static Source fromInt(int dbvalue) {
        return mapping.get(dbvalue);
    }

    private int value;
    private String description;

    private Source(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return this.description;
    }

}

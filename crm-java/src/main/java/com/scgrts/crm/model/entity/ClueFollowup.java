package com.scgrts.crm.model.entity;

import java.util.Date;

import com.scgrts.crm.model.enums.FollowupMethod;

/**
 * 实体类，和数据库的clue_followup表是对应的
 *
 * @author root
 */
public class ClueFollowup {
    private Integer clueFollowupId;
    private Integer clueId;
    private Date followupTime;
    private String clueFollowupPresentation;
    private FollowupMethod followupMethod;
    private Date nextFollowupTime;
    private Date createTime;
    private Date updateTime;
    private Integer createEmpId;

    public Integer getClueFollowupId() {
        return clueFollowupId;
    }

    public void setClueFollowupId(Integer clueFollowupId) {
        this.clueFollowupId = clueFollowupId;
    }

    public Integer getClueId() {
        return clueId;
    }

    public void setClueId(Integer clueId) {
        this.clueId = clueId;
    }

    public Date getFollowupTime() {
        return followupTime;
    }

    public void setFollowupTime(Date followupTime) {
        this.followupTime = followupTime;
    }

    public String getClueFollowupPresentation() {
        return clueFollowupPresentation;
    }

    public void setClueFollowupPresentation(String clueFollowupPresentation) {
        this.clueFollowupPresentation = clueFollowupPresentation;
    }

    public FollowupMethod getFollowupMethod() {
        return followupMethod;
    }

    public void setFollowupMethod(FollowupMethod followupMethod) {
        this.followupMethod = followupMethod;
    }

    public Date getNextFollowupTime() {
        return nextFollowupTime;
    }

    public void setNextFollowupTime(Date nextFollowupTime) {
        this.nextFollowupTime = nextFollowupTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getCreateEmpId() {
        return createEmpId;
    }

    public void setCreateEmpId(Integer createEmpId) {
        this.createEmpId = createEmpId;
    }

    @Override
    public String toString() {
        return "ClueFollowup{" +
                "clueFollowupId=" + clueFollowupId +
                ", clueId=" + clueId +
                ", followupTime=" + followupTime +
                ", clueFollowupPresentation='" + clueFollowupPresentation + '\'' +
                ", followupMethod=" + followupMethod +
                ", nextFollowupTime=" + nextFollowupTime +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", createEmpId=" + createEmpId +
                '}';
    }
}

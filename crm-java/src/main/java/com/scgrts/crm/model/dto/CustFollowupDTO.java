package com.scgrts.crm.model.dto;

import com.scgrts.crm.model.entity.CustFollowup;

public class CustFollowupDTO extends CustFollowup {
    private String contactName;

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

}

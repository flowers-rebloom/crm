package com.scgrts.crm.model.dto.common;

import java.util.List;

/**
 * 数据查询的通用结果对象
 *
 * @param <E> E表示一种类型，它可以是Clue、Emp、Cust ...
 * @author root
 */

public class QueryResultDTO<E> {
    /**
     * 查询到的数据总行数
     */
    private Integer total;
    /**
     * 当前页的数据
     */
    private List<E> list;

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getTotal() {
        return total;
    }

    public void setList(List<E> list) {
        this.list = list;
    }

    public List<E> getList() {
        return list;
    }

}

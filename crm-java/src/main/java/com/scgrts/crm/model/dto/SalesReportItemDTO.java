package com.scgrts.crm.model.dto;

public class SalesReportItemDTO {
    private Integer news;
    private Double rate;

    public Integer getNews() {
        return news;
    }

    public void setNews(Integer news) {
        this.news = news;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

}

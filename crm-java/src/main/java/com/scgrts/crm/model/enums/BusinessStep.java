package com.scgrts.crm.model.enums;

import java.util.HashMap;

public enum BusinessStep {
    VALIDATING(0, "验证客户"), REQUIREANALY(1, "需求分析"), PRICING(2, "方案报价"), END(3, "结束");

    private final int value;
    private final String description;

    private BusinessStep(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    private static HashMap<Integer, BusinessStep> mapping = new HashMap<Integer, BusinessStep>();

    static {
        mapping.put(0, VALIDATING);
        mapping.put(1, REQUIREANALY);
        mapping.put(2, PRICING);
        mapping.put(3, END);
    }

    public static BusinessStep fromInt(int k) {
        return mapping.get(k);
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return String.valueOf(this.value);
    }

}

package com.scgrts.crm.model.dto;

public class SalesReportDTO {
    private SalesReportItemDTO clue;
    private SalesReportItemDTO cust;
    private SalesReportItemDTO business;
    private SalesReportItemDTO contract;
    private SalesReportItemDTO followup;
    private SalesReportItemDTO contractMoney;
    private SalesReportItemDTO businessMoney;
    private SalesReportItemDTO paymentMoney;

    public SalesReportItemDTO getClue() {
        return clue;
    }

    public void setClue(SalesReportItemDTO clue) {
        this.clue = clue;
    }

    public SalesReportItemDTO getCust() {
        return cust;
    }

    public void setCust(SalesReportItemDTO cust) {
        this.cust = cust;
    }

    public SalesReportItemDTO getBusiness() {
        return business;
    }

    public void setBusiness(SalesReportItemDTO business) {
        this.business = business;
    }

    public SalesReportItemDTO getContract() {
        return contract;
    }

    public void setContract(SalesReportItemDTO contract) {
        this.contract = contract;
    }

    public SalesReportItemDTO getFollowup() {
        return followup;
    }

    public void setFollowup(SalesReportItemDTO followup) {
        this.followup = followup;
    }

    public SalesReportItemDTO getContractMoney() {
        return contractMoney;
    }

    public void setContractMoney(SalesReportItemDTO contractMoney) {
        this.contractMoney = contractMoney;
    }

    public SalesReportItemDTO getBusinessMoney() {
        return businessMoney;
    }

    public void setBusinessMoney(SalesReportItemDTO businessMoney) {
        this.businessMoney = businessMoney;
    }

    public SalesReportItemDTO getPaymentMoney() {
        return paymentMoney;
    }

    public void setPaymentMoney(SalesReportItemDTO paymentMoney) {
        this.paymentMoney = paymentMoney;
    }

}

package com.scgrts.crm.model.dto;

public class KPIDTO {
    private KPIItemDTO contractKpi;
    private KPIItemDTO paymentKpi;

    public KPIItemDTO getContractKpi() {
        return contractKpi;
    }

    public void setContractKpi(KPIItemDTO contractKpi) {
        this.contractKpi = contractKpi;
    }

    public KPIItemDTO getPaymentKpi() {
        return paymentKpi;
    }

    public void setPaymentKpi(KPIItemDTO paymentKpi) {
        this.paymentKpi = paymentKpi;
    }

}

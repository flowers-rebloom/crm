package com.scgrts.crm.model.dto;

import com.scgrts.crm.model.enums.BusinessStep;

/**
 * 销售漏斗实体类
 */
public class SalesFunnel {
    private Integer current_step;
    private Integer end_status;

    public Integer getCurrent_step() {
        return current_step;
    }

    public void setCurrent_step(Integer current_step) {
        this.current_step = current_step;
    }

    public Integer getEnd_status() {
        return end_status;
    }

    public void setEnd_status(Integer end_status) {
        this.end_status = end_status;
    }
}

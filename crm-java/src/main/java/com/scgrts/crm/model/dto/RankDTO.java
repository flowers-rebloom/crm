package com.scgrts.crm.model.dto;

public class RankDTO {
    private String empName;
    private Integer clues;
    private Integer contacts;
    private Integer custs;
    private Integer businesss;
    private Integer contracts;
    private Integer payments;

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public Integer getClues() {
        return clues;
    }

    public void setClues(Integer clues) {
        this.clues = clues;
    }

    public Integer getContacts() {
        return contacts;
    }

    public void setContacts(Integer contacts) {
        this.contacts = contacts;
    }

    public Integer getCusts() {
        return custs;
    }

    public void setCusts(Integer custs) {
        this.custs = custs;
    }

    public Integer getBusinesss() {
        return businesss;
    }

    public void setBusinesss(Integer businesss) {
        this.businesss = businesss;
    }

    public Integer getContracts() {
        return contracts;
    }

    public void setContracts(Integer contracts) {
        this.contracts = contracts;
    }

    public Integer getPayments() {
        return payments;
    }

    public void setPayments(Integer payments) {
        this.payments = payments;
    }

}

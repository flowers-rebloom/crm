package com.scgrts.crm.model.dto;

public class RemindDTO {
    private Integer connectBefore7Days;
    private Integer connectBefore15Days;
    private Integer connectionBefore30Days;
    private Integer conectTimePassed;

    public Integer getConnectBefore7Days() {
        return connectBefore7Days;
    }

    public void setConnectBefore7Days(Integer connectBefore7Days) {
        this.connectBefore7Days = connectBefore7Days;
    }

    public Integer getConnectBefore15Days() {
        return connectBefore15Days;
    }

    public void setConnectBefore15Days(Integer connectBefore15Days) {
        this.connectBefore15Days = connectBefore15Days;
    }

    public Integer getConnectionBefore30Days() {
        return connectionBefore30Days;
    }

    public void setConnectionBefore30Days(Integer connectionBefore30Days) {
        this.connectionBefore30Days = connectionBefore30Days;
    }

    public Integer getConectTimePassed() {
        return conectTimePassed;
    }

    public void setConectTimePassed(Integer conectTimePassed) {
        this.conectTimePassed = conectTimePassed;
    }

}

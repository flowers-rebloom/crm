package com.scgrts.crm.model.entity;

import java.math.BigDecimal;
import java.util.Date;

public class Contract {
    private Integer contractId;
    private String contractCode;
    private String contractName;
    private Integer custId;
    private Integer businessId;
    private Integer checkStatus;
    private Date orderTime;
    private Integer ownerEmpId;
    private Integer createEmpId;
    private Date createTime;
    private Date updateTime;
    private Date contractStartTime;
    private Date contractEndTime;
    private BigDecimal contractMoney;
    private Boolean paymentStatus;
    private Integer contactId;
    private Integer companyEmpId;
    private String contractRemark;
    private Boolean deleteStatus;

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public void setDeleteStatus(Boolean deleteStatus) {
        this.deleteStatus = deleteStatus;
    }

    public Boolean getDeleteStatus() {
        return deleteStatus;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    public Integer getCustId() {
        return custId;
    }

    public void setCustId(Integer custId) {
        this.custId = custId;
    }

    public Integer getBusinessId() {
        return businessId;
    }

    public void setBusinessId(int businessId2) {
        this.businessId = businessId2;
    }

    public Integer getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(int i) {
        this.checkStatus = i;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public Integer getOwnerEmpId() {
        return ownerEmpId;
    }

    public void setOwnerEmpId(Integer ownerEmpId) {
        this.ownerEmpId = ownerEmpId;
    }

    public Integer getCreateEmpId() {
        return createEmpId;
    }

    public void setCreateEmpId(Integer createEmpId) {
        this.createEmpId = createEmpId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getContractStartTime() {
        return contractStartTime;
    }

    public void setContractStartTime(Date contractStartTime) {
        this.contractStartTime = contractStartTime;
    }

    public Date getContractEndTime() {
        return contractEndTime;
    }

    public void setContractEndTime(Date contractEndTime) {
        this.contractEndTime = contractEndTime;
    }

    public BigDecimal getContractMoney() {
        return contractMoney;
    }

    public void setContractMoney(BigDecimal contractMoney) {
        this.contractMoney = contractMoney;
    }

    public void setPaymentStatus(Boolean paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Boolean getPaymentStatus() {
        return paymentStatus;
    }

    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    public Integer getCompanyEmpId() {
        return companyEmpId;
    }

    public void setCompanyEmpId(Integer companyEmpId) {
        this.companyEmpId = companyEmpId;
    }

    public String getContractRemark() {
        return contractRemark;
    }

    public void setContractRemark(String contractRemark) {
        this.contractRemark = contractRemark;
    }

}

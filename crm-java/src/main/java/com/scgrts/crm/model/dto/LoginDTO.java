package com.scgrts.crm.model.dto;

/**
 * 登录信息数据传输对象，用于封装前端传过来的用户名和密码
 *
 * @author root
 */
public class LoginDTO {

    private String username;
    private String password;
    private String loginIp;

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}

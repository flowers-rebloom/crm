package com.scgrts.crm.model.entity;

import java.util.Date;

import com.scgrts.crm.model.enums.Source;

/**
 * 和数据库的clue表对应的实体类
 *
 * @author root
 */
public class Clue {

    private Integer clueId;
    private String clueName;
    private Source clueSource;
    private String cluePhone;
    private String clueAddress;
    private String clueRemark;
    private Integer createEmpId;
    private Date createTime;
    private Date updateTime;
    private Integer ownerEmpId;
    private Boolean followupStatus;
    private Date lastFollowupTime;
    private String lastFollowupPresentation;
    private Date nextFollowupTime;

    public Integer getClueId() {
        return clueId;
    }

    public void setClueId(Integer clueId) {
        this.clueId = clueId;
    }

    public String getClueName() {
        return clueName;
    }

    public void setClueName(String clueName) {
        this.clueName = clueName;
    }

    public Source getClueSource() {
        return clueSource;
    }

    public void setClueSource(Source clueSource) {
        this.clueSource = clueSource;
    }

    public String getCluePhone() {
        return cluePhone;
    }

    public void setCluePhone(String cluePhone) {
        this.cluePhone = cluePhone;
    }

    public String getClueAddress() {
        return clueAddress;
    }

    public void setClueAddress(String clueAddress) {
        this.clueAddress = clueAddress;
    }

    public String getClueRemark() {
        return clueRemark;
    }

    public void setClueRemark(String clueRemark) {
        this.clueRemark = clueRemark;
    }

    public Integer getCreateEmpId() {
        return createEmpId;
    }

    public void setCreateEmpId(Integer createEmpId) {
        this.createEmpId = createEmpId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getOwnerEmpId() {
        return ownerEmpId;
    }

    public void setOwnerEmpId(Integer ownerEmpId) {
        this.ownerEmpId = ownerEmpId;
    }

    public Boolean getFollowupStatus() {
        return followupStatus;
    }

    public void setFollowupStatus(Boolean followupStatus) {
        this.followupStatus = followupStatus;
    }

    public Date getLastFollowupTime() {
        return lastFollowupTime;
    }

    public void setLastFollowupTime(Date lastFollowupTime) {
        this.lastFollowupTime = lastFollowupTime;
    }

    public String getLastFollowupPresentation() {
        return lastFollowupPresentation;
    }

    public void setLastFollowupPresentation(String lastFollowupPresentation) {
        this.lastFollowupPresentation = lastFollowupPresentation;
    }

    public Date getNextFollowupTime() {
        return nextFollowupTime;
    }

    public void setNextFollowupTime(Date nextFollowupTime) {
        this.nextFollowupTime = nextFollowupTime;
    }

    @Override
    public String toString() {
        return "Clue{" +
                "clueId=" + clueId +
                ", clueName='" + clueName + '\'' +
                ", clueSource=" + clueSource +
                ", cluePhone='" + cluePhone + '\'' +
                ", clueAddress='" + clueAddress + '\'' +
                ", clueRemark='" + clueRemark + '\'' +
                ", createEmpId=" + createEmpId +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", ownerEmpId=" + ownerEmpId +
                ", followupStatus=" + followupStatus +
                ", lastFollowupTime=" + lastFollowupTime +
                ", lastFollowupPresentation='" + lastFollowupPresentation + '\'' +
                ", nextFollowupTime=" + nextFollowupTime +
                '}';
    }
}

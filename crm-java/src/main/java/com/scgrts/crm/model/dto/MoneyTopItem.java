package com.scgrts.crm.model.dto;

import java.math.BigDecimal;

public class MoneyTopItem {
    private Integer empId;
    private String empName;
    private BigDecimal sumContractMoney;
    private BigDecimal sumPaymentMoney;

    public Integer getEmpId() {
        return empId;
    }

    public void setEmpId(Integer empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public BigDecimal getSumContractMoney() {
        return sumContractMoney;
    }

    public void setSumContractMoney(BigDecimal sumContractMoney) {
        this.sumContractMoney = sumContractMoney;
    }

    public BigDecimal getSumPaymentMoney() {
        return sumPaymentMoney;
    }

    public void setSumPaymentMoney(BigDecimal sumPaymentMoney) {
        this.sumPaymentMoney = sumPaymentMoney;
    }

}

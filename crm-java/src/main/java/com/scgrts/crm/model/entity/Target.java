package com.scgrts.crm.model.entity;

public class Target {

    private Integer year;
    private Integer month;
    private java.math.BigDecimal targetContractMoney;
    private java.math.BigDecimal targetPaymentMoney;

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public java.math.BigDecimal getTargetContractMoney() {
        return targetContractMoney;
    }

    public void setTargetContractMoney(java.math.BigDecimal targetContractMoney) {
        this.targetContractMoney = targetContractMoney;
    }

    public java.math.BigDecimal getTargetPaymentMoney() {
        return targetPaymentMoney;
    }

    public void setTargetPaymentMoney(java.math.BigDecimal targetPaymentMoney) {
        this.targetPaymentMoney = targetPaymentMoney;
    }

}

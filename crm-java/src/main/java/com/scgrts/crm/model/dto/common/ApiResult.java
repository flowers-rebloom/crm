package com.scgrts.crm.model.dto.common;

/**
 * 后端返回给前端的通用结果对象
 *
 * @author root
 */
public class ApiResult {
    // 业务操作结果码
    private int code;
    // 业务消息
    private String msg;
    // 业务数据
    private Object data;

    public static ApiResult build(int code, String msg, Object data) {
        ApiResult apiResult = new ApiResult();
        apiResult.setCode(code);
        apiResult.setMsg(msg);
        apiResult.setData(data);
        return apiResult;
    }

    public static ApiResult success() {
        return success(null);
    }

    public static ApiResult success(Object data) {
        return build(0, "SUCCESS", data);
    }

    public static ApiResult error(String errorMsg) {
        return build(1, errorMsg, null);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}

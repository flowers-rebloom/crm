package com.scgrts.crm.model.dto;

import java.math.BigDecimal;

public class KPIItemDTO {
    private BigDecimal target;
    private BigDecimal completed;

    public BigDecimal getTarget() {
        return target;
    }

    public void setTarget(BigDecimal target) {
        this.target = target;
    }

    public BigDecimal getCompleted() {
        return completed;
    }

    public void setCompleted(BigDecimal completed) {
        this.completed = completed;
    }

}
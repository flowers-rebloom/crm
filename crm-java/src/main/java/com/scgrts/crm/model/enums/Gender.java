package com.scgrts.crm.model.enums;

import java.util.HashMap;

/**
 * 性别枚举
 *
 * @author root
 */
public enum Gender {
    UNSET(0, "未设置"), // 等价于： Gender UNSET = new Gener(0);
    MALE(1, "男"), FEMALE(2, "女"), UNKOWN(3, "未知");

    private static HashMap<Integer, Gender> mapping = new HashMap<Integer, Gender>(); // 映射表

    static {
        mapping.put(0, UNSET);
        mapping.put(1, MALE);
        mapping.put(2, FEMALE);
        mapping.put(3, UNKOWN);
    }

    /**
     * 从int值转换成Gender值
     *
     * @param v
     * @return
     */
    public static Gender fromInt(int k) {
        return mapping.get(k);
    }

    private int value; // 对应着数据库中的int值
    private String description; // 中文的性别描述

    private Gender(int value, String description) {
        this.value = value;
        this.description = description;
    }

    public int getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return this.description;

    }
}

package com.scgrts.crm.model.dto;

public class Select2ItemDTO {
    private int id;
    private String text;

    public Select2ItemDTO(int id, String text) {

        this.id = id;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}

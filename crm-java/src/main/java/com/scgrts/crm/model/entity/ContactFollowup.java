package com.scgrts.crm.model.entity;

import java.util.Date;

import com.scgrts.crm.model.enums.FollowupMethod;

public class ContactFollowup {
    private Integer contactFollowupId;
    private Integer contactId;
    private Date followupTime;
    private String contact;
    private FollowupMethod followupMethod;
    private String contactFollowupPresentation;
    private Date createTime;
    private Date updateTime;
    private Integer createEmpId;

    public Integer getContactFollowupId() {
        return contactFollowupId;
    }

    public void setContactFollowupId(Integer contactFollowupId) {
        this.contactFollowupId = contactFollowupId;
    }

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    public Date getFollowupTime() {
        return followupTime;
    }

    public void setFollowupTime(Date followupTime) {
        this.followupTime = followupTime;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public FollowupMethod getFollowupMethod() {
        return followupMethod;
    }

    public void setFollowupMethod(FollowupMethod followupMethod) {
        this.followupMethod = followupMethod;
    }

    public String getContactFollowupPresentation() {
        return contactFollowupPresentation;
    }

    public void setContactFollowupPresentation(String contactFollowupPresentation) {
        this.contactFollowupPresentation = contactFollowupPresentation;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getCreateEmpId() {
        return createEmpId;
    }

    public void setCreateEmpId(Integer createEmpId) {
        this.createEmpId = createEmpId;
    }

}

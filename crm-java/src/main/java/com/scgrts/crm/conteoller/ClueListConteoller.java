package com.scgrts.crm.conteoller;

import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.model.dto.common.QueryParamDTO;
import com.scgrts.crm.model.dto.common.QueryResultDTO;
import com.scgrts.crm.model.entity.Clue;
import com.scgrts.crm.service.ClueService;
import com.scgrts.crm.service.impl.ClueServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 查询所有线索
 */
@WebServlet("/clue/list")
public class ClueListConteoller extends BaseController {
    private ClueService clueService = new ClueServiceImpl();

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String q = req.getParameter("q");
        String pageSize = req.getParameter("pageSize");
        String pageNo = req.getParameter("pageNo");
        QueryParamDTO queryParamDTO = new QueryParamDTO();
        queryParamDTO.setQ(q);
        if (pageNo != null)
            queryParamDTO.setPageNo(Integer.parseInt(pageNo));
        if (pageSize != null)
            queryParamDTO.setPageSize(Integer.parseInt(pageSize));
        // 调用Service组件进行查询
        QueryResultDTO<Clue> queryResultDTO = clueService.findPage(queryParamDTO);
        // 响应
        return ApiResult.success(queryResultDTO);
    }
}

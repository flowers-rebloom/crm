package com.scgrts.crm.conteoller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.model.dto.common.QueryParamDTO;
import com.scgrts.crm.model.dto.common.QueryResultDTO;
import com.scgrts.crm.model.entity.Cust;
import com.scgrts.crm.service.CustService;

import com.scgrts.crm.service.impl.CustServiceImpl;

@WebServlet("/cust/list")
public class CustListController extends BaseController {
    private static final long serialVersionUID = 8530423064529466911L;
    private CustService custService = new CustServiceImpl();

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        // 获取请求参数
        String q = req.getParameter("q");
        String pageSize = req.getParameter("pageSize");
        String pageNo = req.getParameter("pageNo");
        QueryParamDTO queryParamDTO = new QueryParamDTO();
        queryParamDTO.setQ(q);
        if (pageNo != null)
            queryParamDTO.setPageNo(Integer.parseInt(pageNo));
        if (pageSize != null)
            queryParamDTO.setPageSize(Integer.parseInt(pageSize));
        // 调用Service组件进行查询
        QueryResultDTO<Cust> queryResultDTO = this.custService.findPage(queryParamDTO);
        // 响应
        return ApiResult.success(queryResultDTO);
    }

}

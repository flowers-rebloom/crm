package com.scgrts.crm.conteoller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.model.entity.Cust;
import com.scgrts.crm.model.enums.Source;
import com.scgrts.crm.service.CustService;
import com.scgrts.crm.service.impl.CustServiceImpl;

import cn.hutool.core.date.DateUtil;

/**
 * 新建客户页面的controller类
 *
 * @author Administrator
 */
@WebServlet("/cust/create")
public class CustCreateController extends BaseController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private CustService custservice = new CustServiceImpl();

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        // 接收参数

        String custName = req.getParameter("clientName");
        String custSource = req.getParameter("clientResource");
        String custPhone = req.getParameter("tele");
        String webnet = req.getParameter("webNet");
        String province = req.getParameter("provience");
        String city = req.getParameter("city");
        String area = req.getParameter("area");
        String custAddress = req.getParameter("address");
        String followupStatus = req.getParameter("followupstate");
        String followupTime = req.getParameter("followupTime");
        String followupPre = req.getParameter("followupContent");
        String nextFollowupTime = req.getParameter("nextTime");
        String custRemark = req.getParameter("remark");
        Cust cust = new Cust();
        cust.setCustName(custName);
        cust.setCustSource(Source.fromInt(Integer.parseInt(custSource)));
        cust.setCustPhone(custPhone);
        cust.setCustWebsite(webnet);
        cust.setCustProvince(Integer.parseInt(province));
        cust.setCustCity(Integer.parseInt(city));
        cust.setCustDistrict(Integer.parseInt(area));
        cust.setCustAddress(custAddress);
        cust.setFollowupStatus("1".equals(followupStatus));
        cust.setLastFollowupTime(DateUtil.parse(followupTime));
        cust.setLastFollowupPresentation(followupPre);
        cust.setNextFollowupTime(DateUtil.parse(nextFollowupTime));
        cust.setCustRemark(custRemark);
        System.out.println(">>>>>>>" + cust);
        boolean save = custservice.save(cust);
        // 响应结果
        return ApiResult.success();
    }

}

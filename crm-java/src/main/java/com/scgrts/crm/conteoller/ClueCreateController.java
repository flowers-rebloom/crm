package com.scgrts.crm.conteoller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.scgrts.crm.dao.EmpDao;
import com.scgrts.crm.dao.impl.EmpDaoImpl;
import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.model.entity.Clue;
import com.scgrts.crm.model.entity.Emp;
import com.scgrts.crm.model.enums.Source;
import com.scgrts.crm.service.ClueService;
import com.scgrts.crm.service.EmpService;
import com.scgrts.crm.service.impl.ClueServiceImpl;

import cn.hutool.core.date.DateUtil;
import com.scgrts.crm.service.impl.EmpServiceImpl;

@WebServlet("/clue/create")
public class ClueCreateController extends BaseController {

    private ClueService clueService = new ClueServiceImpl();
    private EmpDao empDAO = new EmpDaoImpl();

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        // 取到浏览器传过来的参数
        String clueName = req.getParameter("clueName");
        String clueSource = req.getParameter("clueSource");
        String cluePhone = req.getParameter("cluePhone");
        String clueAddress = req.getParameter("clueAddress");
        String followupStatus = req.getParameter("followupStatus");
        String lastFollowupTime = req.getParameter("lastFollowupTime");
        String lastFollowupPresentation = req.getParameter("lastFollowupPresentation");
        String clueRemark = req.getParameter("clueRemark");
        String nextFollowupTime = req.getParameter("nextFollowupTime");
        // 封装成实体
        Clue entity = new Clue();
        entity.setClueName(clueName);
        if (clueSource != null)
            entity.setClueSource(Source.fromInt(Integer.parseInt(clueSource)));
        entity.setCluePhone(cluePhone);
        entity.setClueAddress(clueAddress);
        entity.setFollowupStatus("1".equals(followupStatus));
        entity.setLastFollowupTime(DateUtil.parse(lastFollowupTime));
        entity.setLastFollowupPresentation(lastFollowupPresentation);
        entity.setClueRemark(clueRemark);
        entity.setNextFollowupTime(DateUtil.parse(nextFollowupTime));
        // 调用service组件
        boolean save = clueService.save(entity);
        if (save) {
            return ApiResult.success();
        }
        // 响应
        return ApiResult.success();
    }

}

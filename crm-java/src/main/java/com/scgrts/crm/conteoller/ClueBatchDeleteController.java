package com.scgrts.crm.conteoller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.service.ClueService;
import com.scgrts.crm.service.impl.ClueServiceImpl;

@WebServlet("/clue/batch-delete")
public class ClueBatchDeleteController extends BaseController {
    private ClueService clueService = new ClueServiceImpl();

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String[] strId = req.getParameterValues("id[]");
        Integer[] ids = new Integer[strId.length];
        for (int i = 0; i < ids.length; i++) {
            //parseInt() 方法用于将字符串参数作为有符号的十进制整数进行解析。
            ids[i] = Integer.parseInt(strId[i]);
        }
        boolean b = clueService.batchDelete(ids);
        if (!b) {
            return ApiResult.success();
        }
        return ApiResult.success();
    }
}
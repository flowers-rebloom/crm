package com.scgrts.crm.conteoller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.scgrts.crm.model.dto.CustFollowupDTO;
import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.model.enums.FollowupMethod;
import com.scgrts.crm.service.CustService;
import com.scgrts.crm.service.impl.CustServiceImpl;

import cn.hutool.core.date.DateUtil;

@WebServlet("/cust/followup")
public class CustfollowupController extends BaseController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private CustService custservice = new CustServiceImpl();

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        // 新建跟进的参数
        String followupTime = req.getParameter("followupTime");
        String contactMan = req.getParameter("linkman");
        String followupMethod = req.getParameter("followupMethod");
        String followupContent = req.getParameter("followupNote");
        String nextFollowupTime = req.getParameter("nextfollowupTime");
        String custId = req.getParameter("custid");
        CustFollowupDTO custfollowup = new CustFollowupDTO();
        custfollowup.setCustId(Integer.parseInt(custId));
        custfollowup.setContactName(contactMan);
        custfollowup.setFollowupTime(DateUtil.parse(followupTime));
        custfollowup.setFollowupMethod(FollowupMethod.fromString(followupMethod));
        custfollowup.setCustFollowupPresentation(followupContent);
        custfollowup.setNextFollowupTime(DateUtil.parse(nextFollowupTime));
        this.custservice.saveFollowup(custfollowup);
        // 响应结果
        return ApiResult.success();
    }

}

package com.scgrts.crm.conteoller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.service.ClueService;
import com.scgrts.crm.service.impl.ClueServiceImpl;

@WebServlet("/clue/change-into-cust")
public class ClueChangeIntoCustController extends BaseController {
    private ClueService clueService = new ClueServiceImpl();

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        // 获取请求参数
        if (req.getParameter("id") == null) {
            return ApiResult.error("没收到id");
        }
        int id = Integer.parseInt(req.getParameter("id"));
        boolean b = clueService.changeInotoCust(id);
        if (b) {
            return ApiResult.success();
        }
        return ApiResult.success();
    }

}

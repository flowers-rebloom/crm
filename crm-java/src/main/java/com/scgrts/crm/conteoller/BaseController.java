package com.scgrts.crm.conteoller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.scgrts.crm.dao.EmpDao;
import com.scgrts.crm.dao.impl.EmpDaoImpl;
import com.scgrts.crm.dao.impl.JdbcTemplate;
import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.model.entity.Emp;
import com.scgrts.crm.service.impl.WhoAmI;

public abstract class BaseController extends HttpServlet {
    private EmpDao empDAO = new EmpDaoImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcessRequest(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doProcessRequest(req, resp);
    }

    protected void doProcessRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        // 获取那个自定义请求头whoami
        String whoami = req.getHeader("whoami");
        if (whoami != null) {
            // 查出当前用户的id
            Emp emp = empDAO.findByCode(whoami);
            if (emp != null) {
                // 放入WhoAmI这个类的静态属性当中，这样呢，我们那些Service层的方法需要获取当前用户id的时候就可以直接调用WhoAmI.id()
                WhoAmI.set(emp.getEmpId());
            }
        }
        // 处理请求，得到ApiResult
        ApiResult apiResult = processRequest(req, resp);
        // 序列化成JSON串，输出到浏览器
        String json = JSON.toJSONString(apiResult, SerializerFeature.WriteEnumUsingToString,
                SerializerFeature.PrettyFormat);
        PrintWriter out = resp.getWriter();
        out.write(json);
        JdbcTemplate.closeConnection();
    }

    /**
     * 处理请求
     *
     * @param req
     * @param resp
     * @return
     * @throws ServletException
     * @throws IOException
     */

    protected abstract ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException;
}

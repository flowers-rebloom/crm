package com.scgrts.crm.conteoller;

import com.scgrts.crm.model.dto.AreaDTO;
import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.service.CustService;
import com.scgrts.crm.service.impl.CustServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/area")
public class CustAreaController extends BaseController {
    private CustService custService = new CustServiceImpl();

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AreaDTO areaDTO = custService.findCustArea();
        return ApiResult.build(0, "success", areaDTO);
    }
}

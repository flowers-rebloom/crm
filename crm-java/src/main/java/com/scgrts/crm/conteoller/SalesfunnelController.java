package com.scgrts.crm.conteoller;

import cn.hutool.core.date.DateUtil;
import com.scgrts.crm.model.dto.SalesFunnelDTO;
import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.service.SalesFunnelService;
import com.scgrts.crm.service.impl.SalesFunnelServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/salesfunnel")
public class SalesfunnelController extends BaseController {
    private SalesFunnelService service = new SalesFunnelServiceImpl();

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String dateBegin = req.getParameter("dateBegin");
        String dateEnd = req.getParameter("dateEnd");
        List<SalesFunnelDTO> list = service.moneySalesFunnel(DateUtil.parse(dateBegin), DateUtil.parse(dateEnd));
        return ApiResult.success(list);
    }
}

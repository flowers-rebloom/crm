package com.scgrts.crm.conteoller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.model.entity.Clue;
import com.scgrts.crm.model.enums.Source;
import com.scgrts.crm.service.ClueService;
import com.scgrts.crm.service.impl.ClueServiceImpl;

import cn.hutool.core.date.DateUtil;

@WebServlet("/clue/update")
public class ClueUpdateController extends BaseController {
    private ClueService clueService = new ClueServiceImpl();

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        // 获取请求参数
        int clueId = Integer.parseInt(req.getParameter("clueId"));
        String clueName = req.getParameter("clueName");
        int clueSource = Integer.parseInt(req.getParameter("clueSource"));
        String cluePhone = req.getParameter("cluePhone");
        String clueAddress = req.getParameter("clueAddress");
        String clueRemark = req.getParameter("clueRemark");
        String nextFollowupTime = req.getParameter("nextFollowupTime");
        Clue entity = clueService.findById(clueId);
        entity.setClueId(clueId);
        entity.setClueName(clueName);
        entity.setClueSource(Source.fromInt(clueSource));
        entity.setCluePhone(cluePhone);
        entity.setClueAddress(clueAddress);
        entity.setClueRemark(clueRemark);
        entity.setNextFollowupTime(DateUtil.parse(nextFollowupTime));
        // 更新
        boolean update = clueService.update(entity);

        if (update) {
            return ApiResult.success();
        }
        // 响应
        return ApiResult.success();
    }

}

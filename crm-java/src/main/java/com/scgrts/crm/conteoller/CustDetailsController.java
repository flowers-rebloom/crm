package com.scgrts.crm.conteoller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.scgrts.crm.model.dto.CustDTO;
import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.model.entity.Contact;
import com.scgrts.crm.model.entity.CustFollowup;
import com.scgrts.crm.service.CustService;
import com.scgrts.crm.service.impl.CustServiceImpl;

/**
 * 客戶详情模块的controller类
 *
 * @author Administrator
 */
@WebServlet("/cust/detail")
public class CustDetailsController extends BaseController {

    private CustService custservice = new CustServiceImpl();

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
//      查询详情的参数
        String custid = req.getParameter("id");
        int id = Integer.parseInt(custid);
        CustDTO custdetail = custservice.findDetailsById(id);

//     返回跟进记录的参数
        List<CustFollowup> followupList = custservice.findFollowup(id);
//		返回联系人
        List<Contact> contacts = custservice.findContacts(id);
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("baseinfo", custdetail);
        data.put("followups", followupList);
        data.put("contacts", contacts);
//    响应结果
        return ApiResult.success(data);
    }
}

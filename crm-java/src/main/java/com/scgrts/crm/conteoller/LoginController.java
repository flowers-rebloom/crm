package com.scgrts.crm.conteoller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.scgrts.crm.model.dto.EmpLoginResultDTO;
import com.scgrts.crm.model.dto.LoginDTO;
import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.model.entity.Emp;
import com.scgrts.crm.service.LoginService;
import com.scgrts.crm.service.impl.LoginServiceImpl;
import cn.hutool.core.bean.BeanUtil;

@WebServlet("/login")
public class LoginController extends BaseController {


    // Service业务组件
    private LoginService loginService = new LoginServiceImpl();

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        // 获取浏览器传过来的参数
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String loginIp = req.getRemoteAddr();
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setUsername(username);
        loginDTO.setPassword(password);
        loginDTO.setLoginIp(loginIp);
        // 调用ILoginService组件的方法进行身份认证
        boolean success = this.loginService.login(loginDTO);
        ApiResult apiResult = null;
        if (success) {
            Emp emp = this.loginService.findEmpByName(username);
            EmpLoginResultDTO dto = BeanUtil.copyProperties(emp, EmpLoginResultDTO.class);
            apiResult = ApiResult.success(dto);
        } else {
            apiResult = ApiResult.error("用户名或密码错误");
        }
        return apiResult;
    }

}

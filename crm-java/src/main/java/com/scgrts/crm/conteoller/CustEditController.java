package com.scgrts.crm.conteoller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.model.entity.Cust;
import com.scgrts.crm.service.CustService;
import com.scgrts.crm.service.impl.CustServiceImpl;

/**
 * 编辑客户页面返回源客户的controller类
 *
 * @author Administrator
 */
@WebServlet("/cust/edit")
public class CustEditController extends BaseController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private CustService custservice = new CustServiceImpl();

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        // 接收参数
        String clueid = req.getParameter("id");
        // 调用service层的更新方法
        Cust cust = custservice.findById(Integer.parseInt(clueid));
        // 响应结果
        return ApiResult.success(cust);
    }

}

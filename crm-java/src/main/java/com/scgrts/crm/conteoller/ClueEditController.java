package com.scgrts.crm.conteoller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.model.entity.Clue;
import com.scgrts.crm.service.ClueService;
import com.scgrts.crm.service.impl.ClueServiceImpl;

@WebServlet("/clue/edit")
public class ClueEditController extends BaseController {
    private ClueService clueService = new ClueServiceImpl();

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        // 获取请求参数
        int id = Integer.parseInt(req.getParameter("id"));
        // 查详细信息
        Clue clue = this.clueService.findById(id);
        // 响应
        return ApiResult.success(clue);
    }

}

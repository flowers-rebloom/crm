package com.scgrts.crm.conteoller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.service.CustService;
import com.scgrts.crm.service.impl.CustServiceImpl;

/**
 * 批量删除客户的controller类
 */
@WebServlet("/cust/batchdelete")
public class CustBatchDeletController extends BaseController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private CustService custservice = new CustServiceImpl();

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String[] strid = req.getParameterValues("oneofcheckedId");
        Integer[] id = new Integer[strid.length];
        for (int i = 0; i < id.length; i++) {
            id[i] = Integer.parseInt(strid[i]);
        }
        custservice.batchDelete(id);
        return ApiResult.success();
    }

}
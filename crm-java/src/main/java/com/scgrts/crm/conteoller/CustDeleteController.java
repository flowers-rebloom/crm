package com.scgrts.crm.conteoller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 删除单个线索的controller类
 */
import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.service.CustService;
import com.scgrts.crm.service.impl.CustServiceImpl;

@WebServlet("/cust/delete")
public class CustDeleteController extends BaseController {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private CustService custservice = new CustServiceImpl();

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String custid = req.getParameter("id");
        this.custservice.delete(Integer.parseInt(custid));
        return ApiResult.success();
    }

}

package com.scgrts.crm.conteoller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.scgrts.crm.model.dto.common.ApiResult;

@WebServlet("/cust/find-for-select")
public class CustFindForSelectController extends BaseController {

    /**
     *
     */
    private static final long serialVersionUID = 3094740669205567011L;

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        System.out.println("aaaa");
        String q = req.getParameter("q");
        // 假的数据
        ArrayList<CustForSelect> list = new ArrayList<>();
        list.add(new CustForSelect(1, "Bella"));
        list.add(new CustForSelect(2, "LUCY"));
        list.add(new CustForSelect(3, "Tony"));
        list.add(new CustForSelect(4, "Luccas"));
        list.add(new CustForSelect(5, "john"));
        list.add(new CustForSelect(1, "LUY"));

        // select * from list where text like '%q%'
        List<CustForSelect> results = list.stream().filter(item -> item.getText().contains(q))
                .collect(Collectors.toList());

        return ApiResult.success(results);
    }

    private class CustForSelect {
        private int id;
        private String text;

        public CustForSelect(int id, String text) {
            super();
            this.id = id;
            this.text = text;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }

    }

}
package com.scgrts.crm.conteoller;

import cn.hutool.core.date.DateUtil;
import com.scgrts.crm.model.dto.MoneyTopItem;
import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.service.StatisticsService;
import com.scgrts.crm.service.impl.StatisticsServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/contract/money")
public class ContractContrller extends BaseController {
    private StatisticsService service = new StatisticsServiceImpl();

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String dateBegin = req.getParameter("dateBegin");
        String dateEnd = req.getParameter("dateEnd");
        List<MoneyTopItem> list = service.moneyStatistics(DateUtil.parse(dateBegin), DateUtil.parse(dateEnd));
        return ApiResult.success(list);
    }
}

package com.scgrts.crm.conteoller;

import cn.hutool.core.date.DateUtil;
import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.model.entity.ClueFollowup;
import com.scgrts.crm.model.enums.FollowupMethod;
import com.scgrts.crm.service.ClueService;
import com.scgrts.crm.service.impl.ClueServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/clue/write-followup")
public class ClueFollowupController extends BaseController {
    private ClueService clueService = new ClueServiceImpl();

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String followupTime = req.getParameter("followupTime");
        String followupMethod = req.getParameter("followupMethod");
        String clueFollowupPresentation = req.getParameter("clueFollowupPresentation");
        String nextFollowupTime = req.getParameter("nextFollowupTime");
        String clueId = req.getParameter("clueId");
        ClueFollowup clueFollowup = new ClueFollowup();
        clueFollowup.setClueId(Integer.parseInt(clueId));
        clueFollowup.setFollowupMethod(FollowupMethod.fromString(followupMethod));
        clueFollowup.setFollowupTime(DateUtil.parse(followupTime));
        clueFollowup.setClueFollowupPresentation(clueFollowupPresentation);
        clueFollowup.setNextFollowupTime(DateUtil.parse(nextFollowupTime));
        boolean saveFollowup = clueService.saveFollowup(clueFollowup);
        if (saveFollowup) {
            return ApiResult.success();
        }
        return ApiResult.success();
    }
}

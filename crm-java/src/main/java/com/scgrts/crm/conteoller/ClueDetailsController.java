package com.scgrts.crm.conteoller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.scgrts.crm.model.dto.ClueDTO;
import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.model.entity.ClueFollowup;
import com.scgrts.crm.service.ClueService;
import com.scgrts.crm.service.impl.ClueServiceImpl;

@WebServlet("/clue/details")
public class ClueDetailsController extends BaseController {
    private ClueService clueService = new ClueServiceImpl();

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        // 获取请求参数
        Integer id = Integer.parseInt(req.getParameter("id"));
        // 查详细信息
        ClueDTO clueDTO = clueService.findDetailsById(id);
        List<ClueFollowup> followups = clueService.findFollowup(id);
        // 响应
        HashMap<String, Object> data = new HashMap<String, Object>();
        data.put("baseInfo", clueDTO);
        data.put("followups", followups);
        return ApiResult.success(data);
    }

}

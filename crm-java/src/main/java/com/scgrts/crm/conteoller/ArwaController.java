package com.scgrts.crm.conteoller;

import com.scgrts.crm.model.dto.common.ApiResult;
import com.scgrts.crm.model.entity.Area;
import com.scgrts.crm.service.AreaService;
import com.scgrts.crm.service.impl.AreaServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/cust/area")
public class ArwaController extends BaseController {
    private AreaService areaService = new AreaServiceImpl();

    @Override
    protected ApiResult processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String parentId = req.getParameter("parentid");
        List<Area> list = this.areaService.findAreaByParentId(Integer.parseInt(parentId));
        return ApiResult.success(list);
    }
}

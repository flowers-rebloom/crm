package com.scgrts.crm.service;

import com.scgrts.crm.model.dto.AreaDTO;
import com.scgrts.crm.model.dto.CustDTO;
import com.scgrts.crm.model.entity.Cust;
import com.scgrts.crm.model.entity.CustFollowup;


/**
 * 客户模块业务逻辑接口
 */
public interface CustService extends SuperService<Cust, CustDTO>, SuperFollowupService<CustFollowup>, SuperNameService {
    /**
     * 改变指定客户的成交状态
     *
     * @param id     客户id
     * @param status 成交状态
     * @return
     */
    boolean setDealStatus(Integer id, boolean status);

    /**
     * 改变指定客户的锁定状态
     *
     * @param id     客户id
     * @param status 成交状态
     * @return
     */
    boolean setLockStatus(Integer id, boolean status);

    /**
     * 把指定的客户放入公海
     *
     * @param id 客户id
     * @return
     */
    boolean putIntoPublicSea(Integer id);

    boolean delete(Integer id);

    /**
     * 查询所有地区
     *
     * @return
     */
    AreaDTO findCustArea();
}

package com.scgrts.crm.service;

import com.scgrts.crm.model.dto.PaymentDTO;
import com.scgrts.crm.model.entity.Payment;

/**
 * 回款管理模块业务接口
 */
public interface PaymentService extends SuperService<Payment, PaymentDTO> {
    /**
     * 变更指定实体的负责人
     *
     * @param entityId 实体id，它可能是custId/bisinessId/contractId
     * @param empId    负责人id
     * @return
     */
    boolean setOwner(Integer entityId, Integer empId);

}

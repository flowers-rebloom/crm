package com.scgrts.crm.service.impl;

import com.scgrts.crm.dao.*;
import com.scgrts.crm.dao.impl.*;
import com.scgrts.crm.model.dto.ClueDTO;
import com.scgrts.crm.model.dto.common.QueryParamDTO;
import com.scgrts.crm.model.dto.common.QueryResultDTO;
import com.scgrts.crm.model.entity.*;
import com.scgrts.crm.model.enums.EmpLogObjectType;
import com.scgrts.crm.service.ClueService;

import java.util.Date;
import java.util.List;

/**
 * 线索业务逻辑实现类
 */

public class ClueServiceImpl implements ClueService {

    private ClueDao clueDao = new ClueDaoImpl();// 这是操作线索表的DAO对象
    private EmpLogDao empLogDAO = new EmpLogDaoImpl(); // 这是操作员工日志表的DAO对象
    private ClueFollowupDao clueFollowupDAO = new ClueFollowupDaoImpl(); // 这是操作线索记录表的DAO对象
    private CustDao custDao = new CustDaoImpl();//客户表对象
    private CustFollowupDao custFollowupDao = new CustFollowupDaoImpl();//客户表记录对象

    @Override
    public boolean save(Clue obj) {
        obj.setCreateEmpId(WhoAmI.id());
        obj.setCreateTime(new Date());//设置创建时间
        obj.setOwnerEmpId(obj.getCreateEmpId());//设置负责人与创建人一样
        Integer save = clueDao.save(obj);//添加数据
        EmpLog empLog = EmpLogBuilder.buildLog("新建", EmpLogObjectType.CLUE, obj.getClueName(), obj.getClueId());
        Integer integer = empLogDAO.sevEmpLog(empLog);
        if (save > 0 & integer > 0) {
            return true;
        }
        return false;
    }

    @Override
    public QueryResultDTO<Clue> findPage(QueryParamDTO queryParamDTO) {
        QueryResultDTO<Clue> queryResultDTO = new QueryResultDTO<Clue>();
        if (queryParamDTO.getQ() == null) {
            List<Clue> list = clueDao.findAll(queryParamDTO);
            queryResultDTO.setTotal(list.size());
            queryResultDTO.setList(list);
            return queryResultDTO;
        }
        Integer total = clueDao.findTotal(queryParamDTO.getQ());
        // 查当前页数据
        List<Clue> list = clueDao.findPage(queryParamDTO);
        // 把2个结果封装成一个QueryResultDTO实例并返回
        queryResultDTO.setTotal(total);
        queryResultDTO.setList(list);
        return queryResultDTO;
    }

    @Override
    public Clue findById(Integer id) {
        Clue clue = clueDao.findById(id);
        if (clue == null) {
            throw new RuntimeException("线索" + id + "不存在");
        }
        return clue;
    }

    @Override
    public ClueDTO findDetailsById(Integer id) {
        ClueDTO detailsById = clueDao.findDetailsById(id);
        return detailsById;
    }

    @Override
    public boolean update(Clue obj) {
        if (obj.getOwnerEmpId() == 0) {
            obj.setOwnerEmpId(obj.getCreateEmpId());
        }
        obj.setUpdateTime(new Date());
        Integer update = clueDao.update(obj);
        EmpLog log = EmpLogBuilder.buildLog("修改", EmpLogObjectType.CLUE, obj.getClueName(), obj.getClueId());
        Integer integer = empLogDAO.sevEmpLog(log);
        if (update > 0 & integer > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean batchDelete(Integer[] id) {
        if (id == null || id.length == 0) {
            return false;
        }
        Integer delete = clueDao.batchDelete(id);
        if (delete != id.length) {
            return false;
        }
        return true;
    }

    @Override
    public boolean saveFollowup(ClueFollowup obj) {
        obj.setCreateEmpId(WhoAmI.id());
        JdbcTemplate.startTrasnaction();
        obj.setCreateTime(new Date());
        obj.setUpdateTime(new Date());
        Integer integer = clueFollowupDAO.saveFollowup(obj);
        Clue clue = clueDao.findById(obj.getClueId());
        clue.setFollowupStatus(true);
        clue.setLastFollowupTime(obj.getFollowupTime());
        clue.setLastFollowupPresentation(obj.getClueFollowupPresentation());
        clue.setNextFollowupTime(obj.getNextFollowupTime());
        clue.setUpdateTime(new Date());
        Integer update = clueDao.update(clue);
        JdbcTemplate.commit();
        JdbcTemplate.closeConnection();
        if (update > 0 & integer > 0) {
            return true;
        }


        return false;
    }

    @Override
    public List<ClueFollowup> findFollowup(Integer id) {
        List<ClueFollowup> followup = clueFollowupDAO.findFollowup(id);
        return followup;
    }

    @Override
    public boolean changeInotoCust(Integer id) {
        // 查出线索
        Clue clue = this.clueDao.findById(id);
        // 插入到客户表
        Cust cust = new Cust();
        cust.setCustName(clue.getClueName());
        cust.setCustSource(clue.getClueSource());
        cust.setCustPhone(clue.getCluePhone());
        cust.setCustAddress(clue.getClueAddress());
        cust.setCustRemark(clue.getClueRemark());
        cust.setCreateEmpId(clue.getCreateEmpId());
        cust.setCreateTime(clue.getCreateTime());
        cust.setUpdateTime(clue.getUpdateTime());
        cust.setOwnerEmpId(clue.getOwnerEmpId());
        cust.setFollowupStatus(clue.getFollowupStatus());
        cust.setLastFollowupTime(clue.getLastFollowupTime());
        cust.setLastFollowupPresentation(clue.getLastFollowupPresentation());
        cust.setNextFollowupTime(clue.getNextFollowupTime());
        cust.setLockStatus(false);
        cust.setDealStatus(false);
        cust.setDeleteStatus(false);
        Integer save = custDao.save(cust);
        // 查出线索跟进
        List<ClueFollowup> clueFollowups = this.clueFollowupDAO.findFollowup(id);
        // 插入到客户跟进表
        if (clueFollowups != null) {
            for (ClueFollowup clueFollowup : clueFollowups) {
                CustFollowup custFollowup = new CustFollowup();
                custFollowup.setCreateEmpId(clueFollowup.getCreateEmpId());
                custFollowup.setCreateTime(clueFollowup.getCreateTime());
                custFollowup.setCustFollowupPresentation(clueFollowup.getClueFollowupPresentation());
                custFollowup.setCustId(cust.getCustId());
                custFollowup.setFollowupMethod(clueFollowup.getFollowupMethod());
                custFollowup.setFollowupTime(clueFollowup.getFollowupTime());
                custFollowup.setNextFollowupTime(clueFollowup.getNextFollowupTime());
                custFollowup.setUpdateTime(clueFollowup.getUpdateTime());
                this.custFollowupDao.saveFollowup(custFollowup);
            }
        }
        // 删除线索跟进
        Integer integer = clueFollowupDAO.deleteByClueId(id);
        // 删除线索
        Integer delete = clueDao.delete(id);
        // 生成日志
        EmpLog log = EmpLogBuilder.buildLog("转为客户", EmpLogObjectType.CLUE, clue.getClueName(), id);
        Integer empLog = empLogDAO.sevEmpLog(log);
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        Integer delete = clueDao.delete(id);
        if (delete <= 0) {
            return false;
        }
        return true;
    }

}

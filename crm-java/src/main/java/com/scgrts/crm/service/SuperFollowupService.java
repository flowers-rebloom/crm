package com.scgrts.crm.service;

import java.util.List;

/**
 * 跟进记录父类
 */
public interface SuperFollowupService<EFollowup> {

    /**
     * 保存新的跟进
     *
     * @param obj
     * @return
     */
    boolean saveFollowup(EFollowup obj);

    /**
     * 根据线索id查找跟进记录,安时间升序排序
     *
     * @return
     */
    List<EFollowup> findFollowup(Integer id);
}

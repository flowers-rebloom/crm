package com.scgrts.crm.service.impl;

import com.scgrts.crm.dao.AreaDao;
import com.scgrts.crm.dao.impl.AreaDaoImpl;
import com.scgrts.crm.model.entity.Area;
import com.scgrts.crm.service.AreaService;

import java.util.List;

/**
 * 客户分布接口
 */
public class AreaServiceImpl implements AreaService {

    private AreaDao areaDao = new AreaDaoImpl();


    @Override
    public List<Area> findAreaByParentId(Integer parentId) {
        List<Area> arealist = areaDao.findByParentId(parentId);
        return arealist;
    }
}

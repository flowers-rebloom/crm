package com.scgrts.crm.service.impl;

import com.scgrts.crm.dao.BusinessDao;
import com.scgrts.crm.dao.BusinessFollowupDao;
import com.scgrts.crm.dao.EmpLogDao;
import com.scgrts.crm.dao.impl.BusinessDaoImpl;
import com.scgrts.crm.dao.impl.BusinessFollowupDaoImpl;
import com.scgrts.crm.dao.impl.EmpLogDaoImpl;
import com.scgrts.crm.model.dto.BusinessDTO;
import com.scgrts.crm.model.dto.common.QueryParamDTO;
import com.scgrts.crm.model.dto.common.QueryResultDTO;
import com.scgrts.crm.model.entity.*;
import com.scgrts.crm.model.enums.EmpLogObjectType;
import com.scgrts.crm.service.BusinessService;

import java.util.Date;
import java.util.List;

/**
 * 商机管理模块实现类
 */
public class BusinessServiceImpl implements BusinessService {
    private BusinessDao businessDao = new BusinessDaoImpl();
    private BusinessFollowupDao businessFollowupDao = new BusinessFollowupDaoImpl();
    private EmpLogDao empLogDao = new EmpLogDaoImpl();

    @Override
    public boolean save(Business obj) {
        obj.setCreateEmpId(WhoAmI.id());
        obj.setOwnerEmpId(obj.getCreateEmpId());
        obj.setCreateTime(new Date());
        obj.setDeleteStatus(false);
        Integer save = businessDao.save(obj);
        EmpLog log = EmpLogBuilder.buildLog("新建", EmpLogObjectType.BUSINESS, obj.getBusinessName(),
                obj.getBusinessId());
        Integer sevEmpLog = empLogDao.sevEmpLog(log);
        if (save > 0 & sevEmpLog > 0) {
            return true;
        }
        return false;
    }

    @Override
    public QueryResultDTO<Business> findPage(QueryParamDTO queryParamDTO) {
        Integer total = businessDao.findTotal(queryParamDTO.getQ());
        List<Business> list = businessDao.findPage(queryParamDTO);
        // 把2个结果封装成一个QueryResultDTO实例并返回
        QueryResultDTO<Business> queryResultDTO = new QueryResultDTO<Business>();
        queryResultDTO.setTotal(total);
        queryResultDTO.setList(list);
        return queryResultDTO;
    }

    @Override
    public Business findById(Integer id) {
        Business business = businessDao.findById(id);
        if (business == null) {
            throw new RuntimeException("商机" + id + "不存在");
        }

        return business;
    }

    @Override
    public BusinessDTO findDetailsById(Integer id) {
        BusinessDTO detailsById = businessDao.findDetailsById(id);
        return detailsById;
    }

    @Override
    public boolean update(Business obj) {
        Business old = businessDao.findById(obj.getBusinessId());
        old.setBusinessName(obj.getBusinessName());
        old.setCustId(obj.getCustId());
        old.setBusinessMoney(obj.getBusinessMoney());
        old.setBusinessRemark(obj.getBusinessRemark());
        old.setNextFollowupTime(obj.getNextFollowupTime());
        old.setUpdateTime(new Date());
        Integer update = businessDao.update(obj);
        EmpLog log = EmpLogBuilder.buildLog("修改", EmpLogObjectType.BUSINESS, old.getBusinessName(),
                old.getBusinessId());
        Integer sevEmpLog = empLogDao.sevEmpLog(log);
        if (update > 0 & sevEmpLog > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean batchDelete(Integer[] id) {
        if (id == null || id.length == 0) {
            return false;
        }
        Integer delete = businessDao.batchDelete(id);
        if (delete != id.length) {
            return false;
        }
        return true;
    }

    @Override
    public boolean saveFollowup(BusinessFollowup obj) {
        obj.setCreateEmpId(WhoAmI.id());
        obj.setCreateTime(new Date());
        Integer followup = businessFollowupDao.saveFollowup(obj);
        Business business = this.businessDao.findById(obj.getBusinessId());
        business.setFollowupStatus(true);
        business.setLastFollowupPresentation(obj.getBusinessFollowupPresentation());
        business.setLastFollowupTime(obj.getFollowupTime());
        business.setNextFollowupTime(obj.getNextFollowupTime());
        business.setUpdateTime(new Date());
        Integer update = businessDao.update(business);
        if (followup <= 0 & update <= 0) {
            return false;
        }
        return true;
    }

    @Override
    public List<BusinessFollowup> findFollowup(Integer id) {
        List<BusinessFollowup> followup = businessFollowupDao.findFollowup(id);
        return followup;
    }


    @Override
    public List<Business> findByCustId(Integer custid) {
        List<Business> businesslist = businessDao.findByCustId(custid);
        return businesslist;
    }

    @Override
    public boolean setOwner(Integer entityId, Integer empId) {
        Business business = businessDao.findById(entityId);
        business.setOwnerEmpId(empId);
        business.setUpdateTime(new Date());
        Integer update = businessDao.update(business);

        EmpLog log = EmpLogBuilder.buildLog("修改", EmpLogObjectType.BUSINESS, business.getBusinessName(),
                business.getBusinessId());
        Integer sevEmpLog = this.empLogDao.sevEmpLog(log);
        if (update > 0 & sevEmpLog > 0) {
            return true;
        }
        return false;
    }
}

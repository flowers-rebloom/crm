package com.scgrts.crm.service.impl;

import com.scgrts.crm.dao.*;
import com.scgrts.crm.dao.impl.*;
import com.scgrts.crm.model.dto.ContractDTO;
import com.scgrts.crm.model.dto.ForSelect;
import com.scgrts.crm.model.dto.common.QueryParamDTO;
import com.scgrts.crm.model.dto.common.QueryResultDTO;
import com.scgrts.crm.model.entity.Business;
import com.scgrts.crm.model.entity.Contact;
import com.scgrts.crm.model.entity.Contract;
import com.scgrts.crm.model.entity.EmpLog;
import com.scgrts.crm.model.enums.EmpLogObjectType;
import com.scgrts.crm.service.ContractService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 合同业务逻辑实现类
 */
public class ContractServiceImpl implements ContractService {
    private ContractDao contractDao = new ContractDaoImpl();
    private EmpLogDao empLogDao = new EmpLogDaoImpl();
    private ContactDao contactDao = new ContactDaoImpl();
    private CustDao custDao = new CustDaoImpl();
    private BusinessDao businessDao = new BusinessDaoImpl();

    @Override
    public boolean save(Contract obj) {
        obj.setCreateEmpId(WhoAmI.id());
        obj.setOwnerEmpId(obj.getCreateEmpId());
        obj.setCreateTime(new Date());
        obj.setDeleteStatus(false);
        Integer save = contractDao.save(obj);
        EmpLog log = EmpLogBuilder.buildLog("新建", EmpLogObjectType.CONTRACT, obj.getContractName(),
                obj.getContractId());
        Integer sevEmpLog = empLogDao.sevEmpLog(log);
        if (save > 0 & sevEmpLog > 0) {
            return true;
        }
        return false;
    }

    @Override
    public QueryResultDTO<Contract> findPage(QueryParamDTO queryParamDTO) {
        Integer total = contractDao.findTotal(queryParamDTO.getQ());
        List<Contract> list = contractDao.findPage(queryParamDTO);
        QueryResultDTO<Contract> queryResultDTO = new QueryResultDTO<Contract>();
        queryResultDTO.setTotal(total);
        queryResultDTO.setList(list);
        return queryResultDTO;
    }

    @Override
    public Contract findById(Integer id) {
        Contract contract = contractDao.findById(id);
        if (contract == null) {
            throw new RuntimeException("合同" + id + "不存在");
        }
        return contract;
    }

    @Override
    public ContractDTO findDetailsById(Integer id) {
        ContractDTO contractDTO = contractDao.findDetailsById(id);
        return contractDTO;
    }

    @Override
    public boolean update(Contract obj) {
        Contract old = this.contractDao.findById(obj.getContractId());
        old.setContractCode(obj.getContractCode());
        old.setContractName(obj.getContractName());
        old.setCustId(obj.getCustId());
        //判断商机id
        if (obj.getBusinessId() != 0 && obj.getBusinessId() != null) {
            old.setBusinessId(obj.getBusinessId());
        }
        old.setContractMoney(obj.getContractMoney());
        old.setOrderTime(obj.getOrderTime());
        old.setCompanyEmpId(obj.getCompanyEmpId());
        old.setContractRemark(obj.getContractRemark());
        old.setUpdateTime(new Date());
        Integer update = contractDao.update(old);

        EmpLog log = EmpLogBuilder.buildLog("修改", EmpLogObjectType.CONTRACT, old.getContractName(), old.getContactId());
        Integer sevEmpLog = empLogDao.sevEmpLog(log);
        if (update > 0 & sevEmpLog > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean batchDelete(Integer[] id) {
        if (id == null || id.length == 0) {
            return false;
        }
        Integer delete = contractDao.batchDelete(id);
        if (delete != id.length) {
            return false;
        }
        return true;
    }

    @Override
    public List<Contact> findContacts(Integer contractId) {
        List<Contact> list = new ArrayList<Contact>();
        Contract contract = this.contractDao.findById(contractId);
        int id = contract.getContactId();
        list.add(contactDao.findById(id));
        return list;
    }

    @Override
    public boolean setPaymentStatus(int contractId, boolean status) {
        Contract contract = contractDao.findById(contractId);
        contract.setPaymentStatus(status);
        contract.setUpdateTime(new Date());
        Integer update = contractDao.update(contract);

        EmpLog log = EmpLogBuilder.buildLog("修改", EmpLogObjectType.CONTRACT, contract.getContractName(), contract.getContractId());
        Integer sevEmpLog = empLogDao.sevEmpLog(log);
        if (update > 0 & sevEmpLog > 0) {
            return true;
        }
        return false;
    }

    @Override
    public List<ForSelect> businessForSelect(String q) {
        List<ForSelect> list = businessDao.businessForSelect(q);
        return list;
    }

    @Override
    public List<ForSelect> contactForSelect(String q) {
        List<ForSelect> list = contactDao.contactForSelect(q);
        return list;
    }
}

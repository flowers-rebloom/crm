package com.scgrts.crm.service.impl;

import com.scgrts.crm.dao.EmpDao;
import com.scgrts.crm.dao.impl.EmpDaoImpl;
import com.scgrts.crm.model.entity.Emp;
import com.scgrts.crm.model.entity.EmpLog;
import com.scgrts.crm.model.enums.EmpLogObjectType;

public class EmpLogBuilder {

    private static EmpDao empDAO = new EmpDaoImpl();

    /**
     * 生成一个EmpLog对象，表示操作日志
     *
     * @param operation  操作类型，如：新建、修改、删除 ...
     * @param objectType 对象类型
     * @param objectName
     * @param objectId
     * @return
     */
    public static EmpLog buildLog(String operation, EmpLogObjectType objectType, String objectName, int objectId) {
        EmpLog log = new EmpLog();
        final int empId = WhoAmI.id();
        log.setCreateEmpId(empId);
        Emp emp = empDAO.findById(empId);
        String empLogMessage = String.format("%s(%d)%s了%s%s(%d)", emp.getEmpName(), empId, operation,
                objectType.getDescription(), objectName, objectId);
        log.setEmpLogMessage(empLogMessage);
        log.setObjectId(objectId);
        log.setObjectType(objectType.getValue());
        return log;
    }
}

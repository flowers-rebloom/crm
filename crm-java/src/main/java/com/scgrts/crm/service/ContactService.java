package com.scgrts.crm.service;

import com.scgrts.crm.model.dto.ContactDTO;
import com.scgrts.crm.model.entity.Contact;
import com.scgrts.crm.model.entity.ContactFollowup;

/**
 * 联系人管理模块业务接口
 */
public interface ContactService extends SuperService<Contact, ContactDTO> {
}

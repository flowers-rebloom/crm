package com.scgrts.crm.service;


import com.scgrts.crm.model.dto.ClueDTO;
import com.scgrts.crm.model.entity.Clue;
import com.scgrts.crm.model.entity.ClueFollowup;

/**
 * 线索模块的业务接口
 */
public interface ClueService extends SuperService<Clue, ClueDTO>, SuperFollowupService<ClueFollowup> {

    /**
     * 将指定线索转为客户
     *
     * @param id
     * @return
     */
    boolean changeInotoCust(Integer id);

    /**
     * @param id
     * @return
     */
    boolean delete(Integer id);
}

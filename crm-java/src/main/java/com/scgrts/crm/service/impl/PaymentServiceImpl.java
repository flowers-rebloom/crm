package com.scgrts.crm.service.impl;

import com.scgrts.crm.dao.EmpLogDao;
import com.scgrts.crm.dao.PaymentDao;
import com.scgrts.crm.dao.impl.EmpLogDaoImpl;
import com.scgrts.crm.dao.impl.PaymentDaoImpl;
import com.scgrts.crm.model.dto.PaymentDTO;
import com.scgrts.crm.model.dto.common.QueryParamDTO;
import com.scgrts.crm.model.dto.common.QueryResultDTO;
import com.scgrts.crm.model.entity.EmpLog;
import com.scgrts.crm.model.entity.Payment;
import com.scgrts.crm.model.enums.EmpLogObjectType;
import com.scgrts.crm.service.PaymentService;

import java.util.Date;
import java.util.List;

/**
 * 回款业务逻辑实现类
 */
public class PaymentServiceImpl implements PaymentService {

    private PaymentDao paymentDao = new PaymentDaoImpl();
    private EmpLogDao empLogDao = new EmpLogDaoImpl();

    @Override
    public boolean save(Payment obj) {
        obj.setCreateEmpId(WhoAmI.id());
        obj.setOwnerEmpId(obj.getCreateEmpId());
        obj.setCreateTime(new Date());
        obj.setDeleteStatus(false);
        Integer save = paymentDao.save(obj);

        EmpLog log = EmpLogBuilder.buildLog("新建", EmpLogObjectType.PAYMENT, "PAYMENT" + obj.getPaymentId(),
                obj.getPaymentId());
        Integer sevEmpLog = empLogDao.sevEmpLog(log);
        if (save == 0 & sevEmpLog == 0) {
            return false;
        }
        return true;
    }

    @Override
    public QueryResultDTO<Payment> findPage(QueryParamDTO queryParamDTO) {
        Integer total = paymentDao.findTotal(queryParamDTO.getQ());
        List<Payment> list = paymentDao.findPage(queryParamDTO);
        QueryResultDTO<Payment> queryResultDTO = new QueryResultDTO<Payment>();
        queryResultDTO.setTotal(total);
        queryResultDTO.setList(list);
        return queryResultDTO;
    }

    @Override
    public Payment findById(Integer id) {
        Payment payment = paymentDao.findById(id);
        if (payment == null) {
            throw new RuntimeException("回款" + id + "不存在");
        }
        return payment;
    }

    @Override
    public PaymentDTO findDetailsById(Integer id) {
        PaymentDTO paymentDTO = paymentDao.findDetailsById(id);
        return paymentDTO;
    }

    @Override
    public boolean update(Payment obj) {
        Payment old = this.paymentDao.findById(obj.getPaymentId());
        old.setCustId(obj.getCustId());
        old.setContractId(obj.getContractId());
        old.setPaymentTime(obj.getPaymentTime());
        old.setPaymentType(obj.getPaymentType());
        old.setPaymentMoney(obj.getPaymentMoney());
        old.setPaymentRemark(obj.getPaymentRemark());
        old.setOwnerEmpId(obj.getOwnerEmpId());
        old.setUpdateTime(new Date());
        Integer update = paymentDao.update(old);
        EmpLog log = EmpLogBuilder.buildLog("修改", EmpLogObjectType.PAYMENT, "PAYMENT" + obj.getPaymentId(),
                obj.getPaymentId());
        Integer sevEmpLog = empLogDao.sevEmpLog(log);
        if (update > 0 & sevEmpLog > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean batchDelete(Integer[] id) {
        if (id == null || id.length == 0) {
            return false;
        }
        Integer delete = paymentDao.batchDelete(id);
        if (delete != id.length) {
            return false;
        }
        return true;
    }

    @Override
    public boolean setOwner(Integer entityId, Integer empId) {
        Payment payment = this.paymentDao.findById(entityId);
        payment.setOwnerEmpId(empId);
        payment.setUpdateTime(new Date());
        Integer update = paymentDao.update(payment);

        EmpLog log = EmpLogBuilder.buildLog("修改", EmpLogObjectType.PAYMENT, "PAYMENT" + payment.getPaymentId(),
                payment.getPaymentId());
        Integer sevEmpLog = empLogDao.sevEmpLog(log);
        if (update > 0 & sevEmpLog > 0) {
            return true;
        }
        return false;
    }
}

package com.scgrts.crm.service.impl;

public class WhoAmI {

    private static ThreadLocal<Integer> TL = new ThreadLocal<Integer>();

    public static void set(Integer id) {
        TL.set(id);
    }

    /**
     * 获得当前用户的id，这个id可以作为EmpLog的createEmpId
     *
     * @return
     */
    public static int id() {
        return TL.get();
    }
}

package com.scgrts.crm.service.impl;

import com.scgrts.crm.dao.*;
import com.scgrts.crm.dao.impl.*;
import com.scgrts.crm.model.dto.AreaDTO;
import com.scgrts.crm.model.dto.CustDTO;
import com.scgrts.crm.model.dto.CustFollowupDTO;
import com.scgrts.crm.model.dto.common.QueryParamDTO;
import com.scgrts.crm.model.dto.common.QueryResultDTO;
import com.scgrts.crm.model.entity.*;
import com.scgrts.crm.model.enums.EmpLogObjectType;
import com.scgrts.crm.service.CustService;

import java.util.Date;
import java.util.List;

/**
 * 客户模块业务实现类
 */
public class CustServiceImpl implements CustService {

    private CustDao custDao = new CustDaoImpl();
    private EmpLogDao empLogDao = new EmpLogDaoImpl();
    private CustFollowupDao custFollowupDao = new CustFollowupDaoImpl();
    private ContactDao contactDao = new ContactDaoImpl();
    private AreaDao areaDao = new AreaDaoImpl();

    @Override
    public boolean save(Cust obj) {
        // 先对数据做一些预处理
        obj.setCreateEmpId(WhoAmI.id());
        obj.setOwnerEmpId(obj.getCreateEmpId());// 设置数据的创建人默认为负责人
        obj.setCreateTime(new Date());
        obj.setDeleteStatus(false);
        obj.setLockStatus(false);
        obj.setDealStatus(false);
        // 操作一：向客户表中插入一行数据
        System.out.println("<<<<<" + obj);
        Integer save = custDao.save(obj);
        // 操作二 ：向日指标中插入一行数据
        EmpLog log = EmpLogBuilder.buildLog("新建", EmpLogObjectType.CUST, obj.getCustName(), obj.getCustId());
        Integer integer = empLogDao.sevEmpLog(log);
        if (save > 0 & integer > 0) {
            return true;
        }
        return false;
    }

    @Override
    public QueryResultDTO<Cust> findPage(QueryParamDTO queryParamDTO) {
        QueryResultDTO<Cust> queryResultDTO = new QueryResultDTO<Cust>();
        if (queryParamDTO.getQ() == null) {
            List<Cust> list = custDao.findAll(queryParamDTO);
            queryResultDTO.setTotal(list.size());
            queryResultDTO.setList(list);
            return queryResultDTO;
        }
        Integer total = custDao.findTotal(queryParamDTO.getQ());
        // 查当前页数据
        List<Cust> list = custDao.findPage(queryParamDTO);
        // 把2个结果封装成一个QueryResultDTO实例并返回

        queryResultDTO.setTotal(total);
        queryResultDTO.setList(list);
        return queryResultDTO;
    }

    @Override
    public Cust findById(Integer id) {
        Cust cust = custDao.findById(id);
        if (cust == null) {
            throw new RuntimeException("客户" + id + "不存在");
        }
        return cust;
    }

    @Override
    public CustDTO findDetailsById(Integer id) {
        CustDTO custDTO = custDao.findDetailsById(id);
        return custDTO;
    }

    @Override
    public boolean update(Cust obj) {
        Integer update = custDao.update(obj);
        EmpLog log = EmpLogBuilder.buildLog("修改", EmpLogObjectType.CUST, obj.getCustName(), obj.getCustId());
        Integer integer = empLogDao.sevEmpLog(log);
        if (update > 0 & integer > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean batchDelete(Integer[] id) {
        if (id == null || id.length == 0) {
            return false;
        }

        Integer delete = custDao.batchDelete(id);
        if (delete != id.length) {
            return false;
        }
        return true;
    }

    @Override
    public boolean saveFollowup(CustFollowup obj) {
        // 设置跟进记录创建人id是正在登陆的员工id
        obj.setCreateEmpId(WhoAmI.id());

        // 调用CustfollowupImpl的save方法
        obj.setUpdateTime(new Date());
        Integer integer = custFollowupDao.saveFollowup(obj);
        // 调用custDAO的方法更新cust的last_followup_time, last_followup_presentation,
        Cust cust = this.custDao.findById(obj.getCustId());
        cust.setFollowupStatus(true);
        cust.setLastFollowupPresentation(obj.getCustFollowupPresentation());
        cust.setNextFollowupTime(obj.getNextFollowupTime());
        cust.setLastFollowupTime(obj.getFollowupTime());
        Integer update = custDao.update(cust);
        if (update > 0 & integer > 0) {
            return true;
        }
        return false;
    }

    @Override
    public List<CustFollowup> findFollowup(Integer id) {
        List<CustFollowup> followup = custFollowupDao.findFollowup(id);
        return followup;
    }

    @Override
    public boolean setDealStatus(Integer id, boolean status) {
        Cust cust = custDao.findById(id);
        cust.setDealStatus(status);
        Integer update = custDao.update(cust);
        EmpLog log = EmpLogBuilder.buildLog("更新", EmpLogObjectType.CUST, cust.getCustName(), cust.getCustId());
        Integer integer = empLogDao.sevEmpLog(log);
        if (update > 0 & integer > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setLockStatus(Integer id, boolean status) {
        Cust cust = custDao.findById(id);
        cust.setDealStatus(status);
        Integer update = custDao.update(cust);
        EmpLog log = EmpLogBuilder.buildLog("更新", EmpLogObjectType.CUST, cust.getCustName(), cust.getCustId());
        Integer integer = empLogDao.sevEmpLog(log);
        if (update > 0 & integer > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean putIntoPublicSea(Integer id) {
        Cust cust = custDao.findById(id);
        cust.setOwnerEmpId(null);
        Integer update = custDao.update(cust);
        EmpLog log = EmpLogBuilder.buildLog("更新", EmpLogObjectType.CUST, cust.getCustName(), cust.getCustId());
        Integer integer = empLogDao.sevEmpLog(log);
        if (update > 0 & integer > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean delete(Integer id) {
        Integer delete = custDao.delete(id);
        if (delete > 0) {
            return true;
        }
        return false;
    }

    @Override
    public AreaDTO findCustArea() {
        AreaDTO areaDTO = areaDao.findCustArea();
        JdbcTemplate.closeConnection();
        return areaDTO;
    }

    @Override
    public boolean setOwner(Integer entityId, Integer empId) {
        Cust cust = custDao.findById(entityId);
        cust.setOwnerEmpId(empId);
        Integer update = custDao.update(cust);
        EmpLog log = EmpLogBuilder.buildLog("更新", EmpLogObjectType.CUST, cust.getCustName(), cust.getCustId());
        Integer integer = empLogDao.sevEmpLog(log);
        if (update > 0 & integer > 0) {
            return true;
        }
        return false;
    }

    @Override
    public List<Contact> findContacts(Integer entityId) {
        List<Contact> list = this.contactDao.findContacts(entityId);
        return list;
    }
}

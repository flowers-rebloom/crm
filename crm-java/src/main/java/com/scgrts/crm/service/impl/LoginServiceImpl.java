package com.scgrts.crm.service.impl;

import com.scgrts.crm.dao.EmpDao;
import com.scgrts.crm.dao.LoginDao;
import com.scgrts.crm.dao.impl.EmpDaoImpl;
import com.scgrts.crm.dao.impl.JdbcTemplate;
import com.scgrts.crm.dao.impl.LoginDaoImpl;
import com.scgrts.crm.model.dto.LoginDTO;
import com.scgrts.crm.model.entity.Emp;
import com.scgrts.crm.service.LoginService;

import java.util.Date;

/**
 * 登录接口实现类
 */
public class LoginServiceImpl implements LoginService {

    private LoginDao loginDao = new LoginDaoImpl();//操作员工表
    private EmpDao empDao = new EmpDaoImpl();//员工表

    @Override
    public boolean login(LoginDTO loginDTO) {
        Emp emp = loginDao.findEmpByName(loginDTO.getUsername());
        if (emp == null) {
            return false;
        }
        // md5校验密码加盐
        String inputPwd = MD5Utils.encrypt(loginDTO.getPassword() + emp.getEmpSalt());
        if (!inputPwd.equals(emp.getEmpPwd())) {
            return false;
        }
        emp.setLastLoginTime(new Date());
        emp.setLastLoginIp(loginDTO.getLoginIp());
        Integer update = empDao.update(emp);
        if (update == 0) {
            return false;
        }
        JdbcTemplate.closeConnection();
        return true;
    }

    @Override
    public Emp findEmpByName(String username) {
        Emp emp = this.empDao.findByCode(username);
        JdbcTemplate.closeConnection();
        return emp;
    }
}

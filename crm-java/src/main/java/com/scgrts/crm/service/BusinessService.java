package com.scgrts.crm.service;

import com.scgrts.crm.model.dto.BusinessDTO;
import com.scgrts.crm.model.entity.Business;
import com.scgrts.crm.model.entity.BusinessFollowup;

import java.util.List;

/**
 * 商机模块业务接口
 */
public interface BusinessService extends SuperService<Business, BusinessDTO>, SuperFollowupService<BusinessFollowup> {
    /**
     * 根据客户id查询商机
     *
     * @param custid
     * @return
     */
    List<Business> findByCustId(Integer custid);

    /**
     * 变更指定实体的负责人
     *
     * @param entityId 实体id，它可能是custId/bisinessId/contractId
     * @param empId    负责人id
     * @return
     */
    boolean setOwner(Integer entityId, Integer empId);

}

package com.scgrts.crm.service;

import com.scgrts.crm.model.dto.MoneyTopItem;

import java.util.Date;
import java.util.List;

/**
 * 排行业务接口
 */
public interface StatisticsService {
    List<MoneyTopItem> moneyStatistics(Date dateBegin, Date dateEnd);
}

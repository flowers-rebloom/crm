package com.scgrts.crm.service;

import com.scgrts.crm.model.dto.KPIDTO;
import com.scgrts.crm.model.dto.RemindDTO;
import com.scgrts.crm.model.dto.ResourceDTO;
import com.scgrts.crm.model.dto.SalesReportDTO;

import java.util.List;

/**
 * 仪表盘业务接口
 */
public interface DashboardService {


    /**
     * 统计指定个人的销售简报
     *
     * @param username 当前用户名
     * @return
     */
    SalesReportDTO countSalesReportPersonal(String username);

    /**
     * 统计指定个人所属部门的销售简报
     *
     * @param username 当前用户名
     * @return
     */
    SalesReportDTO countSalesReportOfDept(String username);

    /**
     * 统计指定个人的业绩指标完成率
     *
     * @param username
     * @return
     */
    KPIDTO countKPIPersonal(String username);

    /**
     * 统计指定个人所属部门的业绩指标完成率
     *
     * @param username
     * @return
     */
    KPIDTO countKPIOfDept(String username);

    /**
     * 统计指定用户今日待联系的资源（线索、客户、商机统称资源）
     *
     * @param username
     * @return
     */
    List<ResourceDTO> countResourcesToBeConnectToday(String username);

    /**
     * 统计指定用户的遗忘资源数
     *
     * @param username
     * @return
     */
    RemindDTO countRemindResources(String username);
}

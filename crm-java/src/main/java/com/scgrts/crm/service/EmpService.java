package com.scgrts.crm.service;

/**
 * 用户业务接口
 */
public interface EmpService {
    /**
     * 更新员工头像
     *
     * @param headPhotoAccessPath 头像访问地址
     * @param empId
     * @param headPhotoAccessPath
     * @return
     */
    boolean updateHead(Integer empId, String headPhotoAccessPath);
}

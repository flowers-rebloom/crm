package com.scgrts.crm.service.impl;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5密文处理工具类
 *
 * @author root
 */
public class MD5Utils {

    /**
     * 把指定的明文字符串通过MD5算法，加密成密文并返回
     *
     * @param input
     * @return
     */
    public static String encrypt(String input) {
        // String -> 字节
        byte[] bytes = input.getBytes();
        try {
            // 对字节进行md5 hash运算，得到一个长为16的字节数组
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            bytes = md5.digest(bytes);
            // 把每个字节转换成2位十六进制数，加在一起，就是32个字符的密文
            StringBuffer sb = new StringBuffer();
            for (byte b : bytes) {
                String hex = String.format("%02x", b);
                sb.append(hex);
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

}

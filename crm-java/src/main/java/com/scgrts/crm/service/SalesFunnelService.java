package com.scgrts.crm.service;


import com.scgrts.crm.model.dto.SalesFunnelDTO;

import java.util.Date;
import java.util.List;

/**
 * 销售漏斗接口
 */
public interface SalesFunnelService {

    /**
     * 销售漏斗
     *
     * @param dateBegin 开始时间
     * @param dateEnd   结束时间
     * @return
     */

    List<SalesFunnelDTO> moneySalesFunnel(Date dateBegin, Date dateEnd);

}

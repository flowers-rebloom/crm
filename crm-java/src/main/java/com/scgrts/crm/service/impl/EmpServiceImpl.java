package com.scgrts.crm.service.impl;

import com.scgrts.crm.dao.EmpDao;
import com.scgrts.crm.dao.impl.EmpDaoImpl;
import com.scgrts.crm.model.entity.Emp;
import com.scgrts.crm.service.EmpService;

/**
 * 更换头像实现类
 */
public class EmpServiceImpl implements EmpService {
    private EmpDao empDao = new EmpDaoImpl();

    @Override
    public boolean updateHead(Integer empId, String headPhotoAccessPath) {
        Emp emp = this.empDao.findById(empId);
        emp.setHeadPhoto(headPhotoAccessPath);
        Integer updateHead = empDao.update(emp);
        if (updateHead > 0) {
            return true;
        }
        return false;
    }
}

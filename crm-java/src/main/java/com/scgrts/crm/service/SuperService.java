package com.scgrts.crm.service;


import com.scgrts.crm.model.dto.common.QueryParamDTO;
import com.scgrts.crm.model.dto.common.QueryResultDTO;

/**
 * 父类业务逻辑接口
 *
 * @param <E> 随意对象
 */
public interface SuperService<E, EDTO> {

    /**
     * 保存新的实体
     *
     * @param obj 封装新建表单中的数据
     * @return true->成功 false->失败
     */
    boolean save(E obj);

    /**
     * 根据搜索关键字、每页条数、页码查找数据
     *
     * @param queryParamDTO 封装搜索关键字、每页条数、页码查找线索的一个对象
     * @return 总行数和总条数
     */
    QueryResultDTO<E> findPage(QueryParamDTO queryParamDTO);

    /**
     * 根据id查询数据
     *
     * @param id
     * @return
     */
    E findById(Integer id);

    /**
     * 根据id查询实体详情
     *
     * @param id
     * @return
     */
    EDTO findDetailsById(Integer id);

    /**
     * 更新数据
     *
     * @param obj 需要更新数据的封装类
     * @return true->成功 false->失败
     */
    boolean update(E obj);

    /**
     * 批量删除数据
     *
     * @param id 一组id
     * @return true->成功 false->失败
     */
    boolean batchDelete(Integer[] id);


}

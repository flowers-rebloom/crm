package com.scgrts.crm.service;

import com.scgrts.crm.model.entity.Contact;

import java.util.List;

/**
 * 联系人列表功能和更变负责人的抽象
 */
public interface SuperNameService {
    /**
     * 变更指定实体的负责人
     *
     * @param entityId 实体id，它可能是custId/bisinessId/contractId
     * @param empId    负责人id
     * @return
     */
    boolean setOwner(Integer entityId, Integer empId);

    /**
     * 查找指定客户的联系人列表
     *
     * @param entityId 实体id，它可能是custId/bisinessId/contractId
     * @return
     */
    List<Contact> findContacts(Integer entityId);
}

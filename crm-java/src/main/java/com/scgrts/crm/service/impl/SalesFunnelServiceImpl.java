package com.scgrts.crm.service.impl;

import com.scgrts.crm.dao.SalesFunnelDao;
import com.scgrts.crm.dao.impl.SalesFunnelDaoImpl;
import com.scgrts.crm.model.dto.SalesFunnel;
import com.scgrts.crm.model.dto.SalesFunnelDTO;
import com.scgrts.crm.service.SalesFunnelService;

import java.util.*;

/**
 * 销售漏斗实现类
 */
public class SalesFunnelServiceImpl implements SalesFunnelService {
    private SalesFunnelDao dao = new SalesFunnelDaoImpl();

    @Override
    public List<SalesFunnelDTO> moneySalesFunnel(Date dateBegin, Date dateEnd) {
        List<SalesFunnel> list = dao.moneySalesFunnel(dateBegin, dateEnd);
        int a = 0, b = 0, c = 0, d = 0;
        for (SalesFunnel s : list) {
            if (s.getCurrent_step() != null) {
                a++;
            }
            if (s.getCurrent_step() != 0 && s.getCurrent_step() != null) {
                b++;
            }
            if (s.getCurrent_step() == 2 || s.getCurrent_step() == 3) {
                c++;
            }
            if (s.getCurrent_step() == 3) {
                if (s.getEnd_status() == 1) {
                    d++;
                }
            }
        }
        List<SalesFunnelDTO> map = new ArrayList<>();
        map.add(new SalesFunnelDTO("验证客户", a * 100 / list.size()));
        map.add(new SalesFunnelDTO("需求分析", b * 100 / list.size()));
        map.add(new SalesFunnelDTO("方案报价", c * 100 / list.size()));
        map.add(new SalesFunnelDTO("赢单", d * 100 / list.size()));
        return map;
    }
}

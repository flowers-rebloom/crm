package com.scgrts.crm.service.impl;

import com.scgrts.crm.model.dto.KPIDTO;
import com.scgrts.crm.model.dto.RemindDTO;
import com.scgrts.crm.model.dto.ResourceDTO;
import com.scgrts.crm.model.dto.SalesReportDTO;
import com.scgrts.crm.service.DashboardService;

import java.util.List;

/**
 * 仪表盘业务实现类
 */
public class DashboardServiceImpl implements DashboardService {
    @Override
    public SalesReportDTO countSalesReportPersonal(String username) {

        return null;
    }

    @Override
    public SalesReportDTO countSalesReportOfDept(String username) {
        return null;
    }

    @Override
    public KPIDTO countKPIPersonal(String username) {
        return null;
    }

    @Override
    public KPIDTO countKPIOfDept(String username) {
        return null;
    }

    @Override
    public List<ResourceDTO> countResourcesToBeConnectToday(String username) {
        return null;
    }

    @Override
    public RemindDTO countRemindResources(String username) {
        return null;
    }
}

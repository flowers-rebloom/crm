package com.scgrts.crm.service;

import com.scgrts.crm.model.dto.LoginDTO;
import com.scgrts.crm.model.entity.Emp;

/**
 * 用户登录接口
 */
public interface LoginService {
    /**
     * 用户身份验证
     *
     * @param loginDTO 前端传递过来的用户名和密码
     * @returnc true->登录成功 false->登录失败
     */
    boolean login(LoginDTO loginDTO);

    /**
     * 根据用户名查找用户
     *
     * @param username 用户名
     * @return
     */
    Emp findEmpByName(String username);

}

package com.scgrts.crm.service.impl;

import com.scgrts.crm.dao.ContactDao;
import com.scgrts.crm.dao.ContactFollowupDao;
import com.scgrts.crm.dao.EmpLogDao;
import com.scgrts.crm.dao.impl.ContactDaoImpl;
import com.scgrts.crm.dao.impl.ContactFollowupDaoImpl;
import com.scgrts.crm.dao.impl.EmpLogDaoImpl;
import com.scgrts.crm.model.dto.ContactDTO;
import com.scgrts.crm.model.dto.common.QueryParamDTO;
import com.scgrts.crm.model.dto.common.QueryResultDTO;
import com.scgrts.crm.model.entity.Contact;
import com.scgrts.crm.model.entity.ContactFollowup;
import com.scgrts.crm.model.entity.Cust;
import com.scgrts.crm.model.entity.EmpLog;
import com.scgrts.crm.model.enums.EmpLogObjectType;
import com.scgrts.crm.service.ContactService;

import java.util.Date;
import java.util.List;

/**
 * 联系人管理模块业务实现类
 */
public class ContactServiceImpl implements ContactService {


    private ContactDao contactDao = new ContactDaoImpl();
    private ContactFollowupDao contactFollowupDao = new ContactFollowupDaoImpl();
    private EmpLogDao empLogDao = new EmpLogDaoImpl();

    @Override
    public boolean save(Contact obj) {
        obj.setCreateEmpId(WhoAmI.id());
        obj.setOwnerEmpId(obj.getCreateEmpId());
        obj.setCreateTime(new Date());
        Integer save = contactDao.save(obj);
        EmpLog log = EmpLogBuilder.buildLog("新增", EmpLogObjectType.CONTACT, obj.getContactName(), obj.getContactId());
        Integer integer = empLogDao.sevEmpLog(log);
        if (save > 0 & integer > 0) {
            return true;
        }
        return false;
    }

    @Override
    public QueryResultDTO<Contact> findPage(QueryParamDTO queryParamDTO) {
        Integer total = contactDao.findTotal(queryParamDTO.getQ());
        List<Contact> list = contactDao.findPage(queryParamDTO);
        // 把2个结果封装成一个QueryResultDTO实例并返回
        QueryResultDTO<Contact> queryResultDTO = new QueryResultDTO<Contact>();
        queryResultDTO.setTotal(total);
        queryResultDTO.setList(list);
        return queryResultDTO;
    }

    @Override
    public Contact findById(Integer id) {
        Contact byId = contactDao.findById(id);
        if (byId == null) {
            throw new RuntimeException("联系人" + id + "不存在");
        }
        return byId;
    }

    @Override
    public ContactDTO findDetailsById(Integer id) {
        ContactDTO detailsById = contactDao.findDetailsById(id);
        return detailsById;
    }

    @Override
    public boolean update(Contact obj) {
        Contact old = contactDao.findById(obj.getContactId());
        old.setCustId(old.getCustId());
        old.setContactName(obj.getContactName());
        old.setContactPost(obj.getContactPost());
        old.setContactPhone(obj.getContactPhone());
        old.setContactEmail(obj.getContactEmail());
        old.setContactAddress(obj.getContactAddress());
        old.setContactGender(obj.getContactGender());
        old.setContactRemark(obj.getContactRemark());
        old.setIsDecisionMaker(obj.getIsDecisionMaker());
        old.setUpdateTime(new Date());
        Integer update = contactDao.update(old);
        EmpLog log = EmpLogBuilder.buildLog("修改", EmpLogObjectType.CONTACT, obj.getContactName(), obj.getContactId());
        Integer integer = empLogDao.sevEmpLog(log);
        if (update > 0 & integer > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean batchDelete(Integer[] id) {
        if (id == null || id.length == 0) {
            return false;
        }
        Integer delete = contactDao.batchDelete(id);
        if (delete != id.length) {
            return false;
        }
        return true;
    }

}

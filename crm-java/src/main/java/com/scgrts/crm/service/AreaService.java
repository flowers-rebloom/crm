package com.scgrts.crm.service;

import com.scgrts.crm.model.entity.Area;

import java.util.List;

/**
 * 客户分布接口
 */
public interface AreaService {

    /**
     * 根据父id查询地区
     *
     * @param parentId
     * @return
     */
    List<Area> findAreaByParentId(Integer parentId);
}

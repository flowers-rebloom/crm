package com.scgrts.crm.service.impl;

import com.scgrts.crm.dao.StatisticsDao;
import com.scgrts.crm.dao.impl.StatisticsDaoImpl;
import com.scgrts.crm.model.dto.MoneyTopItem;
import com.scgrts.crm.service.StatisticsService;

import java.util.Date;
import java.util.List;

/**
 * 排行实现类
 */
public class StatisticsServiceImpl implements StatisticsService {
    private StatisticsDao statisticsDao = new StatisticsDaoImpl();

    @Override
    public List<MoneyTopItem> moneyStatistics(Date dateBegin, Date dateEnd) {
        List<MoneyTopItem> list = statisticsDao.moneyStatistics(dateBegin, dateEnd);
        return list;
    }
}

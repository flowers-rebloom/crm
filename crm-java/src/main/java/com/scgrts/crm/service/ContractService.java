package com.scgrts.crm.service;

import com.scgrts.crm.model.dto.ContractDTO;
import com.scgrts.crm.model.dto.ForSelect;
import com.scgrts.crm.model.entity.Contact;
import com.scgrts.crm.model.entity.Contract;

import java.util.List;

/**
 * 合同管理模块接口
 */
public interface ContractService extends SuperService<Contract, ContractDTO> {
    /**
     * 查找指定客户的联系人列表
     *
     * @param contractId
     * @return
     */
    List<Contact> findContacts(Integer contractId);

    /**
     * 设置指定合同的回款状态
     *
     * @param contractId 合同id
     * @param status     回款状态值
     * @return
     */
    boolean setPaymentStatus(int contractId, boolean status);

    /**
     * 通过关键字选取商机
     *
     * @param q
     * @return
     */
    List<ForSelect> businessForSelect(String q);

    /**
     * 通过关键字选取客户联系人
     *
     * @param q
     * @return
     */
    List<ForSelect> contactForSelect(String q);


}

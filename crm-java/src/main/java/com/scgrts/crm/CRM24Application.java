package com.scgrts.crm;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.DirResourceSet;
import org.apache.catalina.webresources.StandardRoot;

public class CRM24Application {

    final static int PORT = 8080;

    public static void main(String[] args) throws Exception {
        kill();
        createWebAppDir();
        debug();
    }

    /**
     * 杀掉正在运行的tomcat实例，释放端口
     *
     * @throws IOException
     */
    private static void kill() throws IOException {
        Process process = Runtime.getRuntime().exec("netstat -aon");
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream(), "gbk"));
        String line = null;
        while ((line = reader.readLine()) != null) {
            if (line.contains(":" + PORT + " ") && line.contains("LISTENING")) {
                String[] split = line.split("\\s+");
                String pid = split[split.length - 1];
                Runtime.getRuntime().exec("TASKKILL /F /PID " + pid);
                break;
            }
        }
    }

    private static void createWebAppDir() {
        File file = new File("src/main/webapp");
        if (file.exists()) {
            return;
        }
        file.mkdir();
    }

    /**
     * 启动新的tomcat实例
     *
     * @throws LifecycleException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    private static void debug() throws LifecycleException, InstantiationException, IllegalAccessException {

        String webappDirLocation = "src/main/webapp";
        Tomcat tomcat = new Tomcat();
        tomcat.setHostname("localhost");
        tomcat.setPort(PORT);
        Context ctx = tomcat.addWebapp("/", new File(webappDirLocation).getAbsolutePath());
        ctx.setReloadable(true);

        tomcat.getConnector();
        File additionWebInfClasses = new File("target/classes");
        WebResourceRoot resources = new StandardRoot(ctx);
        resources.addPreResources(
                new DirResourceSet(resources, "/WEB-INF/classes", additionWebInfClasses.getAbsolutePath(), "/"));
        ctx.setResources(resources);

        tomcat.start();
        tomcat.getServer().await();
    }
}

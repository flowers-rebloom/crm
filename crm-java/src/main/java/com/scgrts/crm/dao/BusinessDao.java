package com.scgrts.crm.dao;

import com.scgrts.crm.model.dto.BusinessDTO;
import com.scgrts.crm.model.dto.ForSelect;
import com.scgrts.crm.model.dto.SalesReportDTO;
import com.scgrts.crm.model.entity.Business;

import java.util.List;

/**
 * 商机模块数据库接口
 */
public interface BusinessDao extends SuperDao<Business, BusinessDTO> {
    /**
     * 根据客户id查询商机
     *
     * @param custid
     * @return
     */
    List<Business> findByCustId(Integer custid);

    /**
     * 通过关键字选取商机
     *
     * @param q
     * @return
     */
    List<ForSelect> businessForSelect(String q);

    List<Integer> findtest();

    void upeded(Integer id);

//    /**
//     * 统计指定个人的销售简报
//     * @param username 当前用户名
//     * @return
//     */
//    SalesReportDTO countSalesReportPersonal(String username);
//
//    /**
//     * 统计指定个人所属部门的销售简报
//     * @param username 当前用户名
//     * @return
//     */
//    SalesReportDTO countSalesReportOfDept(String username);

}

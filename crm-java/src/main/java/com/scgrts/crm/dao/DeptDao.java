package com.scgrts.crm.dao;

import com.scgrts.crm.model.entity.Dept;

import java.util.List;

/**
 * 部门数据库接口
 */
public interface DeptDao {
    /**
     * 查询所有部门
     *
     * @return
     */
    List<Dept> findAll();

    /**
     * 查询子部门
     *
     * @param parentId
     * @return
     */
    List<Dept> findByParentId(Integer parentId);

    /**
     * 根据用户名查询部门
     *
     * @param usernmae
     * @return
     */
    Dept findByName(String usernmae);
}

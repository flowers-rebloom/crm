package com.scgrts.crm.dao;


import com.scgrts.crm.model.dto.ClueDTO;
import com.scgrts.crm.model.dto.SalesReportDTO;
import com.scgrts.crm.model.entity.Clue;

/**
 * 线索模块数据库接口
 */
public interface ClueDao extends SuperDao<Clue, ClueDTO> {

    /**
     * 统计指定个人的销售简报
     *
     * @param username 当前用户名
     * @return
     */
    SalesReportDTO countSalesReportPersonal(String username);

    /**
     * 统计指定个人所属部门的销售简报
     *
     * @param username 当前用户名
     * @return
     */
    SalesReportDTO countSalesReportOfDept(String username);
}

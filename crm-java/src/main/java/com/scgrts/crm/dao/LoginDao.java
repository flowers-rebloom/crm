package com.scgrts.crm.dao;

import com.scgrts.crm.model.dto.LoginDTO;
import com.scgrts.crm.model.entity.Emp;

/**
 * 用户登录数据库接口
 */
public interface LoginDao {
    /**
     * 用户身份验证
     *
     * @param username 根据用户名查找用户
     * @returnc 用户名和密码
     */
    LoginDTO login(String username);

    /**
     * 根据用户名查找用户
     *
     * @param username 用户名
     * @return 用户信息
     */
    Emp findEmpByName(String username);
}

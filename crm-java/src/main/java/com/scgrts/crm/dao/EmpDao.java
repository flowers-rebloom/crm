package com.scgrts.crm.dao;

import com.scgrts.crm.model.entity.Emp;

import java.util.List;

/**
 * 员工表
 */
public interface EmpDao {
    /**
     * 修改员工表
     *
     * @param emp
     * @return
     */
    Integer update(Emp emp);

    /**
     * 根据员工代码（相当于登录用户名）查询数据库emp表的记录
     *
     * @param empCode
     * @return 员工实体
     */
    Emp findByCode(String empCode);

    /**
     * 更改头像
     *
     * @param empId
     * @param headPhotoAccessPath
     * @return
     */
    Integer updateHead(Integer empId, String headPhotoAccessPath);

    /**
     * 查询所有用户
     *
     * @return
     */
    List<Emp> findAll();

    Emp findById(Integer id);
}

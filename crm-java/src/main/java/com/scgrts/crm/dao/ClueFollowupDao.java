package com.scgrts.crm.dao;

import com.scgrts.crm.model.entity.ClueFollowup;

import java.util.List;

public interface ClueFollowupDao {
    /**
     * 新增跟进记录
     *
     * @param entity
     * @return
     */
    Integer saveFollowup(ClueFollowup entity);

    /**
     * 根据id查询所有跟进记录
     *
     * @param id
     * @return
     */
    List<ClueFollowup> findFollowup(Integer id);

    /**
     * 根据线索id删除根据记录
     *
     * @param id
     * @return
     */
    Integer deleteByClueId(Integer id);
}

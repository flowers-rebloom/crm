package com.scgrts.crm.dao.impl;

import com.scgrts.crm.dao.EmpLogDao;
import com.scgrts.crm.model.entity.EmpLog;

public class EmpLogDaoImpl implements EmpLogDao {
    @Override
    public Integer sevEmpLog(EmpLog empLog) {
        String sql = "INSERT INTO `emp_log` (`emp_log_message`,`create_emp_id`, `object_type`,`object_id`) " +
                "VALUES (?, ?, ?, ?)";
        // 调用JdbcTemplate执行插入
        int sev = JdbcTemplate.update(sql, empLog.getEmpLogMessage(), empLog.getCreateEmpId(), empLog.getObjectType(),
                empLog.getObjectId());
        return sev;
    }
}

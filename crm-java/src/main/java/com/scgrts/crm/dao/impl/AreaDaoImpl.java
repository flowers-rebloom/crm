package com.scgrts.crm.dao.impl;

import com.scgrts.crm.dao.AreaDao;
import com.scgrts.crm.model.dto.AreaDTO;
import com.scgrts.crm.model.entity.Area;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AreaDaoImpl implements AreaDao {

    @Override
    public AreaDTO findCustArea() {

        String sql = "SELECT p.area_name, IFNULL(t.cust_num, 0) cust_num FROM (SELECT * FROM area WHERE parent_id = 0 "
                + ") p LEFT JOIN(SELECT cust_province, count(0) cust_num FROM cust 	GROUP BY cust_province"
                + ") t ON p.area_id = t.cust_province";
        ResultSet rs = JdbcTemplate.select(sql);

        try {
            AreaDTO dto = new AreaDTO();
            ArrayList<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
            while (rs.next()) {
                String name = rs.getString("area_name");
                int value = rs.getInt("cust_num");
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("name", name);
                map.put("value", value);
                list.add(map);
            }
            dto.setProvinceCustNums(list);
            return dto;
        } catch (SQLException e) {
            throw new RuntimeException();
        }

    }

    @Override
    public List<Area> findByParentId(Integer parentId) {
        String sql = "SELECT * from area where parent_id=?";
        ResultSet rs = JdbcTemplate.select(sql, parentId);
        List<Area> list = new ArrayList<>();
        try {
            while (rs.next()) {
                Area area = new Area();
                area.setAreaId(rs.getInt("area_id"));
                area.setAreaName(rs.getString("area_name"));
                area.setParentId(rs.getInt("parent_id"));
                list.add(area);
            }
            return list;
        } catch (SQLException e) {
            throw new RuntimeException();
        }

    }
}

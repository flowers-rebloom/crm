package com.scgrts.crm.dao;

import com.scgrts.crm.model.entity.BusinessFollowup;

import java.util.List;

public interface BusinessFollowupDao {
    /**
     * 新增数据记录
     *
     * @param businessFollowup
     * @return
     */
    Integer saveFollowup(BusinessFollowup businessFollowup);

    /**
     * 根据商机id查询所有根据记录
     *
     * @param id
     * @return
     */
    List<BusinessFollowup> findFollowup(Integer id);

    /**
     * 根据id删除记录
     *
     * @param id
     * @return
     */
    Integer delete(Integer id);
}

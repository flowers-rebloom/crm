package com.scgrts.crm.dao;

import com.scgrts.crm.model.dto.BusinessDTO;
import com.scgrts.crm.model.dto.SalesFunnel;
import com.scgrts.crm.model.entity.Business;

import java.util.Date;
import java.util.List;

/**
 * 销售漏斗数据库接口
 */
public interface SalesFunnelDao {
    /**
     * 销售漏斗
     *
     * @param dateBegin 开始时间
     * @param dateEnd   结束时间
     * @return
     */
    List<SalesFunnel> moneySalesFunnel(Date dateBegin, Date dateEnd);

}

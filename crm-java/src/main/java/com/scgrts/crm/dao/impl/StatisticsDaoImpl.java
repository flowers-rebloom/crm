package com.scgrts.crm.dao.impl;

import com.scgrts.crm.dao.StatisticsDao;
import com.scgrts.crm.model.dto.MoneyTopItem;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StatisticsDaoImpl implements StatisticsDao {
    @Override
    public List<MoneyTopItem> moneyStatistics(Date dateBegin, Date dateEnd) {
        ArrayList<Date> paramList = new ArrayList<>();
        String sql = "SELECT e.emp_id,e.emp_name,IFNULL( t1.sum_contract_money, 0 ) sum_contract_money,"
                + "	IFNULL( t2.sum_payment_money, 0 ) sum_payment_money FROM emp e"
                + "	LEFT JOIN ( SELECT owner_emp_id, SUM( contract_money ) sum_contract_money FROM contract WHERE 1=1";

        if (dateBegin != null) {
            sql += " AND order_time >= ? ";
            paramList.add(dateBegin);
        }
        if (dateEnd != null) {
            sql += " AND order_time <= ? ";
            paramList.add(dateEnd);
        }

        sql += " GROUP BY owner_emp_id ) t1 ON e.emp_id = t1.owner_emp_id";
        sql += "	LEFT JOIN ( SELECT owner_emp_id, SUM( payment_money ) sum_payment_money FROM payment WHERE 1=1 ";

        if (dateBegin != null) {
            sql += " AND create_time >= ? ";
            paramList.add(dateBegin);
        }
        if (dateEnd != null) {
            sql += " AND create_time <= ? ";
            paramList.add(dateEnd);
        }

        sql += " GROUP BY owner_emp_id ) t2 ON e.emp_id = t2.owner_emp_id ";
        sql += " ORDER BY sum_contract_money DESC, sum_payment_money DESC";
        sql += " LIMIT 10";

        ResultSet rs = JdbcTemplate.select(sql, paramList.toArray());
        ArrayList<MoneyTopItem> list = new ArrayList<>();
        try {
            while (rs.next()) {
                MoneyTopItem item = new MoneyTopItem();
                item.setEmpId(rs.getInt("emp_id"));
                item.setEmpName(rs.getString("emp_name"));
                item.setSumContractMoney(rs.getBigDecimal("sum_contract_money"));
                item.setSumPaymentMoney(rs.getBigDecimal("sum_payment_money"));
                list.add(item);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return list;
    }

}

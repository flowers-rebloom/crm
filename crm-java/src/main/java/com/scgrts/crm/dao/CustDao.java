package com.scgrts.crm.dao;

import com.scgrts.crm.model.dto.CustDTO;
import com.scgrts.crm.model.dto.ForSelect;
import com.scgrts.crm.model.dto.SalesReportDTO;
import com.scgrts.crm.model.entity.Cust;

import java.util.List;

public interface CustDao extends SuperDao<Cust, CustDTO> {
    /**
     * 改变指定客户的成交状态
     *
     * @param id     客户id
     * @param status 成交状态
     * @return
     */
    Integer setDealStatus(Integer id, boolean status);

    /**
     * 把指定的客户放入公海
     *
     * @param id 客户id
     * @return
     */
    Integer putIntoPublicSea(Integer id);

    /**
     * 统计指定个人的销售简报
     *
     * @param username 当前用户名
     * @return
     */
    SalesReportDTO countSalesReportPersonal(String username);

    /**
     * 统计指定个人所属部门的销售简报
     *
     * @param username 当前用户名
     * @return
     */
    SalesReportDTO countSalesReportOfDept(String username);

    /**
     * 通过关键字选取客户
     */
    List<ForSelect> custForSelect(String q);

    List<Cust> findtest();
}

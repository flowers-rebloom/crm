package com.scgrts.crm.dao.impl;

import com.scgrts.crm.dao.ClueFollowupDao;
import com.scgrts.crm.model.entity.ClueFollowup;
import com.scgrts.crm.model.enums.FollowupMethod;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClueFollowupDaoImpl implements ClueFollowupDao {
    @Override
    public Integer saveFollowup(ClueFollowup entity) {
        String sql = "INSERT INTO `clue_followup`(`clue_id`, `followup_time`, `clue_followup_presentation`,`followup_method`, "
                + "`next_followup_time`,`create_emp_id`)VALUES (?, ?, ?, ?, ?, ?)";
        int save = JdbcTemplate.update(sql, entity.getClueId(), entity.getFollowupTime(),
                entity.getClueFollowupPresentation(),
                entity.getFollowupMethod(),
                entity.getNextFollowupTime(),
                entity.getCreateEmpId());
        return save;
    }

    @Override
    public List<ClueFollowup> findFollowup(Integer id) {
        String sql = "SELECT * FROM clue_followup where clue_id=?";
        ResultSet rs = JdbcTemplate.select(sql, id);
        List<ClueFollowup> list = new ArrayList<>();
        try {
            while (rs.next()) {
                ClueFollowup clueFollowup = new ClueFollowup();
                ClueFollowup cluefollowup = new ClueFollowup();
                cluefollowup.setClueFollowupId(rs.getInt("clue_followup_id"));
                cluefollowup.setClueId(rs.getInt("clue_id"));
                cluefollowup.setFollowupTime(rs.getDate("followup_time"));
                cluefollowup.setClueFollowupPresentation(rs.getString("clue_followup_presentation"));
                cluefollowup.setFollowupMethod(FollowupMethod.fromString(rs.getString("followup_method")));
                cluefollowup.setNextFollowupTime(rs.getDate("next_followup_time"));
                cluefollowup.setCreateTime(rs.getDate("create_time"));
                cluefollowup.setUpdateTime(rs.getDate("update_time"));
                cluefollowup.setCreateEmpId(rs.getInt("create_emp_id"));
                list.add(clueFollowup);
            }
            return list;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Integer deleteByClueId(Integer id) {
        String sql = "DELETE FROM clue_followup WHERE clue_id = ?";
        int count = JdbcTemplate.update(sql, id);
        return count;
    }
}

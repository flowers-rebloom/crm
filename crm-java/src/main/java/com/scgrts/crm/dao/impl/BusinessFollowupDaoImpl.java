package com.scgrts.crm.dao.impl;

import com.scgrts.crm.dao.BusinessFollowupDao;
import com.scgrts.crm.model.entity.Business;
import com.scgrts.crm.model.entity.BusinessFollowup;
import com.scgrts.crm.model.enums.FollowupMethod;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class BusinessFollowupDaoImpl implements BusinessFollowupDao {
    @Override
    public Integer saveFollowup(BusinessFollowup businessFollowup) {
        String sql = "INSERT INTO `gcrm`.`business_followup` (`business_id`, `followup_time`, `business_followup_presentation`, "
                + "`contact_id`, `followup_method`, `next_followup_time`, `create_emp_id` "
                + ")VALUES(?, ?, ?, ?, ?, ?, ? )";
        int instert = JdbcTemplate.update(sql, businessFollowup.getBusinessId(), businessFollowup.getFollowupTime(),
                businessFollowup.getBusinessFollowupPresentation(), businessFollowup.getContactId(),
                businessFollowup.getFollowupMethod().getDescription(), businessFollowup.getNextFollowupTime(), businessFollowup.getCreateEmpId());

        return instert;
    }

    @Override
    public List<BusinessFollowup> findFollowup(Integer id) {
        String sql = "SELECT * from business_followup " + "WHERE contact_id = ?";
        ResultSet rs = JdbcTemplate.select(sql, id);
        ArrayList<BusinessFollowup> list = new ArrayList<BusinessFollowup>();
        try {
            while (rs.next()) {
                BusinessFollowup businessFollowup = new BusinessFollowup();
                businessFollowup.setBusinessFollowupId(rs.getInt("business_followup_id"));
                businessFollowup.setBusinessId(rs.getInt("business_id"));
                businessFollowup.setFollowupTime(rs.getDate("followup_time"));
                businessFollowup.setBusinessFollowupPresentation("business_followup_presentation");
                businessFollowup.setContactId(rs.getInt("contact_id"));
                businessFollowup.setFollowupMethod(FollowupMethod.fromString(rs.getString("followup_method")));
                businessFollowup.setNextFollowupTime(rs.getDate("next_followup_time"));
                businessFollowup.setCreateTime(rs.getDate("create_time"));
                businessFollowup.setUpdateTime(rs.getDate("update_time"));
                businessFollowup.setCreateEmpId(rs.getInt("create_emp_id"));
                list.add(businessFollowup);
            }
            return list;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Integer delete(Integer id) {
        String sql = "DELETE FROM business_followup WHERE  business_followup_id = ?";
        int delete = JdbcTemplate.update(sql, id);
        return delete;
    }
}

package com.scgrts.crm.dao.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * JDBC模板类，提供通用的增删改的方法，你只需要调用方法，传入SQL语句及SQL中?对应的参数即可。
 *
 * @author root
 */
public class JdbcTemplate {

    private JdbcTemplate() {
    }

    // 数据库信息
    static final String url = "jdbc:mysql://localhost:3306/gcrm?useSSL=false&serverTimezone=UTC&characterEncoding=UTF-8";
    static final String user = "root";
    static final String password = "1234";

    // 加载MySQL驱动程序。JVM加载JdbcTemplate这个类的时候，就会自动执行一次static{ }代码块，且只执行1次
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * 通用的查询方法，传入SQL语句及SQL语句中的参数，返回结果集
     *
     * @param sql
     * @param parameters
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static ResultSet select(String sql, Object... parameters) {
        try {
            Connection conn = getConnection();
            PreparedStatement stmt = prepare(sql, conn, parameters);
            ResultSet rs = stmt.executeQuery();
            return rs;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 通用的增删改方法，传入SQL及SQL语句中的参数，返回受影响的行数
     *
     * @param sql
     * @param parameters
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static int update(String sql, Object... parameters) {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = getConnection();
            stmt = prepare(sql, conn, parameters);
            int affectedRows = stmt.executeUpdate();
            return affectedRows;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeStatement(stmt);
        }
    }

    /**
     * 查询数据库AUTO_INCREMENT生成的值
     *
     * @return
     */
    public static int getAutoIncrementValue() {
        String sql = "SELECT LAST_INSERT_ID()";
        ResultSet rs = select(sql);
        try {
            rs.next();
            int value = rs.getInt(1);
            return value;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    // 这是用来存储Connection对象的地方
    // 它保证在同一个线程内得到的Connection是同一个Connection， 这样便于我们控制事务
    private static ThreadLocal<Connection> TL = new ThreadLocal<Connection>();

    /**
     * 返回一个到数据库的连接
     *
     * @return
     * @throws SQLException
     */
    public static Connection getConnection() throws SQLException {
        // 首先尝试从TL里面去取得现成的Connection
        Connection conn = TL.get();
        // 如果取不到或者取到了但是是一个已经断掉的连接，说明需要新建一个
        if (conn == null || conn.isClosed()) {
            conn = DriverManager.getConnection(url, user, password);
            TL.set(conn);
        }
        return conn;
    }

    /**
     * 开启事务
     */
    public static void startTrasnaction() {
        try {
            Connection conn = getConnection();
            if (null != conn) {
                conn.setAutoCommit(false);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 提交事务
     */
    public static void commit() {
        try {
            Connection conn = getConnection();
            conn.setAutoCommit(false);
            if (null != conn) {
                conn.commit();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 关闭连接，此方法应该在Service层调用
     */
    public static void closeConnection() {
        try {
            Connection conn = getConnection();
            if (null != conn) {
                conn.close();
                TL.remove();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void closeStatement(Statement stmt) {
        if (null != stmt)
            try {
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
    }

    /**
     * 预处理SQL语句并设置SQL中的参数值
     *
     * @param sql
     * @param conn
     * @param parameters
     * @return
     * @throws SQLException
     */
    private static PreparedStatement prepare(String sql, Connection conn, Object... parameters) throws SQLException {
        PreparedStatement stmt = conn.prepareStatement(sql);
        if (parameters != null && parameters.length > 0) {
            for (int i = 0; i < parameters.length; i++) {
                stmt.setObject(i + 1, parameters[i]);
            }
        }
        return stmt;
    }

}

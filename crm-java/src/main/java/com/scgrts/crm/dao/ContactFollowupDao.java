package com.scgrts.crm.dao;

import com.scgrts.crm.model.entity.ContactFollowup;

import java.util.List;

public interface ContactFollowupDao {
    /**
     * 新增数据记录
     *
     * @param entity
     * @return
     */
    Integer saveFollowup(ContactFollowup entity);

    /**
     * 根据id查询所有根据记录
     *
     * @param id
     * @return
     */
    List<ContactFollowup> findFollowup(Integer id);

}

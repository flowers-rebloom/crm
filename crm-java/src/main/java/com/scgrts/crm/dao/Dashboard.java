package com.scgrts.crm.dao;

import com.scgrts.crm.model.dto.KPIDTO;
import com.scgrts.crm.model.dto.RemindDTO;
import com.scgrts.crm.model.dto.ResourceDTO;

import java.util.List;

/**
 * 仪表盘数据库接口
 */
public interface Dashboard {

    /**
     * 统计指定个人的业绩指标完成率
     *
     * @param username
     * @return
     */
    KPIDTO countKPIPersonal(String username);

    /**
     * 统计指定个人所属部门的业绩指标完成率
     *
     * @param username
     * @return
     */
    KPIDTO countKPIOfDept(String username);


    /**
     * 统计指定用户今日待联系的资源（线索、客户、商机统称资源）
     *
     * @param username
     * @return
     */
    List<ResourceDTO> countResourcesToBeConnectToday(String username);

    /**
     * 统计指定用户的遗忘资源数
     *
     * @param username
     * @return
     */
    RemindDTO countRemindResources(String username);
}

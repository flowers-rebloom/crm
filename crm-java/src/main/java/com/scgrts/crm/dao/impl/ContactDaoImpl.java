package com.scgrts.crm.dao.impl;

import com.scgrts.crm.dao.ContactDao;
import com.scgrts.crm.model.dto.ContactDTO;
import com.scgrts.crm.model.dto.ForSelect;
import com.scgrts.crm.model.dto.common.QueryParamDTO;
import com.scgrts.crm.model.entity.BusinessFollowup;
import com.scgrts.crm.model.entity.Contact;
import com.scgrts.crm.model.entity.ContactFollowup;

import java.util.List;

/**
 * 联系人管理数据库实现类
 */
public class ContactDaoImpl implements ContactDao {
    @Override
    public Integer save(Contact entity) {
        return null;
    }

    @Override
    public List<Contact> findAll(QueryParamDTO queryParamDTO) {
        return null;
    }

    @Override
    public Integer findTotal(String q) {
        return null;
    }

    @Override
    public List<Contact> findPage(QueryParamDTO queryParamDTO) {
        return null;
    }

    @Override
    public Contact findById(Integer id) {
        return null;
    }

    @Override
    public ContactDTO findDetailsById(Integer id) {
        return null;
    }

    @Override
    public Integer update(Contact entity) {
        return null;
    }

    @Override
    public Integer delete(Integer id) {
        return null;
    }

    @Override
    public Integer batchDelete(Integer[] id) {
        return null;
    }

    @Override
    public List<ForSelect> contactForSelect(String q) {
        return null;
    }

    @Override
    public List<Contact> findContacts(Integer contractId) {
        return null;
    }
}

package com.scgrts.crm.dao.impl;

import com.scgrts.crm.dao.ClueDao;
import com.scgrts.crm.model.dto.ClueDTO;
import com.scgrts.crm.model.dto.SalesReportDTO;
import com.scgrts.crm.model.dto.common.QueryParamDTO;
import com.scgrts.crm.model.entity.BusinessFollowup;
import com.scgrts.crm.model.entity.Clue;
import com.scgrts.crm.model.enums.Source;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 线索模块数据库实现类
 */
public class ClueDaoImpl implements ClueDao {
    @Override
    public Integer save(Clue entity) {
        String sql = "INSERT INTO `gcrm`.`clue` (`clue_name`,`clue_source`,`clue_phone`,`clue_address`,`clue_remark`,`create_emp_id`,`owner_emp_id`,"
                + "`followup_status`,`last_followup_time`,`last_followup_presentation`,`next_followup_time`)VALUES("
                + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        int insert = JdbcTemplate.update(sql,
                new Object[]{entity.getClueName(), entity.getClueSource().getValue(), entity.getCluePhone(),
                        entity.getClueAddress(), entity.getClueRemark(), entity.getCreateEmpId(),
                        entity.getOwnerEmpId(), entity.getFollowupStatus(), entity.getLastFollowupTime(),
                        entity.getLastFollowupPresentation(), entity.getNextFollowupTime()});
        int clueId = JdbcTemplate.getAutoIncrementValue();
        entity.setClueId(clueId);
        return insert;
    }

    @Override
    public List<Clue> findAll(QueryParamDTO queryParamDTO) {
        String sql = "SELECT * FROM clue ORDER BY clue_id DESC LIMIT ?, ?";
        int pageSize = queryParamDTO.getPageSize();
        int pageNo = queryParamDTO.getPageNo();
        int skip = pageSize * (pageNo - 1);
        ResultSet rs = JdbcTemplate.select(sql, skip, pageSize);
        List<Clue> list = new ArrayList<>();
        try {
            while (rs.next()) {
                Clue clue = new Clue();
                clue.setClueAddress(rs.getString("clue_address"));
                clue.setClueId(rs.getInt("clue_id"));
                clue.setClueName(rs.getString("clue_name"));
                clue.setCluePhone(rs.getString("clue_phone"));
                clue.setClueRemark(rs.getString("clue_remark"));
                clue.setClueSource(Source.fromInt(rs.getInt("clue_source")));
                clue.setCreateEmpId(rs.getInt("create_emp_id"));
                clue.setCreateTime(rs.getTimestamp("create_time"));
                clue.setFollowupStatus(rs.getBoolean("followup_status"));
                clue.setLastFollowupPresentation(rs.getString("last_followup_presentation"));
                clue.setLastFollowupTime(rs.getDate("last_followup_time"));
                clue.setOwnerEmpId(rs.getInt("owner_emp_id"));
                clue.setUpdateTime(rs.getDate("update_time"));
                clue.setNextFollowupTime(rs.getDate("next_followup_time"));
                list.add(clue);
            }
            return list;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Integer findTotal(String q) {
        String sql = "SELECT COUNT(1) FROM clue WHERE clue_name LIKE CONCAT('%', ?, '%') OR clue_phone LIKE CONCAT('%', ?, '%')";
        ResultSet rs = JdbcTemplate.select(sql, q, q);
        try {
            rs.next();
            return rs.getInt(1);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public List<Clue> findPage(QueryParamDTO queryParamDTO) {
        String sql = "SELECT * FROM clue WHERE clue_name LIKE CONCAT('%', ?, '%') OR clue_phone LIKE CONCAT('%', ?, '%') ORDER BY clue_id DESC LIMIT ?, ?";
        String q = queryParamDTO.getQ();
        int pageSize = queryParamDTO.getPageSize();
        int pageNo = queryParamDTO.getPageNo();
        int skip = pageSize * (pageNo - 1);
        ResultSet rs = JdbcTemplate.select(sql, q, q, skip, pageSize);
        List<Clue> list = new ArrayList<>();
        try {
            while (rs.next()) {
                Clue clue = new Clue();
                clue.setClueAddress(rs.getString("clue_address"));
                clue.setClueId(rs.getInt("clue_id"));
                clue.setClueName(rs.getString("clue_name"));
                clue.setCluePhone(rs.getString("clue_phone"));
                clue.setClueRemark(rs.getString("clue_remark"));
                clue.setClueSource(Source.fromInt(rs.getInt("clue_source")));
                clue.setCreateEmpId(rs.getInt("create_emp_id"));
                clue.setCreateTime(rs.getTimestamp("create_time"));
                clue.setFollowupStatus(rs.getBoolean("followup_status"));
                clue.setLastFollowupPresentation(rs.getString("last_followup_presentation"));
                clue.setLastFollowupTime(rs.getDate("last_followup_time"));
                clue.setOwnerEmpId(rs.getInt("owner_emp_id"));
                clue.setUpdateTime(rs.getDate("update_time"));
                clue.setNextFollowupTime(rs.getDate("next_followup_time"));
                list.add(clue);
            }
            return list;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public Clue findById(Integer id) {
        String sql = "SELECT * FROM clue WHERE clue_id = ?";
        ResultSet rs = JdbcTemplate.select(sql, id);
        Clue clue = new Clue();
        try {
            while (rs.next()) {
                clue.setClueAddress(rs.getString("clue_address"));
                clue.setClueId(rs.getInt("clue_id"));
                clue.setClueName(rs.getString("clue_name"));
                clue.setCluePhone(rs.getString("clue_phone"));
                clue.setClueRemark(rs.getString("clue_remark"));
                clue.setClueSource(Source.fromInt(rs.getInt("clue_source")));
                clue.setCreateEmpId(rs.getInt("create_emp_id"));
                clue.setCreateTime(rs.getTimestamp("create_time"));
                clue.setFollowupStatus(rs.getBoolean("followup_status"));
                clue.setLastFollowupPresentation(rs.getString("last_followup_presentation"));
                clue.setLastFollowupTime(rs.getDate("last_followup_time"));
                clue.setOwnerEmpId(rs.getInt("owner_emp_id"));
                clue.setUpdateTime(rs.getDate("update_time"));
                clue.setNextFollowupTime(rs.getDate("next_followup_time"));
            }
            return clue;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ClueDTO findDetailsById(Integer id) {
        String sql = " SELECT c.*, e1.emp_name `create_emp_name`, e2.emp_name `owner_emp_name`"
                + " FROM clue c INNER JOIN emp e1 ON c.create_emp_id = e1.emp_id"
                + "	INNER JOIN emp e2 ON c.owner_emp_id = e2.emp_id WHERE c.clue_id = ?";
        ResultSet rs = JdbcTemplate.select(sql, id);
        ClueDTO clueDTO = new ClueDTO();
        try {
            while (rs.next()) {
                clueDTO.setClueAddress(rs.getString("clue_address"));
                clueDTO.setClueId(rs.getInt("clue_id"));
                clueDTO.setClueName(rs.getString("clue_name"));
                clueDTO.setCluePhone(rs.getString("clue_phone"));
                clueDTO.setClueRemark(rs.getString("clue_remark"));
                clueDTO.setClueSource(Source.fromInt(rs.getInt("clue_source")));
                clueDTO.setCreateEmpId(rs.getInt("create_emp_id"));
                clueDTO.setCreateTime(rs.getDate("create_time"));
                clueDTO.setFollowupStatus(rs.getBoolean("followup_status"));
                clueDTO.setLastFollowupPresentation(rs.getString("last_followup_presentation"));
                clueDTO.setLastFollowupTime(rs.getDate("last_followup_time"));
                clueDTO.setOwnerEmpId(rs.getInt("owner_emp_id"));
                clueDTO.setUpdateTime(rs.getDate("update_time"));
                clueDTO.setCreateEmpName(rs.getString("create_emp_name"));
                clueDTO.setOwnerEmpName(rs.getString("owner_emp_name"));
            }
            return clueDTO;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Integer update(Clue entity) {
        String sql = "UPDATE `gcrm`.`clue`SET `clue_name` = ?,`clue_source` = ?,`clue_phone` = ?,`clue_address` = ?,`clue_remark` = ?," +
                "`update_time` = ?,`owner_emp_id` = ?,`followup_status` = ?,`last_followup_time` = ?,`last_followup_presentation` = ?," +
                "`next_followup_time` = ? WHERE `clue_id` = ?";
        int update = JdbcTemplate.update(sql,
                new Object[]{entity.getClueName(), entity.getClueSource().getValue(), entity.getCluePhone(),
                        entity.getClueAddress(), entity.getClueRemark(), entity.getUpdateTime(), entity.getOwnerEmpId(),
                        entity.getFollowupStatus(), entity.getLastFollowupTime(), entity.getLastFollowupPresentation(),
                        entity.getNextFollowupTime(), entity.getClueId()});
        return update;
    }

    @Override
    public Integer delete(Integer id) {
        String sql = "DELETE FROM clue WHERE clue_id = ?";
        int delete = JdbcTemplate.update(sql, id);
        return delete;
    }

    @Override
    public Integer batchDelete(Integer[] id) {
        Integer count = 0;
        for (Integer onId : id) {
            count = delete(onId);
        }
        return count;
    }

    @Override
    public SalesReportDTO countSalesReportPersonal(String username) {

        return null;
    }

    @Override
    public SalesReportDTO countSalesReportOfDept(String username) {

        return null;
    }
}

package com.scgrts.crm.dao.impl;

import com.scgrts.crm.dao.SalesFunnelDao;
import com.scgrts.crm.model.dto.BusinessDTO;
import com.scgrts.crm.model.dto.SalesFunnel;
import com.scgrts.crm.model.entity.Business;
import com.scgrts.crm.model.enums.BusinessStep;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SalesFunnelDaoImpl implements SalesFunnelDao {
    @Override
    public List<SalesFunnel> moneySalesFunnel(Date dateBegin, Date dateEnd) {
        String q = " SELECT business_current_step,business_end_status FROM business WHERE create_time >= ?   AND create_time <= ? ";
        ResultSet rs = JdbcTemplate.select(q, dateBegin, dateEnd);
        List<SalesFunnel> list = new ArrayList<>();
        try {
            while (rs.next()) {
                SalesFunnel salesFunnel = new SalesFunnel();
                salesFunnel.setCurrent_step(rs.getInt("business_current_step"));
                salesFunnel.setEnd_status(rs.getInt("business_end_status"));
                list.add(salesFunnel);
            }
            return list;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

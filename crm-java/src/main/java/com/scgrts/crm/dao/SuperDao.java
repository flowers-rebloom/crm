package com.scgrts.crm.dao;

import com.scgrts.crm.model.dto.common.QueryParamDTO;

import java.util.List;

/**
 * 数据接口父类
 *
 * @param <E>
 */
public interface SuperDao<E, EDTO> {
    /**
     * 添加新数据
     *
     * @param entity
     * @return
     */
    Integer save(E entity);

    /**
     * 查询所有数据
     *
     * @param queryParamDTO
     * @return
     */
    List<E> findAll(QueryParamDTO queryParamDTO);

    /**
     * 查询所有满足条件的数据总数
     *
     * @param q
     * @return 总条数
     */
    Integer findTotal(String q);

    /**
     * 根据搜索关键字、每页条数、页码查找数据
     *
     * @param queryParamDTO 封装搜索关键字、每页条数、页码查找线索的一个对象
     * @return 总行数
     */
    List<E> findPage(QueryParamDTO queryParamDTO);

    /**
     * 根据id查询数据
     *
     * @param id
     * @return
     */
    E findById(Integer id);

    /**
     * 根据id查询实体详情
     *
     * @param id
     * @return
     */
    EDTO findDetailsById(Integer id);

    /**
     * 修改数据
     *
     * @param entity
     * @return
     */
    Integer update(E entity);

    /**
     * 删除单个数据
     *
     * @param id
     * @return
     */
    Integer delete(Integer id);

    /**
     * 删除多个id的数据
     *
     * @param id
     * @return
     */
    Integer batchDelete(Integer[] id);
}

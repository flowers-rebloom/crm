package com.scgrts.crm.dao;

import com.scgrts.crm.model.dto.ContractDTO;
import com.scgrts.crm.model.dto.SalesReportDTO;
import com.scgrts.crm.model.entity.Contact;
import com.scgrts.crm.model.entity.Contract;

import java.util.List;

/**
 * 合同数据库接口
 */
public interface ContractDao extends SuperDao<Contract, ContractDTO> {

    /**
     * 查找指定客户的联系人列表
     *
     * @param contractId
     * @return
     */
    List<Contact> findContacts(Integer contractId);

    /**
     * 设置指定合同的回款状态
     *
     * @param contractId 合同id
     * @param status     回款状态值
     * @return
     */
    Integer setPaymentStatus(int contractId, boolean status);

    /**
     * 统计指定个人的销售简报
     *
     * @param username 当前用户名
     * @return
     */
    SalesReportDTO countSalesReportPersonal(String username);

    /**
     * 统计指定个人所属部门的销售简报
     *
     * @param username 当前用户名
     * @return
     */
    SalesReportDTO countSalesReportOfDept(String username);
}

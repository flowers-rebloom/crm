package com.scgrts.crm.dao;


import com.scgrts.crm.model.entity.CustFollowup;

import java.util.List;

public interface CustFollowupDao {
    /**
     * 新增数据记录
     *
     * @param entity
     * @return
     */
    Integer saveFollowup(CustFollowup entity);

    /**
     * 根据id查询所有根据记录
     *
     * @param id
     * @return
     */
    List<CustFollowup> findFollowup(Integer id);
}

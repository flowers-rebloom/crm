package com.scgrts.crm.dao;

import com.scgrts.crm.model.dto.AreaDTO;
import com.scgrts.crm.model.entity.Area;

import java.util.List;

/**
 * 客户分布数据库接口
 */
public interface AreaDao {

    /**
     * 查询所有上级地区
     *
     * @return
     */
    AreaDTO findCustArea();

    /**
     * 根据父id查询地区
     *
     * @param parentId
     * @return
     */
    List<Area> findByParentId(Integer parentId);
}

package com.scgrts.crm.dao.impl;

import com.scgrts.crm.dao.CustDao;
import com.scgrts.crm.model.dto.CustDTO;
import com.scgrts.crm.model.dto.ForSelect;
import com.scgrts.crm.model.dto.SalesReportDTO;
import com.scgrts.crm.model.dto.common.QueryParamDTO;
import com.scgrts.crm.model.entity.Cust;
import com.scgrts.crm.model.enums.Source;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CustDaoImpl implements CustDao {
    @Override
    public Integer save(Cust entity) {
        System.out.println(1);
        String sql = "INSERT INTO `cust` (	`cust_name`,`cust_source`,`cust_phone`,`cust_website`,`cust_province`,`cust_city`,"
                + "	`cust_district`,`cust_address`,`longitude`,`latitude`,`cust_remark`,`create_emp_id`,`owner_emp_id`,"
                + "	`followup_status`,`last_followup_time`,	`last_followup_presentation`,`next_followup_time`,"
                + "	`lock_status`,`deal_status`,`delete_status` )VALUES("
                + "	?, ?, ?, ? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,?)";
        int save = JdbcTemplate.update(sql, entity.getCustName(), entity.getCustSource().getValue(), entity.getCustPhone(),
                entity.getCustWebsite(), entity.getCustProvince(), entity.getCustCity(), entity.getCustDistrict(),
                entity.getCustAddress(), entity.getLongitude(), entity.getLatitude(), entity.getCustRemark(),
                entity.getCreateEmpId(), entity.getOwnerEmpId(), entity.getFollowupStatus(),
                entity.getLastFollowupTime(), entity.getLastFollowupPresentation(), entity.getNextFollowupTime(),
                entity.getLockStatus(), entity.getDealStatus(), entity.getDeleteStatus());
        int custId = JdbcTemplate.getAutoIncrementValue();

        entity.setCustId(custId);
        return save;
    }

    @Override
    public List<Cust> findAll(QueryParamDTO queryParamDTO) {
        String sql = "SELECT c.*, e.emp_name owner_emp_name FROM cust c "
                + "INNER JOIN emp e ON c.owner_emp_id = e.emp_id WHERE c.delete_status = 0 ORDER BY cust_id DESC "
                + "LIMIT ?, ?";

        int pageSize = queryParamDTO.getPageSize();
        int pageNo = queryParamDTO.getPageNo();
        int skip = pageSize * (pageNo - 1);
        ResultSet rs = JdbcTemplate.select(sql, skip, pageSize);
        ArrayList<Cust> list = new ArrayList<Cust>();
        try {
            while (rs.next()) {
                CustDTO custDTO = new CustDTO();
                custDTO.setCustId(rs.getInt("cust_id"));
                custDTO.setCustName(rs.getString("cust_name"));
                custDTO.setCustPhone(rs.getString("cust_phone"));
                custDTO.setCustSource(Source.fromInt(rs.getInt("cust_source")));
                custDTO.setCustAddress(rs.getString("cust_address"));
                custDTO.setCustProvince(rs.getInt("cust_province"));
                custDTO.setCustCity(rs.getInt("cust_city"));
                custDTO.setCustDistrict(rs.getInt("cust_district"));
                custDTO.setCustRemark(rs.getString("cust_remark"));
                custDTO.setCreateEmpId(rs.getInt("cust_id"));
                custDTO.setOwnerEmpId(rs.getInt("owner_emp_id"));
                custDTO.setCreateTime(rs.getDate("create_time"));
                custDTO.setOwnerEmpName(rs.getString("owner_emp_name"));
                custDTO.setUpdateTime(rs.getDate("update_time"));
                custDTO.setLockStatus(rs.getBoolean("lock_status"));
                custDTO.setDealStatus(rs.getBoolean("deal_status"));
                custDTO.setLatitude(rs.getString("latitude"));
                custDTO.setLongitude(rs.getString("longitude"));
                custDTO.setLastFollowupTime(rs.getDate("last_followup_time"));
                custDTO.setLastFollowupPresentation(rs.getString("last_followup_presentation"));
                custDTO.setNextFollowupTime(rs.getDate("next_followup_time"));
                custDTO.setCustWebsite(rs.getString("cust_website"));
                list.add(custDTO);
            }
            return list;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Integer findTotal(String q) {
        String sql = "SELECT COUNT(1) FROM cust WHERE delete_status = 0 AND (cust_name LIKE CONCAT('%', ?, '%') OR cust_phone LIKE CONCAT('%', ?, '%'))";
        ResultSet rs = JdbcTemplate.select(sql, q, q);
        try {
            rs.next();
            return rs.getInt(1);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Cust> findPage(QueryParamDTO queryParamDTO) {
        String sql = "SELECT c.*, e.emp_name owner_emp_name FROM cust c "
                + "INNER JOIN emp e ON c.owner_emp_id = e.emp_id WHERE (cust_name LIKE CONCAT('%', ?, '%') "
                + "OR cust_phone LIKE CONCAT('%', ?, '%')) AND c.delete_status = 0 ORDER BY cust_id DESC "
                + "LIMIT ?, ?";
        String q = queryParamDTO.getQ();
        int pageSize = queryParamDTO.getPageSize();
        int pageNo = queryParamDTO.getPageNo();
        int skip = pageSize * (pageNo - 1);
        ResultSet rs = JdbcTemplate.select(sql, q, q, skip, pageSize);
        ArrayList<Cust> list = new ArrayList<Cust>();
        try {
            while (rs.next()) {
                CustDTO custDTO = new CustDTO();
                custDTO.setCustId(rs.getInt("cust_id"));
                custDTO.setCustName(rs.getString("cust_name"));
                custDTO.setCustPhone(rs.getString("cust_phone"));
                custDTO.setCustSource(Source.fromInt(rs.getInt("cust_source")));
                custDTO.setCustAddress(rs.getString("cust_address"));
                custDTO.setCustProvince(rs.getInt("cust_province"));
                custDTO.setCustCity(rs.getInt("cust_city"));
                custDTO.setCustDistrict(rs.getInt("cust_district"));
                custDTO.setCustRemark(rs.getString("cust_remark"));
                custDTO.setCreateEmpId(rs.getInt("cust_id"));
                custDTO.setOwnerEmpId(rs.getInt("owner_emp_id"));
                custDTO.setCreateTime(rs.getDate("create_time"));
                custDTO.setOwnerEmpName(rs.getString("owner_emp_name"));
                custDTO.setUpdateTime(rs.getDate("update_time"));
                custDTO.setLockStatus(rs.getBoolean("lock_status"));
                custDTO.setDealStatus(rs.getBoolean("deal_status"));
                custDTO.setLatitude(rs.getString("latitude"));
                custDTO.setLongitude(rs.getString("longitude"));
                custDTO.setLastFollowupTime(rs.getDate("last_followup_time"));
                custDTO.setLastFollowupPresentation(rs.getString("last_followup_presentation"));
                custDTO.setNextFollowupTime(rs.getDate("next_followup_time"));
                custDTO.setCustWebsite(rs.getString("cust_website"));
                list.add(custDTO);
            }
            return list;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Cust findById(Integer id) {
        String sql = "SELECT * FROM cust WHERE delete_status = 0 AND cust_id=?";
        // 调用jdbctemplate执行查询 得到结果集
        ResultSet rs = JdbcTemplate.select(sql, id);
        Cust cust = new Cust();
        try {
            while (rs.next()) {
                cust.setCustId(rs.getInt("cust_id"));
                cust.setCustName(rs.getString("cust_name"));
                cust.setCustSource(Source.fromInt(rs.getInt("cust_source")));
                cust.setCustPhone(rs.getString("cust_phone"));
                cust.setCustProvince(rs.getInt("cust_province"));
                cust.setCustCity(rs.getInt("cust_city"));
                cust.setCustDistrict(rs.getInt("cust_district"));
                cust.setCustRemark(rs.getString("cust_remark"));
                cust.setCustWebsite(rs.getString("cust_website"));
                cust.setLongitude(rs.getString(("longitude")));
                cust.setLatitude(rs.getString("latitude"));
                cust.setCreateTime(rs.getDate("create_time"));
                cust.setCreateEmpId(rs.getInt("create_emp_id"));
                cust.setUpdateTime(rs.getDate("update_time"));
                cust.setOwnerEmpId(rs.getInt("owner_emp_id"));
                cust.setFollowupStatus(rs.getBoolean("followup_status"));
                cust.setLastFollowupTime(rs.getDate("last_followup_time"));
                cust.setLastFollowupPresentation(rs.getString("last_followup_presentation"));
                cust.setNextFollowupTime(rs.getDate("next_followup_time"));
                cust.setLockStatus(rs.getBoolean("lock_status"));
                cust.setDealStatus(rs.getBoolean("deal_status"));
                cust.setDeleteStatus(rs.getBoolean("delete_status"));
                cust.setCustAddress(rs.getString("cust_address"));
            }
            return cust;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public CustDTO findDetailsById(Integer id) {
        String sql = "SELECT c.*,e1.emp_name `create_emp_name`,e2.emp_name `owner_emp_name` FROM cust c "
                + "INNER JOIN emp e1 ON c.create_emp_id = e1.emp_id INNER JOIN emp e2 ON c.owner_emp_id=e2.emp_id"
                + " WHERE c.delete_status = 0 AND cust_id = ?";
        ResultSet rs = JdbcTemplate.select(sql, id);
        try {
            if (!rs.next())
                return null;
            CustDTO custDTO = new CustDTO();
            custDTO.setCustId(rs.getInt("cust_id"));
            custDTO.setCustName(rs.getString("cust_name"));
            custDTO.setCustPhone(rs.getString("cust_phone"));
            custDTO.setCustSource(Source.fromInt(rs.getInt("cust_source")));
            custDTO.setCustAddress(rs.getString("cust_address"));
            custDTO.setCustProvince(rs.getInt("cust_province"));
            custDTO.setCustCity(rs.getInt("cust_city"));
            custDTO.setCustDistrict(rs.getInt("cust_district"));
            custDTO.setCustRemark(rs.getString("cust_remark"));
            custDTO.setCreateEmpId(rs.getInt("cust_id"));
            custDTO.setOwnerEmpId(rs.getInt("owner_emp_id"));
            custDTO.setCreateTime(rs.getDate("create_time"));
            custDTO.setCreateEmpName(rs.getString("create_emp_name"));
            custDTO.setOwnerEmpName(rs.getString("owner_emp_name"));
            custDTO.setUpdateTime(rs.getDate("update_time"));
            custDTO.setLockStatus(rs.getBoolean("lock_status"));
            custDTO.setDealStatus(rs.getBoolean("deal_status"));
            custDTO.setLatitude(rs.getString("latitude"));
            custDTO.setLongitude(rs.getString("longitude"));
            custDTO.setLastFollowupTime(rs.getDate("last_followup_time"));
            custDTO.setLastFollowupPresentation(rs.getString("last_followup_presentation"));
            custDTO.setNextFollowupTime(rs.getDate("next_followup_time"));
            custDTO.setCustWebsite(rs.getString("cust_website"));
            return custDTO;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Integer update(Cust entity) {
        String sql = "UPDATE `gcrm`.`cust` SET `cust_name`= ?,`cust_source`= ?, `cust_phone`= ?, `cust_website`= ?, "
                + "`cust_province`= ?, `cust_city`= ?, `cust_district`= ?,`cust_address`= ?,`longitude`= ?, "
                + "`latitude`= ?,`cust_remark`= ?,`create_emp_id`= ?,`create_time`= ?,`update_time`= ?,`owner_emp_id`= ?, "
                + "`followup_status`= ?, `last_followup_time`= ?, `last_followup_presentation`= ?, `next_followup_time`= ?, "
                + "`lock_status`= ?, `deal_status`= ?, `delete_status`= ? WHERE `cust_id`= ?";
        // 调用jdbctemplate执行更新
        int update = JdbcTemplate.update(sql,
                new Object[]{entity.getCustName(), entity.getCustSource().getValue(), entity.getCustPhone(),
                        entity.getCustWebsite(), entity.getCustProvince(), entity.getCustCity(),
                        entity.getCustDistrict(), entity.getCustAddress(), entity.getLatitude(), entity.getLongitude(),
                        entity.getCustRemark(), entity.getCreateEmpId(), entity.getCreateTime(), entity.getUpdateTime(),
                        entity.getOwnerEmpId(), entity.getFollowupStatus(), entity.getLastFollowupTime(),
                        entity.getLastFollowupPresentation(), entity.getNextFollowupTime(), entity.getLockStatus(),
                        entity.getDealStatus(), entity.getDeleteStatus(), entity.getCustId()});
        return update;
    }

    @Override
    public Integer delete(Integer id) {
        String sql = "UPDATE cust SET delete_status = 1 WHERE cust_id = ?";
        int delete = JdbcTemplate.update(sql, id);
        return delete;
    }

    @Override
    public Integer batchDelete(Integer[] id) {
        int a = 0;
        for (int i = 0; i < id.length; i++) {
            delete(id[i]);
            a++;
        }
        return a;
    }

    @Override
    public Integer setDealStatus(Integer id, boolean status) {
        return null;
    }

    @Override
    public Integer putIntoPublicSea(Integer id) {
        return null;
    }

    @Override
    public SalesReportDTO countSalesReportPersonal(String username) {
        return null;
    }

    @Override
    public SalesReportDTO countSalesReportOfDept(String username) {
        return null;
    }

    @Override
    public List<ForSelect> custForSelect(String q) {
        String sql = "SELECT cust_id,cust_name FROM cust WHERE cust_name LIKE CONCAT('%', ?, '%')";
        ResultSet rs = JdbcTemplate.select(sql, q);
        ArrayList<ForSelect> custNames = new ArrayList<>();
        try {
            while (rs.next()) {
                custNames.add(new ForSelect(rs.getInt("cust_id"), rs.getString("cust_name")));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return custNames;
    }

    @Override
    public List<Cust> findtest() {
        String q = "SELECT * FROM cust";
        ResultSet rs = JdbcTemplate.select(q);
        List<Cust> list = new ArrayList<>();
        try {
            while (rs.next()) {
                Cust cust = new Cust();
                cust.setCustName(rs.getString("cust_name"));
                cust.setCustSource(Source.fromInt(rs.getInt("cust_source")));
                cust.setCustPhone(rs.getString("cust_phone"));
                cust.setCustProvince(rs.getInt("cust_province"));
                cust.setCustCity(rs.getInt("cust_city"));
                cust.setCustDistrict(rs.getInt("cust_district"));
                cust.setCustRemark(rs.getString("cust_remark"));
                cust.setCustWebsite(rs.getString("cust_website"));
                cust.setLongitude(rs.getString(("longitude")));
                cust.setLatitude(rs.getString("latitude"));
                cust.setCreateTime(rs.getDate("create_time"));
                cust.setCreateEmpId(rs.getInt("create_emp_id"));
                cust.setUpdateTime(rs.getDate("update_time"));
                cust.setOwnerEmpId(rs.getInt("owner_emp_id"));
                cust.setFollowupStatus(rs.getBoolean("followup_status"));
                cust.setLastFollowupTime(rs.getDate("last_followup_time"));
                cust.setLastFollowupPresentation(rs.getString("last_followup_presentation"));
                cust.setNextFollowupTime(rs.getDate("next_followup_time"));
                cust.setLockStatus(rs.getBoolean("lock_status"));
                cust.setDealStatus(rs.getBoolean("deal_status"));
                cust.setDeleteStatus(rs.getBoolean("delete_status"));
                cust.setCustAddress(rs.getString("cust_address"));
                list.add(cust);
            }

            return list;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

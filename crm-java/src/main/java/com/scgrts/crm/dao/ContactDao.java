package com.scgrts.crm.dao;

import com.scgrts.crm.model.dto.ContactDTO;
import com.scgrts.crm.model.dto.ForSelect;
import com.scgrts.crm.model.entity.Contact;

import java.util.List;


/**
 * 联系人管理数据库接口
 */
public interface ContactDao extends SuperDao<Contact, ContactDTO> {
    /**
     * 通过关键字选取客户联系人
     *
     * @param q
     * @return
     */
    List<ForSelect> contactForSelect(String q);

    /**
     * 查找指定客户的联系人列表
     *
     * @param contractId
     * @return
     */
    List<Contact> findContacts(Integer contractId);
}

package com.scgrts.crm.dao.impl;

import com.scgrts.crm.dao.ContractDao;
import com.scgrts.crm.model.dto.ContractDTO;
import com.scgrts.crm.model.dto.SalesReportDTO;
import com.scgrts.crm.model.dto.common.QueryParamDTO;
import com.scgrts.crm.model.entity.Contact;
import com.scgrts.crm.model.entity.Contract;

import java.util.List;

public class ContractDaoImpl implements ContractDao {
    @Override
    public List<Contact> findContacts(Integer contractId) {
        return null;
    }

    @Override
    public Integer setPaymentStatus(int contractId, boolean status) {
        return null;
    }

    @Override
    public SalesReportDTO countSalesReportPersonal(String username) {
        return null;
    }

    @Override
    public SalesReportDTO countSalesReportOfDept(String username) {
        return null;
    }

    @Override
    public Integer save(Contract entity) {
        return null;
    }

    @Override
    public List<Contract> findAll(QueryParamDTO queryParamDTO) {
        return null;
    }

    @Override
    public Integer findTotal(String q) {
        return null;
    }

    @Override
    public List<Contract> findPage(QueryParamDTO queryParamDTO) {
        return null;
    }

    @Override
    public Contract findById(Integer id) {
        return null;
    }

    @Override
    public ContractDTO findDetailsById(Integer id) {
        return null;
    }

    @Override
    public Integer update(Contract entity) {
        return null;
    }

    @Override
    public Integer delete(Integer id) {
        return null;
    }

    @Override
    public Integer batchDelete(Integer[] id) {
        return null;
    }
}

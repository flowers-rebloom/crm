package com.scgrts.crm.dao.impl;

import com.scgrts.crm.dao.PaymentDao;
import com.scgrts.crm.model.dto.PaymentDTO;
import com.scgrts.crm.model.dto.SalesReportDTO;
import com.scgrts.crm.model.dto.common.QueryParamDTO;
import com.scgrts.crm.model.entity.Payment;

import java.util.List;

public class PaymentDaoImpl implements PaymentDao {
    @Override
    public Integer save(Payment entity) {
        return null;
    }

    @Override
    public List<Payment> findAll(QueryParamDTO queryParamDTO) {
        return null;
    }

    @Override
    public Integer findTotal(String q) {
        return null;
    }

    @Override
    public List<Payment> findPage(QueryParamDTO queryParamDTO) {
        return null;
    }

    @Override
    public Payment findById(Integer id) {
        return null;
    }

    @Override
    public PaymentDTO findDetailsById(Integer id) {
        return null;
    }

    @Override
    public Integer update(Payment entity) {
        return null;
    }

    @Override
    public Integer delete(Integer id) {
        return null;
    }

    @Override
    public Integer batchDelete(Integer[] id) {
        return null;
    }

    @Override
    public SalesReportDTO countSalesReportPersonal(String username) {
        return null;
    }

    @Override
    public SalesReportDTO countSalesReportOfDept(String username) {
        return null;
    }
}

package com.scgrts.crm.dao.impl;

import com.scgrts.crm.dao.LoginDao;
import com.scgrts.crm.model.dto.LoginDTO;
import com.scgrts.crm.model.entity.Emp;
import com.scgrts.crm.model.enums.EmpStatus;
import com.scgrts.crm.model.enums.Gender;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginDaoImpl implements LoginDao {
    @Override
    public LoginDTO login(String username) {
        return null;
    }

    @Override
    public Emp findEmpByName(String username) {
        String sql = "SELECT * FROM emp WHERE  emp_code= ?";
        ResultSet rs = JdbcTemplate.select(sql, username);
        Emp emp = new Emp();
        try {
            while (rs.next()) {
                emp.setEmpId(rs.getInt("emp_id"));
                emp.setDeptId(rs.getInt("dept_id"));
                emp.setEmpCode(rs.getString("emp_code"));
                emp.setEmpName(rs.getString("emp_name"));
                emp.setEmpPhone(rs.getString("emp_phone"));
                emp.setEmpPwd(rs.getString("emp_pwd"));
                emp.setEmpSalt(rs.getString("emp_salt"));
                emp.setEmpEmail(rs.getString("emp_email"));
                emp.setEmpPost(rs.getString("emp_post"));
                emp.setEmpSex(Gender.fromInt(1));
                emp.setHeadPhoto(rs.getString("head_photo"));
                emp.setStatus(EmpStatus.fromInt(rs.getInt("status")));
                emp.setSuperior(rs.getInt("superior"));
                emp.setCreateTime(rs.getDate("create_time"));
                emp.setUpdateTime(rs.getDate("update_time"));
                emp.setLastLoginIp(rs.getString("last_login_ip"));
                emp.setLastLoginTime(rs.getDate("last_login_time"));
            }
            return emp;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

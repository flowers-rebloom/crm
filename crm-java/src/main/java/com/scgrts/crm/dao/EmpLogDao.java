package com.scgrts.crm.dao;

import com.scgrts.crm.model.entity.EmpLog;

/**
 * 员工操作日志接口
 */
public interface EmpLogDao {
    /**
     * 添加员工操作日志
     *
     * @param empLog
     * @return
     */
    Integer sevEmpLog(EmpLog empLog);
}

package com.scgrts.crm.dao.impl;

import com.scgrts.crm.dao.EmpDao;
import com.scgrts.crm.model.entity.Emp;
import com.scgrts.crm.model.enums.EmpStatus;
import com.scgrts.crm.model.enums.Gender;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class EmpDaoImpl implements EmpDao {
    @Override
    public Integer update(Emp emp) {
        String sql = "UPDATE `emp` SET `dept_id` = ?,`emp_code` = ?,`emp_name` = ?,`emp_phone` = ?,`emp_pwd` = ?,`emp_salt` = ?,"
                + "`emp_email` = ?,`emp_post` = ?,`emp_sex` = ?,`head_photo` = ?,`status` = ?,`superior` = ?,`update_time` = ?,"
                + "`last_login_time` = ?,`last_login_ip` = ? WHERE	`emp_id` = ?";
        int update = JdbcTemplate.update(sql, emp.getDeptId(), emp.getEmpCode(), emp.getEmpName(), emp.getEmpPhone(),
                emp.getEmpPwd(), emp.getEmpSalt(), emp.getEmpEmail(), emp.getEmpPost(),
                emp.getEmpSex().getValue(), emp.getHeadPhoto(), emp.getStatus().getValue(),
                emp.getSuperior(), emp.getUpdateTime(), emp.getLastLoginTime(), emp.getLastLoginIp(),
                emp.getEmpId());
        return update;
    }

    @Override
    public Emp findByCode(String empCode) {
        String sql = "SELECT * FROM emp WHERE  emp_code= ?";
        ResultSet rs = JdbcTemplate.select(sql, empCode);
        Emp emp = new Emp();
        try {
            while (rs.next()) {
                emp.setEmpId(rs.getInt("emp_id"));
                emp.setDeptId(rs.getInt("dept_id"));
                emp.setEmpCode(rs.getString("emp_code"));
                emp.setEmpName(rs.getString("emp_name"));
                emp.setEmpPhone(rs.getString("emp_phone"));
                emp.setEmpPwd(rs.getString("emp_pwd"));
                emp.setEmpSalt(rs.getString("emp_salt"));
                emp.setEmpEmail(rs.getString("emp_email"));
                emp.setEmpPost(rs.getString("emp_post"));
                emp.setEmpSex(Gender.fromInt(1));
                emp.setHeadPhoto(rs.getString("head_photo"));
                emp.setStatus(EmpStatus.fromInt(rs.getInt("status")));
                emp.setSuperior(rs.getInt("superior"));
                emp.setCreateTime(rs.getDate("create_time"));
                emp.setUpdateTime(rs.getDate("update_time"));
                emp.setLastLoginIp(rs.getString("last_login_ip"));
                emp.setLastLoginTime(rs.getDate("last_login_time"));
            }
            return emp;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Integer updateHead(Integer empId, String headPhotoAccessPath) {
        return null;
    }

    @Override
    public List<Emp> findAll() {
        return null;
    }

    @Override
    public Emp findById(Integer id) {
        String sql = "SELECT * FROM emp WHERE emp_id = ?";
        ResultSet rs = JdbcTemplate.select(sql, id);
        Emp emp = new Emp();
        try {
            while (rs.next()) {
                emp.setEmpId(rs.getInt("emp_id"));
                emp.setDeptId(rs.getInt("dept_id"));
                emp.setEmpCode(rs.getString("emp_code"));
                emp.setEmpName(rs.getString("emp_name"));
                emp.setEmpPhone(rs.getString("emp_phone"));
                emp.setEmpPwd(rs.getString("emp_pwd"));
                emp.setEmpSalt(rs.getString("emp_salt"));
                emp.setEmpEmail(rs.getString("emp_email"));
                emp.setEmpPost(rs.getString("emp_post"));
                emp.setEmpSex(Gender.fromInt(1));
                emp.setHeadPhoto(rs.getString("head_photo"));
                emp.setStatus(EmpStatus.fromInt(rs.getInt("status")));
                emp.setSuperior(rs.getInt("superior"));
                emp.setCreateTime(rs.getDate("create_time"));
                emp.setUpdateTime(rs.getDate("update_time"));
                emp.setLastLoginIp(rs.getString("last_login_ip"));
                emp.setLastLoginTime(rs.getDate("last_login_time"));
            }
            return emp;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

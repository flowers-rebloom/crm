package com.scgrts.crm.dao.impl;

import com.scgrts.crm.dao.BusinessDao;
import com.scgrts.crm.model.dto.BusinessDTO;
import com.scgrts.crm.model.dto.ForSelect;
import com.scgrts.crm.model.dto.SalesReportDTO;
import com.scgrts.crm.model.dto.common.QueryParamDTO;
import com.scgrts.crm.model.entity.Business;
import com.scgrts.crm.model.entity.BusinessFollowup;
import com.scgrts.crm.model.entity.Clue;
import com.scgrts.crm.model.enums.BusinessStep;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BusinessDaoImpl implements BusinessDao {
    @Override
    public Integer save(Business entity) {
        String sql = "INSERT INTO `business` (`cust_id`, `business_name`, `business_money`, `business_remark`, `business_current_step`, `business_end_status`, "
                + "`create_emp_id`,`create_time`, `update_time`, `owner_emp_id`, `followup_status`, `last_followup_time`, "
                + "`last_followup_presentation`, `next_followup_time`, `delete_status`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        int save = JdbcTemplate.update(sql, new Object[]{entity.getCustId(), entity.getBusinessName(), entity.getBusinessMoney(),
                entity.getBusinessRemark(), entity.getCurrentStep().getValue(), entity.getEndStatus(),
                entity.getCreateEmpId(), entity.getCreateTime(), entity.getUpdateTime(), entity.getOwnerEmpId(),
                entity.getFollowupStatus(), entity.getLastFollowupTime(), entity.getLastFollowupPresentation(),
                entity.getNextFollowupTime(), entity.getDeleteStatus()});
        int businessId = JdbcTemplate.getAutoIncrementValue();
        entity.setBusinessId(businessId);
        return save;
    }


    @Override
    public Integer findTotal(String q) {
        String sql = "SELECT COUNT(1) FROM " + "business " + "WHERE " + "delete_status = 0 AND business_name LIKE CONCAT('%',?,'%') ";
        ResultSet rs = JdbcTemplate.select(sql, q);
        try {
            rs.next();
            return rs.getInt(1);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Business> findAll(QueryParamDTO queryParamDTO) {
        String sql = "SELECT business.*, cust.cust_name, emp.emp_name owner_emp_name FROM business "
                + "LEFT JOIN cust ON business.cust_id = cust.cust_id LEFT JOIN emp ON business.owner_emp_id = emp.emp_id "
                + "WHERE business.delete_status = 0 AND "
                + "ORDER BY business.business_id DESC LIMIT ?, ?";
        int pageSize = queryParamDTO.getPageSize();
        int pageNo = queryParamDTO.getPageNo();
        int skip = pageSize * (pageNo - 1);
        ResultSet rs = JdbcTemplate.select(sql, skip, pageSize);
        ArrayList<Business> list = new ArrayList<Business>();
        try {
            while (rs.next()) {
                BusinessDTO businessDTO = new BusinessDTO();
                businessDTO.setBusinessId(rs.getInt("business_id"));
                businessDTO.setBusinessName(rs.getString("business_name"));
                businessDTO.setBusinessMoney(rs.getBigDecimal("business_money"));
                businessDTO.setBusinessRemark(rs.getString("business_remark"));
                businessDTO.setCreateEmpId(rs.getInt("create_emp_id"));
                businessDTO.setCreateTime(rs.getDate("create_time"));
                businessDTO.setCurrentStep(BusinessStep.fromInt(rs.getInt("business_current_step")));
                businessDTO.setCustId(rs.getInt("cust_id"));
                businessDTO.setUpdateTime(rs.getDate("update_time"));
                businessDTO.setDeleteStatus(rs.getBoolean("delete_status"));
                businessDTO.setEndStatus(rs.getInt("business_end_status"));
                businessDTO.setFollowupStatus(rs.getBoolean("followup_status"));
                businessDTO.setLastFollowupPresentation(rs.getString("last_followup_presentation"));
                businessDTO.setLastFollowupTime(rs.getTime("last_followup_time"));
                businessDTO.setNextFollowupTime(rs.getDate("next_followup_time"));
                businessDTO.setOwnerEmpName(rs.getString("owner_emp_name"));
                businessDTO.setCustName(rs.getString("cust_name"));
                list.add(businessDTO);
            }
            return list;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public List<Business> findPage(QueryParamDTO queryParamDTO) {
        String sql = "SELECT business.*, cust.cust_name, emp.emp_name owner_emp_name FROM business "
                + "LEFT JOIN cust ON business.cust_id = cust.cust_id LEFT JOIN emp ON business.owner_emp_id = emp.emp_id "
                + "WHERE business.delete_status = 0 AND business.business_name LIKE CONCAT('%', ?, '%') "
                + "ORDER BY business.business_id DESC LIMIT ?, ?";
        String q = queryParamDTO.getQ();
        int pageSize = queryParamDTO.getPageSize();
        int pageNo = queryParamDTO.getPageNo();
        int skip = pageSize * (pageNo - 1);
        ResultSet rs = JdbcTemplate.select(sql, q, skip, pageSize);
        ArrayList<Business> list = new ArrayList<Business>();
        try {
            while (rs.next()) {
                BusinessDTO businessDTO = new BusinessDTO();
                businessDTO.setBusinessId(rs.getInt("business_id"));
                businessDTO.setBusinessName(rs.getString("business_name"));
                businessDTO.setBusinessMoney(rs.getBigDecimal("business_money"));
                businessDTO.setBusinessRemark(rs.getString("business_remark"));
                businessDTO.setCreateEmpId(rs.getInt("create_emp_id"));
                businessDTO.setCreateTime(rs.getDate("create_time"));
                businessDTO.setCurrentStep(BusinessStep.fromInt(rs.getInt("business_current_step")));
                businessDTO.setCustId(rs.getInt("cust_id"));
                businessDTO.setUpdateTime(rs.getDate("update_time"));
                businessDTO.setDeleteStatus(rs.getBoolean("delete_status"));
                businessDTO.setEndStatus(rs.getInt("business_end_status"));
                businessDTO.setFollowupStatus(rs.getBoolean("followup_status"));
                businessDTO.setLastFollowupPresentation(rs.getString("last_followup_presentation"));
                businessDTO.setLastFollowupTime(rs.getTime("last_followup_time"));
                businessDTO.setNextFollowupTime(rs.getDate("next_followup_time"));
                businessDTO.setOwnerEmpName(rs.getString("owner_emp_name"));
                businessDTO.setCustName(rs.getString("cust_name"));
                list.add(businessDTO);
            }
            return list;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public Business findById(Integer id) {
        String sql = "SELECT * FROM business WHERE delete_status = 0 AND business_id= ?";
        ResultSet rs = JdbcTemplate.select(sql, id);
        Business business = new Business();
        try {
            business.setBusinessId(rs.getInt("business_id"));
            business.setCustId(rs.getInt("cust_id"));
            business.setBusinessName(rs.getString("business_name"));
            business.setBusinessMoney(rs.getBigDecimal("business_money"));
            business.setBusinessRemark(rs.getString("business_remark"));
            business.setCreateEmpId(rs.getInt("create_emp_id"));
            business.setCreateTime(rs.getDate("create_time"));
            business.setUpdateTime(rs.getDate("update_time"));
            business.setOwnerEmpId(rs.getInt("owner_emp_id"));
            business.setDeleteStatus(rs.getBoolean("delete_status"));
            business.setFollowupStatus(rs.getBoolean("followup_status"));
            business.setLastFollowupPresentation(rs.getString("last_followup_presentation"));
            business.setLastFollowupTime(rs.getTime("last_followup_time"));
            business.setNextFollowupTime(rs.getDate("next_followup_time"));
            business.setEndStatus(rs.getInt("business_end_status"));
            return business;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public BusinessDTO findDetailsById(Integer id) {
        String sql = "SELECT bu.*,e1.emp_name create_emp_name,e2.emp_name owner_emp_name,cu.cust_name cust_name "
                + "FROM business bu LEFT JOIN emp e1 ON bu.create_emp_id=e1.emp_id LEFT JOIN emp e2 ON bu.owner_emp_id=e2.emp_id"
                + " LEFT JOIN cust cu ON bu.cust_id=cu.cust_id WHERE bu.delete_status = 0 AND business_id=?";
        ResultSet rs = JdbcTemplate.select(sql, id);
        try {
            BusinessDTO businessDTO = new BusinessDTO();
            businessDTO.setBusinessId(rs.getInt("business_id"));
            businessDTO.setBusinessName(rs.getString("business_name"));
            businessDTO.setBusinessMoney(rs.getBigDecimal("business_money"));
            businessDTO.setBusinessRemark(rs.getString("business_remark"));
            businessDTO.setCreateEmpId(rs.getInt("create_emp_id"));
            businessDTO.setCreateEmpName(rs.getString("create_emp_name"));
            businessDTO.setCreateTime(rs.getDate("create_time"));
            businessDTO.setCurrentStep(BusinessStep.fromInt(rs.getInt("business_current_step")));
            businessDTO.setCustId(rs.getInt("cust_id"));
            businessDTO.setUpdateTime(rs.getDate("update_time"));
            businessDTO.setDeleteStatus(rs.getBoolean("delete_status"));
            businessDTO.setEndStatus(rs.getInt("business_end_status"));
            businessDTO.setFollowupStatus(rs.getBoolean("followup_status"));
            businessDTO.setLastFollowupPresentation(rs.getString("last_followup_presentation"));
            businessDTO.setLastFollowupTime(rs.getTime("last_followup_time"));
            businessDTO.setNextFollowupTime(rs.getDate("next_followup_time"));
            businessDTO.setOwnerEmpName(rs.getString("owner_emp_name"));
            businessDTO.setCustName(rs.getString("cust_name"));
            return businessDTO;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Integer update(Business entity) {
        String sql = "UPDATE `gcrm`.`business` SET `cust_id` = ?, `business_name` = ?, `business_money` = ?, `business_remark` = ?, `business_current_step` = ?, "
                + "`business_end_status` = ?, `update_time` = ?, `owner_emp_id` = ?, `followup_status` = ?, `last_followup_time` = ?, "
                + "`last_followup_presentation` = ?, `next_followup_time` = ?, `delete_status` = ? WHERE `business_id` = ?";
        int update = JdbcTemplate.update(sql, entity.getCustId(), entity.getBusinessName(), entity.getBusinessMoney(),
                entity.getBusinessRemark(), entity.getCurrentStep().getValue(), entity.getEndStatus(),
                entity.getUpdateTime(), entity.getOwnerEmpId(), entity.getFollowupStatus(),
                entity.getLastFollowupTime(), entity.getLastFollowupPresentation(), entity.getNextFollowupTime(),
                entity.getDeleteStatus(), entity.getBusinessId());
        return update;
    }

    @Override
    public Integer delete(Integer id) {
        //逻辑删除
        String sql = "UPDATE business SET delete_status = 1 WHERE business_id = ?";
        int delete = JdbcTemplate.update(sql, id);
        return delete;
    }

    @Override
    public Integer batchDelete(Integer[] id) {
        Integer delete = 0;
        for (int oneId : id) {
            delete = delete(oneId);
        }
        return delete;
    }

    @Override
    public List<Business> findByCustId(Integer custid) {
        String sql = "SELECT * from business where cust_id=?";
        ResultSet rs = JdbcTemplate.select(sql, custid);
        ArrayList<Business> list = new ArrayList<Business>();
        try {
            while (rs.next()) {
                Business business = new Business();
                business.setBusinessId(rs.getInt("business_id"));
                business.setCustId(rs.getInt("cust_id"));
                business.setBusinessName(rs.getString("business_name"));
                business.setBusinessMoney(rs.getBigDecimal("business_money"));
                business.setBusinessRemark(rs.getString("business_remark"));
                business.setCreateEmpId(rs.getInt("create_emp_id"));
                business.setCreateTime(rs.getDate("create_time"));
                business.setUpdateTime(rs.getDate("update_time"));
                business.setOwnerEmpId(rs.getInt("owner_emp_id"));
                business.setDeleteStatus(rs.getBoolean("delete_status"));
                business.setFollowupStatus(rs.getBoolean("followup_status"));
                business.setLastFollowupPresentation(rs.getString("last_followup_presentation"));
                business.setLastFollowupTime(rs.getTime("last_followup_time"));
                business.setNextFollowupTime(rs.getDate("next_followup_time"));
                business.setEndStatus(rs.getInt("business_end_status"));
                list.add(business);
            }
            return list;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<ForSelect> businessForSelect(String q) {
        String sql = "SELECT business_id,business_name FROM business WHERE business_name LIKE CONCAT('%', ?, '%')";
        ResultSet rs = JdbcTemplate.select(sql, q);
        ArrayList<ForSelect> businessNames = new ArrayList<>();
        try {
            while (rs.next()) {
                businessNames.add(new ForSelect(rs.getInt("business_id"), rs.getString("business_name")));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return businessNames;
    }

    public List<Integer> findtest() {
        String q = "SELECT business_id FROM business WHERE business_current_step=3 ORDER BY RAND( ) LIMIT 80";
        ResultSet rs = JdbcTemplate.select(q);
        ArrayList<Integer> list = new ArrayList<Integer>();
        try {
            while (rs.next()) {
                list.add(rs.getInt("business_id"));
            }
            return list;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void upeded(Integer id) {
        String q = "UPDATE business SET business_end_status = 1 WHERE business_id=?";
        int update = JdbcTemplate.update(q, id);
    }
//    @Override
//    public SalesReportDTO countSalesReportPersonal(String username) {
//        return null;
//    }
//
//    @Override
//    public SalesReportDTO countSalesReportOfDept(String username) {
//        return null;
//    }

}

package com.scgrts.crm.dao;

import com.scgrts.crm.model.dto.MoneyTopItem;

import java.util.Date;
import java.util.List;

public interface StatisticsDao {
    List<MoneyTopItem> moneyStatistics(Date dateBegin, Date dateEnd);
}

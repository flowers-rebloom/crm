package com.scgrts.crm.dao;

/**
 * 变更指定实体的负责人
 */
public interface UpdateNameDao {
    /**
     * 变更指定实体的负责人
     *
     * @param entityId 实体id，它可能是custId/bisinessId/contractId
     * @param empId    负责人id
     * @return
     */
    boolean setOwner(Integer entityId, Integer empId);
}

// 获取地址栏上的id
const usp = new URLSearchParams(window.location.search);
const businessId = usp.get("id");

/**
 * 获得后端的数据/baseInf(商机基本信息)/followups(商机的跟进记录)/contacts(商机的联系人列表)
 * /emplist(商机的变更负责人选择员工select数据)
 */
function fetchData() {
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/business/details",
        data: {businessId},
        dataType: "json",
        success: function (response) {
            //调用显示商机基本信息的方法
            showDataBaseInfo(response.data.baseInfo);
            //调用显示跟进记录的方法
            if (response.data.followups != null) {
                showFollowupHistory(response.data.followups);
            }
            //调用显示联系人列表的方法
            if (response.data.contacts != null) {
                showContacts(response.data.contacts);
            }
            //调用显示变更负责人的selectde的option的方法
            showOwnerSelectOption(response.data.empList);
            //调用显示商机现阶段的方法
            currentStep(response.data.baseInfo.currentStep);
            //调用显示写跟进的联系人select的option的方法
            if (response.data.contacts != null) {
                contactSelectOption(response.data.contacts);
            }
        },
    });
}

/**
 *
 * @param {显示点击变更负责人后 员工选择框的数据} emps
 */
function showOwnerSelectOption(emps) {
    for (const emp of emps) {
        $("#changeOwnerSelect").append(`<option value=${emp.empId}>${emp.empName}</option>`);
    }
}

/**
 *
 * @param {显示商机的基本信息:custName, businessName,ownerEmpName,createTime,businessMoney,nextFollowupTime
 * nextFollowupTime,businessRemark} BaseInfo
 */
function showDataBaseInfo(BaseInfo) {
    $("#custName").html(BaseInfo.custName);
    $("#ownerEmpName").html(BaseInfo.ownerEmpName);
    $("#businessName").html(BaseInfo.businessName);
    $("#createTime").html(BaseInfo.createTime);
    $("#businessMoney").html(BaseInfo.businessMoney + "元");
    $("#nextFollowupTime").html(BaseInfo.nextFollowupTime);
    $("businessRemark").html(BaseInfo.businessRemark);
}

/**
 *
 * @param {显示跟进记录} businessFollowups
 */
function showFollowupHistory(businessFollowups) {
    var followupsTbody = $("#followupHistory");
    followupsTbody.empty();
    for (const followup of businessFollowups) {
        var html = `
        <tr>
        <td>${followup.followupTime}</td>
        <td>${followup.contactName}</td>
        <td>${followup.businessFollowupPresentation}</td>
        <td>${followup.followupMethod}</td>
    </tr>
        `;
        followupsTbody.append(html);
    }
}

/**
 *
 * @param {显示联系人列表} businessConatact
 */
function showContacts(businessConatact) {
    var contactsTBody = $("#contacts");
    contactsTBody.empty();
    for (const contact of businessConatact) {
        var html = `
        <tr>
        <td>${contact.contactName}</td>
        <td>${contact.contactPost}</td>
        <td>${contact.contactPhone}</td>
        <td>${contact.contactGender}</td>
        <td>${contact.contactEmail}</td>
        <td>${contact.isDecisionMaker}</td>
    </tr>
        `;
        contactsTBody.append(html);
    }
}

/**
 * 联系人选择框的option 匹配
 */
function contactSelectOption(contacts) {
    for (const contact of contacts) {
        //防止出现同一个联系人的选项
        if (contact.contactName == $(`option:contains(${contact.contactName})`).html()) {
            return;
        }
        $("#contactSelect").append(`<option value=${contact.contactId}>${contact.contactName}</option>`);
    }
}

/**
 * 保存新的跟进
 */
function saveFollowup() {
    var followupTime = $("#followupTime").val();
    var followupPresentation = $("#followupPresentation").val();
    var followupMethod = $("#followupMethod").val();
    var contactId = $("#contactSelect").val();

    var followup = {
        followupTime,
        followupPresentation,
        followupMethod,
        contactId,
        businessId,
    };

    $.ajax({
        type: "POST",
        url: "http://localhost:8080/business/followup",
        data: followup,
        dataType: "dataType",
        success: function (response) {
            if (response.code === 0) {
                window.alert("保存跟进成功！");
            }
        },
    });
}

/**
 * 跳转到编辑界面
 */
function turnToEdit() {
    window.location.href = "business-edit.html?id=" + businessId;
}

/**
 *
 * 删除此商机
 */
function deleteBusiness() {
    if (!confirm("确定要删除吗？")) return;
    $.ajax({
        type: "POST",

        url: "http://localhost:8080/business/delete",

        data: {businessId},

        dataType: "dataType",
        success: function (response) {
            if (response.code === 0) {
                window.alert("删除成功！");
            }
        },
    });
}

/**
 * 变更负责人 发送请求 传入参数:contactId,businessId
 */
function changOwner() {
    var empId = $("#changeOwnerSelect").val();
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/business/setOwner",
        data: {empId, businessId},
        dataType: "dataType",
        success: function (response) {
            if (response.code === 0) {
                window.alert("更换负责人成功！");
            }
        },
    });
    $("#changeOwnerSelect").hide();
}

/**
 * 显示商机阶段
 */
function currentStep(currentStep) {
    var yzkh = $("#yzkh");
    var xqfx = $("#xqfx");
    var fabj = $("#fabj");
    var end = $("#end");
    var values = [yzkh, xqfx, fabj, end];
    for (let i = 0; i < values.length; i++) {
        if (currentStep == i) {
            for (let j = 0; j <= i; j++) {
                values[j].css("background-color", "yellow");
            }
        }
    }
}

/**
 * 更改商机状态为验证客户！
 */
function YanZhengKeHu() {
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/business/YZKH",
        data: {businessId},

        success: function (response) {
            if (response.code === 0) {
                window.alert("更改商机状态为验证客户！");
            }
        },
    });
}

function XuQiuFenXi() {
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/business/XQFX",
        data: {businessId},
        success: function (response) {
            if (response.code === 0) {
                window.alert("更改商机状态为需求分析！");
            }
        },
    });
}

function FangAnBaoJia() {
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/business/FABJ",
        data: {businessId},
        success: function (response) {
            if (response.code === 0) {
                window.alert("更改商机状态为方案报价！");
            }
        },
    });
}

function End() {
    $.ajax({
        type: "POST",
        url: "http://localhost:8080/business/END",
        data: {businessId},
        success: function (response) {
            if (response.code === 0) {
                window.alert("更改商机状态为结束！");
            }
        },
    });
}

/**
 * 窗口加载时调用此函数
 */
$(fetchData);
/**
 * 点击变更负责人按钮时显示员工选择框
 */
$("#changeOwnerButton").on("click", function () {
    $("#changeOwnerSelect").show();
});
/**
 * 点击商机的状态按钮时，调用对应的方法
 */
$("#yzkh").on("click", function () {
    YanZhengKeHu();
});
$("#xqfx").on("click", function () {
    XuQiuFenXi();
});
$("#fabj").on("click", function () {
    FangAnBaoJia();
});
$("#end").on("click", function () {
    End();
});

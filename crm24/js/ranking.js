servPara.method = "GET"
servPara.url = "http://localhost:8080/rank"


function clueUp() {
    var key = "clueUp"
    servPara.data = {key}
    var response = askServer(servPara, "")
    showData(response)
    altRow()
}

function clueDown() {
    var key = "clueDown"
    servPara.data = {key}
    var response = askServer(servPara, "")
    showData(response)
    altRow()
}

function contactUp() {
    var key = "contactUp"
    servPara.data = {key}
    var response = askServer(servPara, "")
    showData(response)
    altRow()
}

function contactDown() {
    var key = "contactDown"
    servPara.data = {key}
    var response = askServer(servPara, "")
    showData(response)
    altRow()
}

function custUp() {
    var key = "custUp"
    servPara.data = {key}
    var response = askServer(servPara, "")
    showData(response)
    altRow()
}

function custDown() {
    var key = "custDown"
    servPara.data = {key}
    var response = askServer(servPara, "")
    showData(response)
    altRow()
}

function businessUp() {
    var key = "businessUp"
    servPara.data = {key}
    var response = askServer(servPara, "")
    showData(response)
    altRow()
}

function businessDown() {
    var key = "businessDown"
    servPara.data = {key}
    var response = askServer(servPara, "")
    showData(response)
    altRow()
}

function contractUp() {
    var key = "contractUp"
    servPara.data = {key}
    var response = askServer(servPara, "")
    showData(response)
    altRow()
}

function contractDown() {
    var key = "contractDown"
    servPara.data = {key}
    var response = askServer(servPara, "")
    showData(response)
    altRow()
}

function paymentUp() {
    var key = "paymentUp"
    servPara.data = {key}
    var response = askServer(servPara, "")
    showData(response)
    altRow()
}

function paymentDown() {
    var key = "paymentDown"
    servPara.data = {key}
    var response = askServer(servPara, "")
    showData(response)
    altRow()
}

function showData(response) {
    $("tb").empty()
    let html = '';
    for (const rank of response.data) {
        html += `
                      <tr> 
                            <td>${rank.empName}</td>
                            <td>${rank.clues}</td>
                            <td>${rank.contacts}</td>
                            <td>${rank.custs}</td>
                            <td>${rank.businesss}</td>
                            <td>${rank.contracts}</td>
                            <td>${rank.payments}</td>
                      </tr> `
        $("#tb").html(html)
    }
    ;
}

//换行颜色
function altRow() {

    var table = document.getElementById("tb");
    var rows = table.getElementsByTagName("tr");
    for (i = 0; i < rows.length; i++) {
        if (i % 2 == 0) {
            rows[i].className = "evenrowcolor";
        } else {
            rows[i].className = "oddrowcolor";
        }
    }
}


window.onload = function () {
    clueUp()
    altRow()
}
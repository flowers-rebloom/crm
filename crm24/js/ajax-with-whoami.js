const whoami = JSON.parse(sessionStorage.getItem("whoami"));
if (whoami) {
    jQuery.ajaxSetup({
        beforeSend: function (jqxhr, settings) {
            jqxhr.setRequestHeader("whoami", whoami.empCode);
        },
    });
}

/**
 * 渲染分页区的数字按钮
 * @param {number} total a integer
 * @param {number} pageSize a integer
 * @param {string} containerSelector 数字按钮容器的选择器
 * @param {(pageNo: number) => void} callback 点击数字按钮时要执行的回调函数
 */
function render(total, pageSize, pageNo, containerSelector, callback) {
    // 最大页码（总页数）
    const maxPageNo = Math.ceil(total / pageSize);
    // 容器
    const container = document.querySelector(containerSelector);
    // 清空原来的数字按钮
    container.innerHTML = "";
    // 计算
    const numbers = [];
    for (let i = 1; i <= maxPageNo; i++) {
        numbers.push(i);
    }
    const segment = Math.floor((pageNo - 1) / 7);
    const numbersToRender = numbers.slice(segment * 7, segment * 7 + 7);
    // 左箭头
    const prevButton = document.createElement("button");
    prevButton.innerText = "<";
    prevButton.onclick = function () {
        if (pageNo > 1) callback(pageNo - 1);
    };
    container.appendChild(prevButton);
    // 重新渲染数字按钮
    for (const i of numbersToRender) {
        // 生成一个按钮，里面的数字是i
        const button = document.createElement("button");
        button.type = "button";
        button.innerText = i;
        // 高亮显示当前页码
        if (i === pageNo) {
            button.className = "active";
        }
        if (i !== pageNo) {
            // 处理点击事件
            button.onclick = function () {
                callback(i);
            };
        }
        // 追加到容器中
        container.appendChild(button);
    }
    // 右箭头
    const nextButton = document.createElement("button");
    nextButton.innerText = ">";
    nextButton.onclick = function () {
        if (pageNo < maxPageNo) callback(pageNo + 1);
    };
    container.appendChild(nextButton);
}

// 获得id
var usp = new URLSearchParams(window.location.search);
const id = usp.get("id");

/**
 * 从后端服务器查询回款数据
 */
$(fetchData);

function fetchData() {
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/payment/edit",
        data: {id},
        dataType: "json",
        success: (response) => {
            showData(response.data);
        },
    });
}

/**
 * 显示当前回款数据
 */
function showData(payment) {
    var form = document.forms[0];
    form.contractId.innerHTML = `<option value="${payment.contractId}">${payment.contractName}</option>`;
    form.custId.innerHTML = `<option value="${payment.custId}">${payment.custName}</option>`;
    form.paymentTime.value = payment.paymentTime;
    form.paymentType.value = payment.paymentType;
    form.paymentMoney.value = payment.paymentMoney;
    form.ownerEmpId.innerHTML = `<option value="${payment.ownerEmpId}">${payment.ownerEmpName}</option>`;
    form.paymentRemark.value = payment.paymentRemark;
}


/**
 * 保存
 */
function save() {
    //得到表单元素的值
    var form = document.forms[0];
    var contractId = form.contractId.value.trim();
    var custId = form.custId.value.trim();
    var paymentTime = form.paymentTime.value.trim();
    var paymentType = form.paymentType.value.trim();
    var paymentMoney = form.paymentMoney.value.trim();
    var ownerEmpId = form.ownerEmpId.value.trim();
    var paymentRemark = form.paymentRemark.value.trim();

    //封装成对象
    const payment = {
        contractId,
        custId,
        paymentTime,
        paymentType,
        paymentMoney,
        ownerEmpId,
        paymentRemark,
        paymentId: id,
    };

    //验证
    var valid = validateForm(payment);
    if (!valid) return;

    //发送到后端服务器
    $("#empsTbody option:selected").val();
    $.ajax({
        method: "POST",
        url: "http://localhost:8080/payment/update",
        data: payment,
        dataType: "json",
        success: (response) => {
            if (response.code === 0) {
                window.alert("更新成功");
                //后退
                history.back();
            } else {
                window.alert("更新失败: " + response.msg);
            }
        },
    });

}

/**
 * 验证表单
 */
function validateForm(payment) {
    if (payment.contractId === "") {
        window.alert("请输入合同名称");
        return false;
    }
    return true;
}
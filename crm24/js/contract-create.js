function setFormValues() {
    $("#contractCode").val("2104261436");
    $("#contractName").val("百万富翁计划");
    $("#custId").val(1);
    $("#businessId").val(1);
    $("#contractMoney").val("23452234.00");
    $("#contractStartTime").val("2021-03-02");
    $("#contactId").val(1);
    $("#empId").val(1);
    $("#contractRemark").val("暂时没有");
}

function save() {
    var contractCode = $("#contractCode").val();
    var contractName = $("#contractName").val();
    var custId = document.getElementById("custId").value;
    var businessId = document.getElementById("businessId").value;
    var contractMoney = $("#contractMoney").val();
    var contractStartTime = $("#contractStartTime").val();
    var contactId = document.getElementById("contactId").value;
    var empId = document.getElementById("empId").value;
    var contractRemark = $("#contractRemark").val();
    var contract = {
        contractCode,
        contractName,
        custId,
        businessId,
        contractMoney,
        contractStartTime,
        contactId,
        empId,
        contractRemark,
    };

    var valid = validateForm(contract);
    if (!valid) {
        return;
    }
    ;

    jQuery.ajax({
        method: "POST",
        url: "http://localhost:8080/contract/create?whoami=" +
            sessionStorage.getItem("whoami"),
        data: contract,
        dataType: "json",
        success: (response) => {
            if (response.code === 0) {
                window.alert("合同新建成功！");
            } else {
                window.alert("合同新建失败！");
            }
        },
    });
}

$("#custId").on("change", function () {
    var custid = $("#custId").val();
    $.ajax({
        type: "get",
        url: "http://localhost:8080/contract/business",
        data: {custid},
        dataType: "json",
        success: function (response) {
            if (response.code === 0) {
                showBusiness(response.data);
            }
        },
    });
});

function showBusiness(data) {
    $("#businessId").empty();
    for (const business of data) {
        var businessHtml = `<option value="${business.businessId}">${business.businessName}</option>`;
        $("#businessId").append(businessHtml);
    }
    $("#businessId").trigger("change");
}

function validateForm(contract) {
    if (contract.contractCode === "") {
        window.alert("请输入合同编号");
        return false;
    }
    return true;
}
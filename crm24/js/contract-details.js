//详情
const usp = new URLSearchParams(window.location.search);
var id = usp.get("id");

servPara.method = "GET";
servPara.url = "http://localhost:8080/contract/details";
servPara.data = {id};

function showDetails() {
    var response = askServer(servPara, "");
    var contractDTO = response.data.contractDTO;
    showBase(contractDTO);
    var contacts = response.data.contacts;
    showContact(contacts);
}

function showBase(contractDTO) {
    $("#contractCode").append(`${contractDTO.contractCode}`);
    $("#contractName").append(`${contractDTO.contractName}`);
    $("#custName").append(`${contractDTO.custName}`);
    $("#owner").append(
        `${contractDTO.ownerEmpName}` +
        `
	<div id="app" style="display:inline-block;margin-left:20px;">
		<!-- Form -->
		<el-button type="text" @click="dialogFormVisible = true">更改</el-button>
		<el-dialog title="更改负责人" :visible.sync="dialogFormVisible">
			<el-form>
				<el-form-item label="公司职员" :label-width="formLabelWidth">
					<el-select id="val" v-model="value" placeholder="请选择">
						<el-option v-for="item in options" :key="item.value" :label="item.label"
							:value="item.value">
						</el-option>
					</el-select>
				</el-form-item>
			</el-form>
			<div slot="footer" class="dialog-footer">
				<el-button id="false" @click="dialogFormVisible = false">取 消</el-button>
				<el-button type="primary" id="true" @click="dialogFormVisible = false">确 定</el-button>
			</div>
		</el-dialog>
	</div>
</div>`
    );
    $("#businessName").append(`${contractDTO.businessName ? contractDTO.businessName : "无关联商机"}`);
    $("#signDate").append(`${contractDTO.contractStartTime}`);
    $("#money").append(`${contractDTO.contractMoney}`);
    if (contractDTO.paymentStatus == true) {
        $("#paymentStatus").append("已完毕");
    } else {
        $("#paymentStatus").append("未完毕");
    }
    $("#custSigner").append(`${contractDTO.contactName}`);
    $("#empSigner").append(`${contractDTO.companyEmpName}`);
    $("#remark").append(`${contractDTO.contractRemark}`);
}

function showContact(contacts) {
    for (const contact of contacts) {
        let html = `<td>${contact.contactName}</td>` + `<td>${contact.contactPost}</td>` + `<td>${contact.contactPhone}</td>`;
        if (contact.sex == 0) {
            html += `<td>女</td>`;
        } else {
            html += `<td>男</td>`;
        }
        html += `<td>${contact.contactEmail}</td>`;
        if (contact.isDecisionMaker == 0) {
            html += `<td>否</td>`;
        } else {
            html += `<td>是</td>`;
        }
        $("#contact").html(html);
    }
}

window.onload = showDetails();

//编辑
function toEdit() {
    servPara.url = "http://localhost:8080/";
    window.location.href = `contract-edit.html?id=${id}`;
}

//删除
function del() {
    servPara.url = "http://localhost:8080//contract/deleteDetails";
    var bool = confirm("请确认是否删除");
    if (bool == true) {
        askServer(servPara, "已删除");
    }
}

//改变回款状态
function paied() {
    servPara.url = "http://localhost:8080/contract/paied";
    var bool = confirm("请确认是否更改回款状态");
    if (bool == true) {
        askServer(servPara, "已改变回款状态为“回款完毕”");
    }
}

function paying() {
    servPara.url = "http://localhost:8080/contract/paying";
    var bool = confirm("请确认是否更改回款状态");
    if (bool == true) {
        askServer(servPara, "已改变回款状态为“回款中”");
    }
}

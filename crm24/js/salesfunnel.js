function statistics() {
    const dateBegin = $("#dateBegin").val();
    const dateEnd = $("#dateEnd").val();
    jQuery
        .ajax({
            method: "GET",
            url: "http://localhost:8080/salesfunnel",
            data: {dateBegin, dateEnd},
            dataType: "json",
        })
        .done((response) => {
            const list = response.data.reverse();
            const name = list.map((item) => item.name);
            const value = list.map((item) => item.value);

            drawCharts(name, value);
        })
}

function drawCharts(name, value) {
    var chartDom = document.getElementById("main");
    var myChart = echarts.init(chartDom);
    var option;
    option = {
        title: {
            text: '商机漏斗图',
            subtext: '纯属虚构'
        },
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c}%"
        },
        toolbox: {
            feature: {
                dataView: {readOnly: false},
                restore: {},
                saveAsImage: {}
            }
        },
        legend: { //图例
            data: ['验证客户', '需求分析', '方案报价', '赢单']
        },

        series: [
            {
                name: '漏斗图',
                type: 'funnel',
                left: '10%',
                top: 60,
                //x2: 80,
                bottom: 60,
                width: '80%',
                // height: {totalHeight} - y - y2,
                min: 0,
                max: 100,
                minSize: '0%',
                maxSize: '100%',
                sort: 'descending',
                gap: 2,
                label: {
                    show: true,
                    position: 'inside'
                },
                labelLine: {
                    length: 10,
                    lineStyle: {
                        width: 1,
                        type: 'solid'
                    }
                },
                itemStyle: {
                    borderColor: '#fff',
                    borderWidth: 1
                },
                emphasis: {
                    label: {
                        fontSize: 20
                    }
                },

                data: [
                    {value: value[0], name: name[0]},
                    {value: value[1], name: name[1]},
                    {value: value[2], name: name[2]},
                    {value: value[3], name: name[3]}
                ]
            }
        ]
    };
    option && myChart.setOption(option);
}
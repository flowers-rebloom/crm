//  封装save函数
function save() {
    // 获得各个表单元素的值
    var clientName = $("#clientName").val();
    var clientResource = $("#clientResource").val();
    var tele = $("#tele").val();
    var webNet = $("#webNet").val();
    var provience = $("#provience").val();
    var city = $("#city").val();
    var area = $("#area").val();
    var address = $("#address").val();
    var followupstate = document.forms[0].followupstate.value;
    var followupTime = $("#followupTime").val();
    var followupContent = $("#note").val();
    var nextTime = $("#nextTime").val();
    var remark = $("#remark").val();
    // 用一个对象封装值
    var client = {
        clientName,
        clientResource,
        tele,
        webNet,
        provience,
        city,
        area,
        address,
        followupstate,
        followupTime,
        followupContent,
        nextTime,
        remark,
    };
    // 检验对象内元素的有效性
    var valid = validity(client);
    if (!valid) return;
    $.ajax({
        type: "post",
        url:
            "http://localhost:8080/cust/create",
        data: client,
        dataType: "json",
        success: function (response) {
            if (response.code === 0) {
                window.alert("保存成功");
                window.location = "cust-list.html";
            }
        },
    });
}

// 封装测试有效性的函数
function validity(client) {
    if (client.clientName === "") {
        window.alert("请填入客户名称");
        return false;
    }
    return true;
}

// 跟进与未跟进显示与隐藏功能
function hidenfollowupTimeandContents() {
    document.getElementById("followupTimeandContent").style.display = "none";
}

function showfollowupTimeandContents() {
    document.getElementById("followupTimeandContent").style.display = "block";
}

// 自动显示省份
$(AutoShowData);

function AutoShowData() {
    $.ajax({
        type: "get",
        url: "http://localhost:8080/cust/area",
        data: {parentid: 0},
        dataType: "json",
        success: function (response) {
            if (response.code === 0) {
                showProvince(response.data);
            }
        },
    });
}

// 处理省市区的change事件
$("#provience").on("change", function () {
    var parentid = $("#provience").val();
    $.ajax({
        type: "get",
        url: "http://localhost:8080/cust/area",
        data: {parentid},
        dataType: "json",
        success: function (response) {
            if (response.code === 0) {
                showCity(response.data);
            }
        },
    });
});
$("#city").on("change", function () {
    var parentid = $("#city").val();
    $.ajax({
        type: "get",
        url: "http://localhost:8080/cust/area",
        data: {parentid},
        dataType: "json",
        success: function (response) {
            if (response.code === 0) {
                showDistrict(response.data);
            }
        },
    });
});

// 显示省
function showProvince(data) {
    for (const province of data) {
        var provinceHtml = `<option value="${province.areaId}">${province.areaName}</option>`;
        $("#provience").append(provinceHtml);
    }
    $("#provience").trigger("change");
}

//显示市
function showCity(data) {
    $("#city").empty();
    for (const city of data) {
        var cityHtml = `<option value="${city.areaId}">${city.areaName}</option>`;
        $("#city").append(cityHtml);
    }
    $("#city").trigger("change");
}

// 显示区
function showDistrict(data) {
    $("#area").empty();
    for (const district of data) {
        var districtHtml = `<option value="${district.areaId}">${district.areaName}</option>`;
        $("#area").append(districtHtml);
    }
}

const usp = new URLSearchParams(window.location.search);
const id = usp.get("id");

//编辑
function toEdit() {
    window.location.href = "contact-edit.html?id=" + id;
}

//删除
function del() {
    if (!confirm("危险！删除了数据不可恢复！确定要删除吗？")) return;
    $.ajax({
        method: "POST",
        url: "http://localhost:8080/contact/delete",
        data: {id},
        dataType: "json",
        success: function (response) {
            if (response.code === 0) {
                alert("已成功删除联系人");
            }
        },
    });
}

function fetchData() {
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/contact/details",
        data: {id},
        success: (response) => {
            showContactBaseInfo(response.data.baseInfo);
            showFollowupHistory(response.data.followups);
            showOwnerSelectOption(response.data.empList);
        },
    });
}

function showOwnerSelectOption(emps) {
    for (const emp of emps) {
        $("#changeOwnerSelect").append(`<option value=${emp.empId}>${emp.empName}</option>`);
    }
}

function showContactBaseInfo(contactBaseInfo) {
    $("#contactName").html(contactBaseInfo.contactName);
    $("#contactPost").html(contactBaseInfo.contactPost);
    $("#contactSex").html(contactBaseInfo.contactSex);
    $("#contactEmail").html(contactBaseInfo.contactEmail);
    $("#contactAddress").html(contactBaseInfo.contactAddress);
    $("#contactPhone").html(contactBaseInfo.contactPhone);
}

function showFollowupHistory(followupHistory) {
    var tbody = $("#followupTbody");
    tbody.empty();
    for (const item of followupHistory) {
        var html = `
        <tr>
            <td>${item.time}</td>
            <td>${item.way}</td>
            <td>${item.content}</td>
        </tr>
        `;
        tbody.append(html);
    }
}

function saveNewFollowup() {
    var followupTime = $("#followupTime").val();
    var contact = $("#contact").val();
    var followupMethod = $("#followupMethod").val();
    var contactFollowupPresentation = $("#contactFollowupPresentation").val();


    const newFollowup = {
        followupTime,
        contact,
        followupMethod,
        contactFollowupPresentation,
        contactId: id,
    };
    jQuery.ajax({
        method: "POST",
        url: "http://localhost:8080/contact/write-followup",
        data: newFollowup,
        dataType: "json",
        success: (response) => {
            if (response.code == 0) {
                alert("新的跟进记录已保存");
            }
        },
    });
}


$(fetchData);

// 获取地址栏上的id
const usp = new URLSearchParams(window.location.search);
const id = usp.get("id");

/**
 * 加载数据
 */
function fetchData() {
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/clue/details",
        data: {id},
        success: (response) => {
            showClueBaseInfo(response.data.baseInfo);
            showFollowupHistory(response.data.followups);
        },
    });
}

/**
 * 显示线索的基本信息
 */
function showClueBaseInfo(clueBaseInfo) {
    // 显示出来
    $("#clueName").html(clueBaseInfo.clueName);
    $("#clueSource").html(clueBaseInfo.clueSource);
    $("#address").html(clueBaseInfo.clueAddress);
    $("#remark").html(clueBaseInfo.clueRemark);
    $("#owner").html(clueBaseInfo.ownerEmpName);
    $("#phone").html(clueBaseInfo.cluePhone);
    $("#nextTime").html(clueBaseInfo.nextFollowupTime);
}

/**
 * 显示历史跟进记录
 */
function showFollowupHistory(followupHistory) {
    var tbody = $("#followupTbody");
    tbody.empty();
    for (const item of followupHistory) {
        var html = `
        <tr>
            <td>${item.time}</td>
            <td>${item.way}</td>
            <td>${item.content}</td>
        </tr>
        `;
        tbody.append(html);
    }
}

/**
 * 保存新的跟进
 */
function saveNewFollowup() {
    // 获取表单元素的值
    var followupTime = $("#followupTime").val();
    var followupMethod = $("#followupMethod").val();
    var clueFollowupPresentation = $("#clueFollowupPresentation").val();
    var nextFollowupTime = $("#nextFollowupTime").val();
    // 封装成对象
    const newFollowup = {
        followupTime,
        followupMethod,
        clueFollowupPresentation,
        nextFollowupTime,
        clueId: id,
    };
    // 请求后端存起来
    jQuery.ajax({
        method: "POST",
        url: "http://localhost:8080/clue/write-followup",
        data: newFollowup,
        dataType: "json",
        success: (response) => {
            if (response.code == 0) {
                alert("新的跟进记录已保存");
            }
        },
    });
}

function toEdit() {
    window.location.href = "clue-edit.html?id=" + id;
}

function changeToBeCust() {
    $.ajax({
        method: "POST",
        url: "http://localhost:8080/clue/change-into-cust",
        data: {id},
        dataType: "json",
        success: function (response) {
            if (response.code === 0) {
                alert("已成功转为客户");
            }
        },
    });
}

function del() {
    $.ajax({
        method: "POST",
        url: "http://localhost:8080/clue/delete",
        data: {id},
        dataType: "json",
        success: function (response) {
            if (response.code === 0) {
                alert("已成功删除线索");
            }
        },
    });
}

$(fetchData);

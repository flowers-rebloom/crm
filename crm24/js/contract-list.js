function fetchData(pageNo) {
    const q = $("#q").val();
    const pageSize = $("select#pageSize").val();
    jQuery.ajax({
        method: "GET",
        url: "http://localhost:8080/contract/list?timeatamp" + new Date().getTime(),
        data: {q, pageSize, pageNo},
        dataType: "json",
        success: (response) => {
            showData(response.data.list);
            $("#total").text(response.data.total);
            // 自动渲染出一堆数字按钮
            render(response.data.total, pageSize, pageNo, ".page-nums", fetchData);
        },
    });
}

function showData(list) {
    $("#tbody").empty();
    for (const contract of list) {
        var T;
        if (contract.paymentStatus === true) {
            T = '<td>已完成回款</td>'
        } else {
            T = '<td>未完成回款</td>'
        }
        const html = `
		<tr>
			<td><input type="checkbox" name="id" value="${contract.contractId}"></td>
			<td><a href="contract-details.html?id=${contract.contractId}">${contract.contractCode}</a></td>
			<td>${contract.contractName}</td>
			<td>${contract.custName}</td>
			<td>${contract.contractMoney}</td>
            <td>${contract.contractStartTime}</td>
			${T}
		</tr>
		`;
        $("#tbody").append(html);
    }
}


$("#btnSearch").on("click", function () {
    fetchData(1);
})


function toEdit() {
    // 取得选中的行的contractId
    var checkboxes = document.querySelectorAll("input[type=checkbox][name=id]");
    var checkedId;
    for (const checkbox of checkboxes) {
        if (checkbox.checked) {
            checkedId = checkbox.value;
            break;
        }
    }
    // 跳转到contract-edit.html
    window.location.href = "contract-edit.html?id=" + checkedId;
}

function batchDelete() {
    if (!confirm("警告！删除操作不可恢复！确定要删除吗？")) return;
    // 获取被选中的多个id
    var checkboxes = document.querySelectorAll("input[type=checkbox][name=id]");
    var checkedId = [];
    for (const checkbox of checkboxes) {
        if (checkbox.checked) {
            checkedId.push(checkbox.value);
        }
    }
    // 请求后端服务器删除
    jQuery.ajax({
        method: "POST",
        url: "http://localhost:8080/contract/batch-delete",
        data: {id: checkedId},
        success: (response) => {
            if (response.code === 0) fetchData();
        },
    });
}

function pageSizeChangeHandler() {
    fetchData(1);
}

fetchData(1);
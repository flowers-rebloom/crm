//保存click按钮处理事件函数
$("#btnSave").on(
    "click",
    function () {
        //获取表单元素的值
        var businessName = $("#businessName").val();
        var custId = document.getElementById("custId").value();
        var businessMoney = $("#businessMoney").val();
        var businessRemark = $("#businessRemark").val();
        var followupStatus = document.forms[0].followupStatus.value;
        var lastFollowupTime = $("#lastFollowupTime").val();
        var lastFollowupPresentation = $("#lastFollowupPresentation").val();
        var nextFollowupTime = $("#nextFollowupTime").val();

        //封装成关于商机的实体对象
        var business = {
            businessName,
            custId,
            businessMoney,
            businessRemark,
            followupStatus,
            lastFollowupTime,
            lastFollowupPresentation,
            nextFollowupTime,
        };
        //调用vaildateForm函数进行表单验证
        var valid = validateForm(business);
        if (!valid) {
            return;
        }
        //发起Ajax请求，提交数据给后端
        jQuery.ajax({
            method: "POST",
            url:
                "http://localhost:8080/business/create?whoami=" +
                sessionStorage.getItem("whoami"),
            data: contract,
            dataType: "json",
            success: (response) => {
                if (response.code === 0) {
                    window.alert("商机新建成功！");
                } else {
                    window.alert("商机新建失败！");
                }
            },
        });
    },
    /**
     * 验证表单
     * @param {{businessName,custId,businessMoney,businessRemark,followupStatus,lastFollowupTime,lastFollowupPresentation,nextFollowupTime,}} clue 封装了表单中各个数据项的线索对象
     * @returns {boolean} true=>验证通过，false=>验证失败，可能有的数据项不符合要求
     */
    function validateForm(business) {
        if (business.businessName === "") {
            window.alert("请输入商机名称");
            return false;
        }
        return true;
    }
);

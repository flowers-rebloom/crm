// 建立一个整体对象
var model = {
    total: 0,
    data: [],
};

// 从数据库获取数据
// 利用循环 获取整体对象里data的元素
function fetchData(pageNo) {
    const q = $("#q").val();
    const pageSize = $("#pageSize").val();
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/cust/list",
        data: {q, pageNo, pageSize},
        dataType: "json",
        success: function (response) {
            showData(response.data.list);
            $("#total").text(response.data.total);
            render(response.data.total, pageSize, pageNo, ".page-nums", fetchData)
        }
    });
}

function showData(list) {
    $("#tbody").empty()
    for (const client of list) {
        const clientHtml = `
           <tr>
           <td > <input type="checkbox" name="id" value="${client.custId}" ></td>
           <td>
               <a href="cust-details.html?id=${client.custId}"> ${client.custName}</a>
           </td>
           <td>${client.custPhone}</td>
           <td>${client.custAddress}</td>
           <td>${client.nextFollowupTime}</td>
           <td>${client.ownerEmpName}</td>
           <td>${client.dealStatus ? "已成交" : "未成交"}</td>
       </tr>
       `;
        //  $("#tbody").text(clientHtml)
        document.getElementById("tbody").innerHTML += clientHtml;
    }
}

// 选择每页行数
function pageSizeChangeHandler() {
    fetchData(1);
}

// 页面加载
fetchData(1)

// 编辑按钮的事件处理
function TOEdit() {
    // 获取被选中复选框的id值
    var checkboxes = document.querySelectorAll("input[type=checkbox][name=id]");
    var checkedId;
    for (const checkbox of checkboxes) {
        if (checkbox.checked) {
            checkedId = checkbox.value;
            break;
        }
    }
    window.location.href = "cust-edit.html?id=" + checkedId;
}

// 批量删除线索的事件处理
function bantchdelete() {
    if (!confirm("请谨慎操作！数据删除不可恢复，你确定要删除迈？")) return;
    var allboxes = document.querySelectorAll("input[type=checkbox][name=id]");
    var allid = [];
    var oneofcheckedId;
    for (const box of allboxes) {
        if (box.checked) {
            oneofcheckedId = box.value;
            allid.push(oneofcheckedId);
        }

    }
    $.ajax({
        type: "post",
        url: "http://localhost:8080/cust/batchdelete",
        data: {oneofcheckedId: allid},
        traditional: true,
        dataType: "json",
        success: function (response) {
            if (response.code === 0) fetchData();
        }
    });
}

//  选择按钮的控制
function checkAll() {
    // 利用jquery 设置按钮合集的checked属性 等于主按钮的checked属性
    $("input[name=id]").prop("checked", $("#mainCheck").prop("checked"))
}

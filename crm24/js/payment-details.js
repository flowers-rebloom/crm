//获取id
const usp = new URLSearchParams(window.location.search);
const id = usp.get("id");

//回款基本信息
//加载数据
function fetchData() {
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/payment/details",
        data: {id},
        detaType: "json",
        success: (response) => {
            showPaymentBaseInfo(response.data.PaymentBaseInfo);
            showChangeOwner(response.data.emps);
        },
    });
}

//显示回款基本信息
function showPaymentBaseInfo(PaymentBaseInfo) {
    //显示出来
    $("#paymentId").html(PaymentBaseInfo.paymentId);
    $("#contractName").html(PaymentBaseInfo.contractName);
    $("#custName").html(PaymentBaseInfo.custName);
    $("#ownerEmpName").html(PaymentBaseInfo.ownerEmpName);
    $("#paymentType").html(PaymentBaseInfo.paymentType);
    $("#paymentTime").html(PaymentBaseInfo.paymentTime);
    $("#paymentMoney").html(PaymentBaseInfo.paymentMoney);
    $("#paymentRemark").html(PaymentBaseInfo.paymentRemark);
}

function showChangeOwner(emps) {
    //得到值
    var tbody = $("#empsTbody");
    tbody.empty();
    for (const item of emps) {
        var html = `
            <option value="${item.empId}">${item.empName}</option>
        `;
        tbody.append(html);
    }

}

//变更负责人
function saveChangeOwner() {
    empId = $("#empsTbody option:selected").val();
    //发送到后端服务器
    $.ajax({
        method: "POST",
        url: "http://localhost:8080/payment/changeOwner",
        data: {id, empId},
        dataType: "json",
        success: (response) => {
            if (response.code === 0) {
                window.alert("更改成功");
                fetchData();
                $("#ownerAndButton").hide();
            } else {
                window.alert("更改失败：" + response.msg);
            }
        },
    });
}


//编辑
function toEdit() {
    window.location.href = "payment-edit.html?id=" + id;
}

//删除
function del() {
    if (!confirm("删除之后数据无法恢复,确定删除?")) return;
    $.ajax({
        method: "POST",
        url: "http://localhost:8080/payment/delete",
        data: {id},
        dataType: "json",
        success: (response) => {
            if (response.code === 0) {
                window.alert("已成功删除回款");
            }
        },
    });
}

$(fetchData);

// 点击变更负责人，显示下方的输入项
$("#toShowChangeOwner").on("click", function () {
    $("#ownerAndButton").show();
});

// 点击取消时，隐藏下方的输入项
$("#toHideChangeOwner").on("click", function () {
    $("#ownerAndButton").hide();
});
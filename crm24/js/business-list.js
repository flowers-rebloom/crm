/**
 * 从后端服务器得到数据
 */

function fetchData(pageNo) {
    // 我得知道你填了什么搜索关键词
    const q = $("#q").val().trim();
    const pageSize = $("select#pageSize").val();
    // 发起请求
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/business/list",
        data: {q, pageSize, pageNo},

        dataType: "json",
        success: (response) => {
            showData(response.data.list);
            $("#total").text(response.data.total);
            // 自动渲染出一堆数字按钮
            render(response.data.total, pageSize, pageNo, ".page-nums", fetchData);
        },
    });
}

/**
 * 显示数据
 */
function showData(list) {
    $("#tbody").empty();
    for (const busi of list) {
        const html = `
		<tr>
			<td><input type="checkbox" name="id" value="${busi.businessId}"></td>
			<td><a href="business-details.html?id=${busi.businessId}">${busi.businessName}</a></td>
			<td>${busi.custName}</td>
			<td>${busi.currentStep}</td>
			<td>${busi.lastFollowupTime}</td>
			<td>${busi.nextFollowupTime}</td>
			<td>${busi.ownerEmpName}</td>
            </tr>
		`;
        $("#tbody").append(html);
    }
}

function toEdit() {
    //取得选中行的clueId
    var checkboxes = document.querySelectorAll("input[type=checkbox][name=id]");
    var checkedId;
    for (const checkbox of checkboxes) {
        if (checkbox.checked) {
            checkedId = checkbox.value;
            break;
        }
    }
    //转跳到business-edit.html
    window.location.href = "business-edit.html?id=" + checkedId;
}

// 点击全选那个checkbox时，让表格中的所有checkbox一起改变状态
$("#checkAll").on("click", function () {
    // 这里的this指的就是被你click的那个元素
    const state = this.checked; //这相当于 const state = $("#checkAll").prop("checked");
    $(":checkbox[name=id]").prop("checked", state);
});

/**
 * 批量删除
 */
function batchDelete() {
    if (!confirm("警告！删除操作不可恢复！确定要删除吗？")) return;
    //获取选中的多个id
    var checkboxes = document.querySelectorAll("input[type=checkbox][name=id]");
    var checkedId = [];
    for (const checkbox of checkboxes) {
        if (checkbox.checked) {
            checkedId.push(checkbox.value);
        }
    }
    // 请求后端服务器删除
    jQuery.ajax({
        method: "POST",
        url: "http://localhost:8080/business/batch-delete",
        data: {id: checkedId},
        success: (response) => {
            if (response.code === 0) window.alert("已批量删除");
            fetchData();
        },
    });
}

function pageSizeChangeHandler() {
    fetchData(1);
}

// 由于这个js文件是在body末尾去导入的，所以这行代码相当于是在load事件发生时执行。
fetchData(1);

var usp = new URLSearchParams(window.location.search);
const id = usp.get("id");
/**
 * 从后端服务器查询线索数据
 */
$(fetchData);

function fetchData() {
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/business/edit",
        data: {id},
        dataType: "json",
        success: function (response) {
            showData(response.data);
        },
    });
}

/**
 * 显示当前线索信息
 */
function showData(business) {
    var form = document.forms[0];
    form.businessName.value = business.businessName;
    form.businessMoney.value = business.businessMoney;
    form.businessRemark.value = business.businessRemark;
    form.nextFollowupTime.value = business.nextFollowupTime;
    // 给原生的<select>添加<option>
    form.custId.innerHTML = `<option value="${business.custId}">${business.custName}</option>`;
    // 初始化select2
}

// window.onload = fetchData;
/**
 * 保存
 */
function save() {
    // 得到表单元素的值
    var form = document.forms[0];
    var businessName = form.businessName.value;
    var custId = form.custId.value;
    var businessMoney = form.businessMoney.value;
    var businessRemark = form.businessRemark.value;
    var nextFollowupTime = form.nextFollowupTime.value;
    const business = {
        businessName,
        custId,
        businessMoney,
        businessRemark,
        nextFollowupTime,
        businessId: id,
    };
    // 验证
    var valid = validateForm(business);
    if (!valid) return;
    $.ajax({
        method: "POST",
        url: "http://localhost:8080/business/update",
        data: business,
        success: function (response) {
            if (response.code === 0) {
                window.alert("更新成功");
                // 后退
                history.back();
            } else {
                window.alert("更新失败" + response.msg);
            }
        },
    });
}

function validateForm(business) {
    if (business.businessName === 0) {
        window.alert("请输入商机名称");
        return false;
    }
    return true;
}

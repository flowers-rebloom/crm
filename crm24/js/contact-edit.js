var usp = new URLSearchParams(window.location.search);
const id = usp.get("id");

/**
 * 从后端服务器查询线索数据
 */
function fetchData() {
    jQuery.ajax({
        method: "GET",
        url: "http://localhost:8080/contact/edit?whoami=" + sessionStorage.getItem("whoami"),
        data: {id},
        success: (response) => {
            showData(response.data);
        },
    });
}

/**
 * 显示当前线索信息
 */
function showData(contact) {
    var form = document.forms[0];
    form.contactName.value = contact.contactName;
    form.contactPost.value = contact.contactPost;
    form.contactPhone.value = contact.contactPhone;
    form.contactEmail.value = contact.contactEmail;
    form.contactAddress.value = contact.contactAddress;
    form.contactRemark.value = contact.contactRemark;
    form.contactGender.value = contact.contactGender;
    form.isDecisionMaker.value = contact.isDecisionMaker;
}

window.onload = fetchData;

/**
 * 保存
 */
function save() {
    // 得到表单元素的值
    var form = document.forms[0];
    var contactName = form.contactName.value.trim();
    var contactPost = form.contactPost.value.trim();
    var contactPhone = form.contactPhone.value.trim();
    var contactEmail = form.contactEmail.value.trim();
    var contactAddress = form.contactAddress.value.trim();
    var contactRemark = form.contactRemark.value.trim();
    var contactSex = form.contactSex.value;
    var isDecisionMaker = form.isDecisionMaker.value;

    // 封装成对象
    const clue = {
        contactName,
        contactPost,
        contactPhone,
        contactEmail,
        contactAddress,
        contactRemark,
        contactSex,
        isDecisionMaker,
        contactId: id,
    };


    // 验证
    var valid = validateForm(contact);
    if (!valid) {
        return;
    }


    // 发送到后端服务器
    $.ajax({
        method: "POST",
        url: "http://localhost:8080/ccontact/update",
        data: clue,
        dataType: "json",
        success: (response) => {
            if (response.code === 0) {
                window.alert("更新成功");
            } else {
                window.alert("更新失败：" + response.msg);
            }
        },
    });


}

/**
 * 验证表单
 * @param {{contactName, contactPost, contactPhone, contactEmail, contactAddress, contactRemark, contactSex, isDecisionMaker, }} contact 封装了表单中各个数据项的联系人对象
 * @returns {boolean} true=>验证通过，false=>验证失败，可能有的数据项不符合要求
 */
function validateForm(contact) {
    if (contact.contactName === "") {
        window.alert("请输入联系人名称");
        return false;
    }
    return true;
}





// 获取地址栏上的id
const usp = new URLSearchParams(window.location.search);
const id = usp.get("id");

// 编辑
function toEdit() {
    window.location.href = "cust-edit.html?id=" + id;
}

// 删除

function del() {
    if (!confirm("危险！删除了数据不可恢复！确定要删除吗？")) return;
    $.ajax({
        method: "POST",
        url: "http://localhost:8080/cust/delete",
        data: {id},
        dataType: "json",
        success: function (response) {
            if (response.code === 0) {
                alert("已成功删除客户");
            }
        },
    });
}

// 改为已成交
function dealed() {
    $.ajax({
        type: "post",
        url: "http://localhost:8080/cust/dealed",
        data: {dealstatus: 0, id: id},
        dataType: "json",
        success: function (response) {
            if (response.code === 0) {
                alert("已成功改为已成交");
                $(fetchdata)
            }
        }
    });
}

// 改为未成交
function notDealed() {
    $.ajax({
        type: "post",
        url: "http://localhost:8080/cust/dealed",
        data: {dealstatus: 1, id: id},
        dataType: "json",
        success: function (response) {
            if (response.code === 0) {
                alert("已成功改为未成交");
                $(fetchdata)
            }
        }
    });
}

// 变更负责人
function ChangeOwner() {
    // 显示选择框
    $("#changeOwner").show();
    // 发送获取emplist的请求
    $.ajax({
        type: "get",
        url: "http://localhost:8080/cust/find-emp-list",
        dataType: "json",
        success: function (response) {
            showEmpList(response.data)
        }
    });

    // 显示更换负责人的option
    function showEmpList(empList) {
        for (const emp of empList) {
            var html = `<option value="${emp.empId}">${emp.empName}</option>`
            $("#changeOwner").append(html);
        }
        $("#changeOwner").on("change", function () {
            var empid = $("#changeOwner").val();
            // 发送选择的emplist
            $.ajax({
                type: "post",
                url: "http://localhost:8080/cust/responser",
                data: {empid: empid, id: id},
                dataType: "json",
                success: function (response) {
                    if (response.code === 0) {
                        alert("已成功更改负责人");
                        $(fetchdata)
                    }
                }
            });
        })
    }
}

// 锁定
function locked() {
    $.ajax({
        type: "post",
        url: "http://localhost:8080/cust/locked",
        data: {lockstatus: 0, id: id},
        dataType: "json",
        success: function (response) {
            if (response.code === 0) {
                alert("已成功锁定");
                $(fetchdata)
            }
        }
    });

}

// 解锁
function notLocked() {
    $.ajax({
        type: "post",
        url: "http://localhost:8080/cust/locked",
        data: {lockstatus: 1, id: id},
        dataType: "json",
        success: function (response) {
            if (response.code === 0) {
                alert("已成功解锁");
                $(fetchdata)
            }
        }
    });
}

// 放入公海
function intoSea() {
    $.ajax({
        type: "post",
        url: "http://localhost:8080/cust/put-into-sea",
        data: {id},
        dataType: "json",
        success: function (response) {
            if (response.code === 0) {
                alert("已成功放入公海");
                $(fetchdata)
            }
        }
    });
}

// 提交按钮的函数
function savefollowup() {
    var followupTime = $("#followupTime").val();
    var linkman = $("#linkman").val();
    var followupMethod = $("#followupMethod").val();
    var followupNote = $("#followupNote").val();
    var nextfollowupTime = $("#nextFollowupTime").val()
    let followUp = {
        followupTime,
        linkman,
        followupMethod,
        followupNote,
        nextfollowupTime,
        custid: id
    };

    // 检验元素的有效性
    var valid = validFormData(followUp);
    if (!valid) return;
    $.ajax({
        type: "post",
        url: "http://localhost:8080/cust/followup",
        dataType: "json",
        success: function (response) {
            if (response.code == 0) {
                alert("新的跟进记录已保存");
            }
        }
    });
}

// 检验有效性的封装函数
function validFormData(followUp) {
    if (followUp.followupNote === "") {
        window.alert("请输入跟进内容");
        return false;
    }
    return true;
}


// 从数据库获取数据
function fetchdata() {
    $.ajax({
        type: "get",
        url: "http://localhost:8080/cust/detail",
        data: {id},
        dataType: "json",
        success: function (response) {
            showBaseInfo(response.data.baseinfo)
            showContacts(response.data.contacts)
            showfollowups(response.data.followups)
            showContactName(response.data.contacts)
        }
    });

}

// 显示数据
function showBaseInfo(baseinfo) {
    // 线索基本信息
    $("#name").html(baseinfo.custName);
    $("#responser").html(baseinfo.ownerEmpName);
    $("#source").html(baseinfo.custSource);
    $("#phone").html(baseinfo.custPhone);
    $("#place").html(baseinfo.custAddress);
    $("#nextCalltime").html(baseinfo.nextFollowupTime);
    $("#eaddress").html(baseinfo.custWebsite);
    $("#localaddress").html(baseinfo.custProvince + baseinfo.custCity + baseinfo.custDistrict);
    $("#lockState").html(baseinfo.lockStatus === true ? "已锁定" : "未锁定");
    $("#dealState").html(baseinfo.dealStatus === true ? "已成交" : "未成交");
    $("#remarks").html(baseinfo.custRemark);
}

// 联系人
function showContacts(contacts) {
    for (const linkman of contacts) {
        const linkmenHeml = `
    <tr>
        <td>${linkman.contactName}</td>
        <td>${linkman.contactPost}</td>
        <td>${linkman.contactPhone}</td>
        <td>${linkman.contactGender}</td>
        <td>${linkman.contactEmail}</td>
        <td>${linkman.isDecisionMaker = 0 ? "是" : "否"}</td>
    </tr>
    `;
        $("#linkmanBody").append(linkmenHeml)
    }
}


// 跟进备注
function showfollowups(followups) {
    for (const Records of followups) {
        const noteHtml = `
    <tr>
        <td>${Records.followupTime}</td>
        <td>${Records.contactName}</td>
        <td>${Records.followupMethod}</td>
        <td>${Records.custFollowupPresentation}</td>
      
    </tr>
    `;
        $("#notesBody").append(noteHtml);
    }
}

window.onload = function () {
    fetchdata();
}

// 显示联系人
function showContactName(contacts) {
    for (const contactName of contacts) {
        var Html = `<option value="${contactName.contactId}">${contactName.contactName}</option>`
        $("#linkman").append(Html);
    }
}

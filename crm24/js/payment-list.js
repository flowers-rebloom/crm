function fetchData(pageNo) {
    const q = $("#q").val();
    const pageSize = $("select#pageSize").val();
    jQuery.ajax({
        method: "GET",
        url: "http://localhost:8080/payment/list",
        data: {q, pageSize, pageNo},
        dataType: "json",
        success: (response) => {
            showData(response.data.list);
            $("#total").text(response.data.total);
            // 自动渲染出一堆数字按钮
            render(response.data.total, pageSize, pageNo, ".page-nums", fetchData);
        },
    });
}

function showData(list) {
    $("#tbody").empty();
    for (const payment of list) {
        const html = `
		<tr>
			<td><input type="checkbox" name="id" value="${payment.paymentId}"></td>
			<td><a href="payment-details.html?id=${payment.paymentId}">${payment.paymentId}</a></td>
			<td>${payment.custName}</td>
			<td>${payment.contractName}</td>
			<td>${payment.paymentTime}</td>
            <td>${payment.paymentType}</td>
			<td>${payment.paymentMoney}</td>
            <td>${payment.ownerEmpName}</td>
			<td>${payment.paymentRemark}</td>
		</tr>
		`;
        $("#tbody").append(html);
    }
}

function toEdit() {
    // 取得选中的行的paymentId
    var checkboxes = document.querySelectorAll("input[type=checkbox][name=id]");
    var checkedId;
    for (const checkbox of checkboxes) {
        if (checkbox.checked) {
            checkedId = checkbox.value;
            break;
        }
    }
    // 跳转到payment-edit.html
    window.location.href = "payment-edit.html?id=" + checkedId;
}

function batchDelete() {
    if (!confirm("警告！删除操作不可恢复！确定要删除吗？")) return;
    // 获取被选中的多个id
    var checkboxes = document.querySelectorAll("input[type=checkbox][name=id]");
    var checkedId = [];
    for (const checkbox of checkboxes) {
        if (checkbox.checked) {
            checkedId.push(checkbox.value);
        }
    }
    // 请求后端服务器删除
    jQuery.ajax({
        method: "POST",
        url: "http://localhost:8080/payment/batch-delete",
        data: {id: checkedId},
        success: (response) => {
            if (response.code === 0) fetchData();
        },
    });
}

function pageSizeChangeHandler() {
    fetchData(1);
}

fetchData(1);

/**
 *
 * [DEBUG]自动设置表单元素的值。此函数的目的是方便我们在开发过程中进行测试，以免手工输入的麻烦。
 */
function setFormValues() {
    $("#clueName").val("线索名称");
    $("#clueSource").val("线索来源");
    $("#cluePhone").val("联系电话");
    $("#clueAddress").val("通讯地址");
    $("#followuped").prop("checked", true);
    $("#lastFollowupTime").val("跟进时间");
    $("#lastFollowupContent").val("跟进记录");
    $("#clueRemark").val("备注");
    $("#nextFollowupTime").val("下次联系时间");
}

/**
 * 保存按钮的click事件处理函数
 */
$("#btnSave").on("click", function () {
    // 获取表单元素的值
    var clueName = $("#clueName").val();
    var clueSource = $("#clueSource").val();
    var cluePhone = $("#cluePhone").val();
    var clueAddress = $("#clueAddress").val();
    var followupStatus = document.forms[0].followupStatus.value;
    var lastFollowupTime = $("#lastFollowupTime").val();
    var lastFollowupPresentation = $("#lastFollowupPresentation").val();
    var clueRemark = $("#clueRemark").val();
    var nextFollowupTime = $("#nextFollowupTime").val();
    // 封装成线索对象（数据模型）
    var clue = {
        clueName,
        clueSource,
        cluePhone,
        clueAddress,
        followupStatus,
        lastFollowupTime,
        lastFollowupPresentation,
        clueRemark,
        nextFollowupTime,
    };
    // 调用validateForm函数进行表单验证
    var valid = validateForm(clue);
    if (!valid) return;
    // 发起Ajax请求，提交数据给后端控制器
    $.ajax({
        method: "POST",
        url: "http://localhost:8080/clue/create",
        data: clue,
    }).done((response) => {
        if (response.code === 0) {
            window.alert("线索新建成功！");
        } else {
            window.alert("线索新建失败！");
        }
    });
});

/**
 * 验证表单
 * @param {{clueName, clueSource, phone, address, followupState, followupTime, followupContent, remark, nextTime, }} clue 封装了表单中各个数据项的线索对象
 * @returns {boolean} true=>验证通过，false=>验证失败，可能有的数据项不符合要求
 */
function validateForm(clue) {
    if (clue.clueName === "") {
        window.alert("请输入线索名称");
        return false;
    }
    return true;
}

// 点击未跟进时，隐藏下方的输入项
$("#notFollowup").on("click", function () {
    $("#FollowupTimeAndContent").hide();
});

// 点击已跟时，显示下方的输入项
$("#followuped").on("click", function () {
    $("#FollowupTimeAndContent").show();
});

function fetchData(pageNo) {
    const q = $("#q").val();
    const pageSize = $("select#pageSize").val();
    jQuery.ajax({
        method: "GET",
        url: "http://localhost:8080/contact/list",
        data: {q, pageSize, pageNo},
        dataType: "json",
        success: (response) => {
            showData(response.data.list);
            $("#total").text(response.data.total);
            // 自动渲染出一堆数字按钮
            render(response.data.total, pageSize, pageNo, ".page-nums", fetchData);
        },
    });
}

function showData(list) {
    $("#tbody").empty();
    for (const contact of list) {
        const html = `
		<tr>
			<td><input type="checkbox" name="id" value="${contact.contactId}"></td>
			<td><a href="contact-details.html?id=${contact.contactId}">${
            contact.contactName
        }</a></td>
			<td>${contact.contactPost}</td>
			<td>${contact.contactGender}</td>
			<td>${contact.isDecisionMaker ? "是" : "否"}</td>
			<td>${contact.contactPhone}</td>
			<td>${contact.contactEmail}</td>
			<td>${contact.contactAddress}</td>
			<td>${contact.contactRemark}</td>
  		</tr>
		`;
        $("#tbody").append(html);
    }
}

function toEdit() {
    var checkboxes = document.querySelectorAll("input[type=checkbox][name=id]");
    var checkedId;
    for (const checkbox of checkboxes) {
        if (checkbox.checked) {
            checkedId = checkbox.value;
            break;
        }
    }
    window.location.href = "contact-edit.html?id=" + checkedId;
}

function batchDelete() {
    if (!confirm("警告！删除操作不可恢复！确定要删除吗？")) return;
    var checkboxes = document.querySelectorAll("input[type=checkbox][name=id]");
    var checkedId = [];
    for (const checkbox of checkboxes) {
        if (checkbox.checked) {
            checkedId.push(checkbox.value);
        }
    }
    jQuery.ajax({
        method: "POST",
        url: "http://localhost:8080/contact/batchdelete",
        data: {id: checkedId},
        dataType: "json",
        success: (response) => {
            if (response.code === 0) fetchData();
        },
    });
}

function checkAll() {
    $("input[name=id]").prop("checked", $("#mainCheck").prop("checked"));
}

function pageSizeChangeHandler() {
    fetchData(1);
}

fetchData(1);

var usp = new URLSearchParams(window.location.search);
const id = usp.get("id");


//根据id从后端查询数据线索
function fetchData() {
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/contract/edit",
        data: {id},
        dataType: "json",
        success: (response) => {
            showData(response.data);
        },
    });
}


//显示从后端得到的数据
function showData(contract) {
    $("#contractCode").val(contract.contractCode);
    $("#contractName").val(contract.contractName);
    $("#custId").html(`<option value="${contract.custId}">${contract.custName}</option>`);
    $("#businessId").html(`<option value="${contract.businessId}">${contract.businessName ? contract.businessId : "暂无数据"}</option>`);
    $("#contractMoney").val(contract.contractMoney);
    $("#contractStartTime").val(contract.contractStartTime);
    $("#contactId").html(`<option value="${contract.ownerEmpId}">${contract.ownerEmpName}</option>`);
    $("#empId").html(`<option value="${contract.companyEmpId}">${contract.companyEmpName}</option>`);
    $("#contractRemark").val(contract.contractRemark);
}


function save() {
    //得到里面的值
    var contractCode = $("#contractCode").val();
    var contractName = $("#contractName").val();
    var custId = $("#custId").val();
    var businessId = $("#businessId").val();
    var contractMoney = $("#contractMoney").val();
    var contractStartTime = $("#contractStartTime").val();
    var companyEmpId = $("#contactId").val();
    var ownerEmpId = $("#empId").val();
    var contractRemark = $("#contractRemark").val();
    // var form = document.forms[0];
    // var contractCode=form.contractCode.value;
    //     var contractName =form.contractName.value;
    //     var custId=form.custId.value;
    //     var businessId =form.businessId.value;
    //     var contractMoney = form.contractMoney.value;
    //     var contractStartTime = form.contractStartTime.value;
    //     var companyEmpId=form.contactId.value;
    //     var ownerEmpId =form.empId.value;
    //     var contractRemark = form.contractRemark.value;


    //封装成实体
    const contract = {
        contractCode,
        contractName,
        custId,
        businessId,
        contractMoney,
        contractStartTime,
        companyEmpId,
        ownerEmpId,
        contractRemark,
        contractId: id
    };
    // 验证
    var valid = validateForm(contract);
    if (!valid) {
        return;
    }
    //发送到后端服务器
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/contract/update",
        data: contract,
        success: (response) => {
            if (response.code === 0) {
                window.alert("更新成功");
                //回退
                history.back();
            } else {
                window.alert("更新失败：" + response.msg);
            }
        },
    })


}

/**
 * 验证表单
 * @param {{clueName, clueSource, phone, address, followupState, followupTime, followupContent, remark, nextTime, }} clue 封装了表单中各个数据项的线索对象
 * @returns {boolean} true=>验证通过，false=>验证失败，可能有的数据项不符合要求
 */
function validateForm(contract) {
    if (contract.contractName === "") {
        window.alert("请输入名称");
        return false;
    }
    return true;
}


fetchData();
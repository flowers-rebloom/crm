var method;
var url;
var data;

var servPara = {
    method,
    url,
    data,
};
const whoami = sessionStorage.getItem("whoami");
if (whoami) {
    jQuery.ajaxSetup({
        beforeSend: function (jqxhr, settings) {
            jqxhr.setRequestHeader("whoami", whoami);
        },
    });
}

/**
 * 请求后端服务器
 * @param {*} servPara 传入一个对象，包含method，url,data endBack=>弹窗提示内容
 */
function askServer(servPara, endBack) {
    var resp;
    $.ajax({
        method: `${servPara.method}`,
        url: `${servPara.url}`,
        data: servPara.data,
        dataType: "json",
        success: function (response) {
            respCode = response.code;
            resp = response;
            judge(respCode, endBack);
        },
    });
    return resp;
}

var code;

function returnCode(respCode) {
    code = respCode;
}

/**
 *
 * @param {} judgePara需验证的表单项，如custom.customName
 * @param {} nameString 提示的输入的名称
 * @returns
 */
function validateFrom(judgePara, nameString) {
    if (judgePara === "") {
        window.alert(`请输入${nameString}名称`);
        return false;
    }
    return true;
}

/**
 * 隐藏dom元素
 */
function hideFollowupTimeAndContent() {
    $("#FollowupTimeAndContent").hide();
}

/**
 * 显示dom元素
 */
function showFollowupTimeAndContent() {
    $("#FollowupTimeAndContent").show();
}

/**
 * 判断服务器通信结果
 * @param {} respCode 求情服务器返回的请求
 * @param {} doThing 正在进行的操作
 */
function judge(respCode, endBack) {
    if (respCode == 0) {
        if (endBack !== "") {
            window.alert(endBack);
            window.location.reload();
        } else {
            return;
        }
    } else {
        window.alert("发生错误");
    }
}

//================================分页===============================================
const ulTag = document.querySelector("ul");

function element(totalPages, page) {
    let liTag = "";
    let activeLi;
    let beforePages = page - 1; //8-1=7
    let afterPages = page + 1; //8+1=9
    //当页数大于1时，添加一个前页码按钮
    if (page > 1) {
        liTag += `  <li class="bton prev" onclick="element(totalPages, ${
            page - 1
        })"><span><<i class="fas fa-angle-left prev"></i></span></li>`;
    }
    //添加前省略...
    if (page > 2) {
        liTag += `<li class="numb" onclick="element(totalPages, 1)"><span>1</span></li>`;
        if (page > 3) {
            liTag += `<li class="dots"><span>...</span></li>`;
        }
    }
    //
    if (page == totalPages) {
        beforePages = beforePages - 2;
    } else if (page == totalPages - 1) {
        beforePages = beforePages - 1;
    }
    //
    if (page == 1) {
        afterPages = beforePages + 2;
    } else if (page == 2) {
        afterPages = beforePages + 1;
    }

    //添加前后页码
    for (let pageLength = beforePages; pageLength <= afterPages; pageLength++) {
        //限制添加
        if (pageLength > totalPages) {
            continue;
        }
        if (pageLength == 0) {
            pageLength = pageLength + 1;
        }
        if (page == pageLength) {
            activeLi = "activing";
        } else {
            activeLi = ``;
        }
        liTag += `<li class="numb ${activeLi}" onclick="element(totalPages, ${pageLength})"><span>${pageLength}</span></li>`;
    }
    //添加后省略...
    if (page < totalPages - 1) {
        if (page < totalPages - 2) {
            liTag += `<li class="dots"><span>...</span></li>`;
        }
        liTag += `<li class="numb" onclick="element(totalPages, ${totalPages})">${totalPages}<span></span></li>`;
    }

    //当页码小于总页数，添加一个后页码按钮
    if (page < totalPages) {
        liTag += `  <li class="bton next" onclick="element(totalPages, ${
            page + 1
        })"><span>><i class="fas fa-angle-right prev"></i></span></li>`;
    }
    ulTag.innerHTML = liTag;

    //查询数据库

    pageNum = page;
    pageServer();
}

// 获取地址栏上的id
const usp = new URLSearchParams(window.location.search);
const id = usp.get("id");
var CUST;
$(
    fetchData()
)

// 获取原有数据
function fetchData() {
    $.ajax({
        type: "get",
        url: "http://localhost:8080/cust/edit",
        data: {id},
        dataType: "json",
        success: function (response) {
            CUST = response.data;
            showDate(response.data)
            AutoShowData()
        }
    });
}

function showDate(cust) {
    $("#clientName").val(cust.custName),
        $(`option:contains(${cust.custSource})`).prop("selected", true),
        $("#tele").val(cust.custPhone),
        $("#webNet").val(cust.custWebsite),
        // $("#provience").val(cust.custProvince),
        // $("#city").val(cust.custCity),
        // $("#area").val(cust.custDistrict),
        $("#address").val(cust.custAddress),
        $("#nextTime").val(cust.nextFollowupTime),
        $("#remark").val(cust.custRemark);
    $("#provience").change();
}


//  点击保存
function save() {
// 获得各个表单元素的值
    var clientName = $("#clientName").val();
    var clientResource = $("#clientResource").val();
    var tele = $("#tele").val();
    var webNet = $("#webNet").val();
    var provience = $("#provience").val();
    var city = $("#city").val();
    var area = $("#area").val();
    var address = $("#address").val();
    var nextTime = $("#nextTime").val();
    var remark = $("#remark").val();
// 用一个对象封装值
    var client = {
        custId: id,
        clientName,
        clientResource,
        tele,
        webNet,
        provience,
        city,
        area,
        address,
        nextTime,
        remark,
    }
// 检验对象内元素的有效性
    var valid = validity(client);
    if (!valid) return;
    $.ajax({
        type: "post",
        url: "http://localhost:8080/cust/update",
        data: client,
        dataType: "json",
        success: function (response) {
            if (response.code === 0) {
                window.alert("更新成功");
                window.location.href = "cust-list.html"
            } else {
                window.alert("更新失败：" + response.msg);
            }
        }
    });


}

// 封装测试有效性的函数
function validity(client) {
    if (client.clientName === "") {
        window.alert("请填入客户名称");
        return false;
    }
    return true;
}


// 自动显示省份
// $(AutoShowData)
function AutoShowData() {
    $.ajax({
        type: "get",
        url: "http://localhost:8080/cust/area",
        data: {parentid: 0},
        dataType: "json",
        success: function (response) {
            if (response.code === 0) {
                showProvince(response.data)

            }
        }
    });
}

// 处理省市区的change事件
$("#provience").on("change", function () {

    var parentid = $("#provience").val();
    if (!parentid) return;
    $.ajax({
        type: "get",
        url: "http://localhost:8080/cust/area",
        data: {parentid},
        dataType: "json",
        success: function (response) {
            if (response.code === 0) {
                showCity(response.data)
            }
        }
    });
})
$("#city").on("change", function () {
    var parentid = $("#city").val();
    $.ajax({
        type: "get",
        url: "http://localhost:8080/cust/area",
        data: {parentid},
        dataType: "json",
        success: function (response) {
            if (response.code === 0) {
                showDistrict(response.data)
            }
        }
    });
})

// 显示省
function showProvince(data) {
    for (const province of data) {
        var provinceHtml = `<option value="${province.areaId}">${province.areaName}</option>`;
        $("#provience").append(provinceHtml);
    }
    $("#provience").val(CUST.custProvince).change()
}

//显示市
function showCity(data) {
    for (const city of data) {
        var cityHtml = `<option value="${city.areaId}">${city.areaName}</option>`
        $("#city").append(cityHtml)
    }
    $("#city").val(CUST.custCity).change()
}

// 显示区
function showDistrict(data) {
    for (const district of data) {
        var districtHtml = `<option value="${district.areaId}">${district.areaName}</option>`
        $("#area").append(districtHtml)
    }

}
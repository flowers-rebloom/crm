function statistics() {
    const dateBegin = $("#dateBegin").val();
    const dateEnd = $("#dateEnd").val();
    jQuery
        .ajax({
            method: "GET",
            url: "http://localhost:8080/contract/money",
            data: {dateBegin, dateEnd},
            dataType: "json",
        })
        .done((response) => {
            const list = response.data.reverse();
            const empName = list.map((item) => item.empName);
            const sumContractMoney = list.map((item) => item.sumContractMoney);
            const sumPaymentMoney = list.map((item) => item.sumPaymentMoney);
            drawCharts(empName, sumContractMoney, sumPaymentMoney);
        });
}

function drawCharts(empName, sumContractMoney, sumPaymentMoney) {
    var chartDom = document.getElementById("main");
    var myChart = echarts.init(chartDom);
    var option;

    option = {
        title: {
            text: "排行榜",
        },
        tooltip: {
            trigger: "axis",
            axisPointer: {
                type: "shadow",
            },
        },
        legend: {
            data: ["合同金额", "回款金额"],
        },
        grid: {
            left: "3%",
            right: "4%",
            bottom: "3%",
            containLabel: true,
        },
        xAxis: {
            type: "value",
            boundaryGap: [0, 0.01],
        },
        yAxis: {
            type: "category",
            data: empName,
        },
        series: [
            {
                name: "合同金额",
                type: "bar",
                data: sumContractMoney,
            },
            {
                name: "回款金额",
                type: "bar",
                data: sumPaymentMoney,
            },
        ],
    };

    option && myChart.setOption(option);
}
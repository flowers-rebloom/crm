/**
 *
 * [DEBUG]自动设置表单元素的值。此函数的目的是方便我们在开发过程中进行测试，以免手工输入的麻烦。
 */
function setFormValues() {
    $("#paymentId").val(2);
    $("#custId").val(2);
    $("#contractId").val(2);
    $("#paymentTime").val("2021-03-02");
}

/**
 * 保存按钮的click事件处理函数
 */
$("#btnSave").on("click", function () {
    // 获取表单元素的值
    var paymentId = $("#paymentId").val();
    var custName = $("#custName").val();
    var contractName = $("#contractName").val();
    var paymentTime = $("#paymentTime").val();
    var paymentType = $("#paymentType").val();
    var paymentMoney = $("#paymentMoney").val();
    var ownerEmpName = $("#ownerEmpName").val();
    var paymentRemark = $("#paymentRemark").val();
    // 封装成线索对象（数据模型）
    var payment = {
        paymentId,
        custName,
        contractName,
        paymentTime,
        paymentType,
        paymentMoney,
        ownerEmpName,
        paymentRemark,
    };
    // 调用validateForm函数进行表单验证
    var valid = validateForm(payment);
    if (!valid) return;
    // 发起Ajax请求，提交数据给后端控制器
    $.ajax({
        method: "POST",
        url:
            "http://localhost:8080/payment/create?whoami=" +
            sessionStorage.getItem("whoami"),
        data: payment,
    }).done((response) => {
        if (response.code === 0) {
            window.alert("线索回款成功！");
        } else {
            window.alert("线索回款失败！");
        }
    });
});

/**
 * 验证表单
 * @param {{paymentId, paymentMoney...}} payment 封装了表单中各个数据项的回款对象
 * @returns {boolean} true=>验证通过，false=>验证失败，可能有的数据项不符合要求
 */
function validateForm(payment) {
    if (payment.paymentId === "") {
        window.alert("请输入回款id");
        return false;
    }
    return true;
}

/**
 * 让下拉框支持搜索查询
 */
$("#custName").select2({
    ajax: {
        url: "http://localhost:8080/payment/find-for-select",
        dataType: "json",
        processResults: function (response) {
            return {
                results: response.data,
            };
        },
    },
});

/**
 * 选定客户名称之后，根据客户名称对应的id找到其对应的所有合同
 */
$("#custName").on("change", function () {
    // 获取客户的值
    var custName = $("#custName").val();
    // ajax请求
    $.ajax({
        method: "get",
        url: "http://localhost:8080/payment/select-contract",
        data: {custName},
        dataType: "json",
        success: (response) => {
            showContractNames(response.data);
        },
    });
});

/**
 * 通过循环和append动态渲染合同名称并 show 出来
 * @param {*} data
 */
function showContractNames(data) {
    for (const ContractNames of data) {
        var options = `<option value="${ContractNames.contractId}">${ContractNames.contractName}</option>`
        $("#contractName").append(options)
    }

}

/**
 * 通过循环和append动态渲染负责人姓名并 show 出来
 */

$(showEmpList)

function showEmpList() {
    $.ajax({
        method: "get",
        url: "http://localhost:8080/payment/select-owner-emp-name",

        dataType: "json",
        success: (response) => {
            showOwnerEmpName(response.data);
        }
    })
}


function showOwnerEmpName(data) {
    for (const names of data) {
        var options = `<option value="${names.empId}">${names.empName}</option>`
        $("#ownerEmpName").append(options)
    }
}
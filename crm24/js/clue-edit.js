var usp = new URLSearchParams(window.location.search);
const id = usp.get("id");

/**
 * 从后端服务器查询线索数据
 */
function fetchData() {
    jQuery.ajax({
        method: "GET",
        url: "http://localhost:8080/clue/edit",
        data: {id},
        success: (response) => {
            showData(response.data);
        },
    });
}

/**
 * 显示当前线索信息
 */
function showData(clue) {
    var form = document.forms[0];
    form.clueName.value = clue.clueName;
    $(`option:contains(${clue.clueSource})`).prop("selected", true);
    form.cluePhone.value = clue.cluePhone;
    form.clueAddress.value = clue.clueAddress;
    form.clueRemark.value = clue.clueRemark;
    form.nextFollowupTime.value = clue.nextFollowupTime;
}

window.onload = fetchData;

/**
 * 保存
 */
function save() {
    // 得到表单元素的值
    var form = document.forms[0];
    var clueName = form.clueName.value.trim();
    var clueSource = form.clueSource.value;
    var cluePhone = form.cluePhone.value.trim();
    var clueAddress = form.clueAddress.value.trim();
    var clueRemark = form.clueRemark.value.trim();
    var nextFollowupTime = form.nextFollowupTime.value;

    // 封装成对象
    const clue = {
        clueName,
        clueSource,
        cluePhone,
        clueAddress,
        clueRemark,
        nextFollowupTime,
        clueId: id,
    };

    // 验证
    var valid = validateForm(clue);
    if (!valid) {
        return;
    }

    // 发送到后端服务器
    $.ajax({
        method: "POST",
        url: "http://localhost:8080/clue/update",
        data: clue,
        dataType: "json",
        success: (response) => {
            if (response.code === 0) {
                window.alert("更新成功");
            } else {
                window.alert("更新失败：" + response.msg);
            }
        },
    });
}

/**
 * 验证表单
 * @param {{clueName, clueSource, phone, address, followupState, followupTime, followupContent, remark, nextTime, }} clue 封装了表单中各个数据项的线索对象
 * @returns {boolean} true=>验证通过，false=>验证失败，可能有的数据项不符合要求
 */
function validateForm(clue) {
    if (clue.clueName === "") {
        window.alert("请输入线索名称");
        return false;
    }
    return true;
}
